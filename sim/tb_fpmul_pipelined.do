# Tcl script to simulate the initial multiplier
# by Guillermo Marcus, before applying any change



# ---------------------- VARIABLES ---------------------- 

# Directories
set COMMON_DIR "../fpuvhdl/common"
set SOURCE_DIR "../fpuvhdl/multiplier"
set TB_DIR "../tb"

# Testbench entity. Supposed to be coincident
# with the testbench file name
set TB "tb_fpmul_pipeline"




# ---------------------- COMPILE ---------------------- 

# Create the work directory
vlib work

# Our multiplier
vcom ../multiplier/main_blocks/dadda_tree.vhd
vcom ../multiplier/layers/L17.vhd
vcom ../multiplier/layers/L13.vhd
vcom ../multiplier/layers/L9.vhd
vcom ../multiplier/layers/L6.vhd
vcom ../multiplier/layers/L4.vhd
vcom ../multiplier/layers/L3.vhd
vcom ../multiplier/shift_triangle/shift_triangle.vhd
vcom ../multiplier/remapper/*.vhd
vcom ../multiplier/compressor/*.vhd
vcom ../multiplier/sideCompressors/*.vhd
vcom ../multiplier/booth/*.vhd
vcom ../multiplier/MBE_mpy.vhd

# Compile all the required components
vcom $COMMON_DIR/*.vhd
vcom $SOURCE_DIR/fpmul_stage*
vcom $SOURCE_DIR/fpmul_pipeline.vhd

# Compile the testbench
vcom $TB_DIR/$TB.vhd



# ---------------------- SIMULATE  ---------------------- 

vsim -t ns -novopt work.$TB
run 1 us

# Exit from modelsim
quit -f

