 
****************************************
Report : area
Design : FPmul
Version: O-2018.06-SP4
Date   : Wed Nov 18 11:02:13 2020
****************************************

Library(s) Used:

    NangateOpenCellLibrary (File: /software/dk/nangate45/synopsys/NangateOpenCellLibrary_typical_ecsm_nowlm.db)

Number of ports:                          301
Number of nets:                          2707
Number of cells:                         2169
Number of combinational cells:           1905
Number of sequential cells:               243
Number of macros/black boxes:               0
Number of buf/inv:                        345
Number of references:                      29

Combinational area:               3032.932008
Buf/Inv area:                      233.282000
Noncombinational area:            1102.569961
Macro/Black Box area:                0.000000
Net Interconnect area:      undefined  (Wire load has zero net area)

Total cell area:                  4135.501969
Total area:                 undefined
1
