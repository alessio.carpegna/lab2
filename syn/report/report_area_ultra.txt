 
****************************************
Report : area
Design : FPmul
Version: O-2018.06-SP4
Date   : Sat Nov 21 12:51:29 2020
****************************************

Library(s) Used:

    NangateOpenCellLibrary (File: /software/dk/nangate45/synopsys/NangateOpenCellLibrary_typical_ecsm_nowlm.db)

Number of ports:                           97
Number of nets:                          2837
Number of cells:                         2511
Number of combinational cells:           2225
Number of sequential cells:               283
Number of macros/black boxes:               0
Number of buf/inv:                        278
Number of references:                      39

Combinational area:               3149.439991
Buf/Inv area:                      187.796000
Noncombinational area:            1291.695955
Macro/Black Box area:                0.000000
Net Interconnect area:      undefined  (Wire load has zero net area)

Total cell area:                  4441.135946
Total area:                 undefined
1
