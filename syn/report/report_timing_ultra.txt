Information: Updating design information... (UID-85)
Warning: There are infeasible paths detected in your design that were ignored during optimization. Please run 'report_timing -attributes' and/or 'create_qor_snapshot/query_qor_snapshot -infeasible_paths' to identify these paths.  (OPT-1721)
 
****************************************
Report : timing
        -path full
        -delay max
        -max_paths 1
Design : FPmul
Version: O-2018.06-SP4
Date   : Sat Nov 21 12:51:28 2020
****************************************

Operating Conditions: typical   Library: NangateOpenCellLibrary
Wire Load Model Mode: top

  Startpoint: I1/A_SIG_reg[7]
              (rising edge-triggered flip-flop clocked by MY_CLK)
  Endpoint: I2/seg_sig_in_reg[18]
            (rising edge-triggered flip-flop clocked by MY_CLK)
  Path Group: MY_CLK
  Path Type: max

  Des/Clust/Port     Wire Load Model       Library
  ------------------------------------------------
  FPmul              5K_hvratio_1_1        NangateOpenCellLibrary

  Point                                    Incr       Path
  -----------------------------------------------------------
  clock MY_CLK (rise edge)                 0.00       0.00
  clock network delay (ideal)              0.00       0.00
  I1/A_SIG_reg[7]/CK (DFF_X1)              0.00       0.00 r
  I1/A_SIG_reg[7]/QN (DFF_X1)              0.09       0.09 f
  U831/ZN (INV_X1)                         0.12       0.20 r
  U311/Z (XOR2_X1)                         0.12       0.32 r
  U837/ZN (INV_X1)                         0.04       0.36 f
  U424/ZN (INV_X1)                         0.05       0.41 r
  U936/ZN (OAI22_X1)                       0.05       0.46 f
  U985/CO (FA_X1)                          0.11       0.57 f
  U996/CO (FA_X1)                          0.10       0.67 f
  U984/S (FA_X1)                           0.14       0.81 r
  U1090/S (FA_X1)                          0.12       0.92 f
  U1091/S (FA_X1)                          0.13       1.06 r
  U685/ZN (OR2_X1)                         0.04       1.09 r
  U636/ZN (AND2_X1)                        0.04       1.14 r
  U629/ZN (AND4_X1)                        0.06       1.20 r
  U691/ZN (NAND2_X1)                       0.03       1.23 f
  U597/ZN (AND3_X2)                        0.05       1.28 f
  U2030/ZN (OAI21_X1)                      0.05       1.33 r
  U2033/ZN (XNOR2_X1)                      0.06       1.39 r
  I2/seg_sig_in_reg[18]/D (DFF_X1)         0.01       1.40 r
  data arrival time                                   1.40

  clock MY_CLK (rise edge)                 0.00       0.00
  clock network delay (ideal)              0.00       0.00
  clock uncertainty                       -0.07      -0.07
  I2/seg_sig_in_reg[18]/CK (DFF_X1)        0.00      -0.07 r
  library setup time                      -0.03      -0.10
  data required time                                 -0.10
  -----------------------------------------------------------
  data required time                                 -0.10
  data arrival time                                  -1.40
  -----------------------------------------------------------
  slack (VIOLATED)                                   -1.50


1
