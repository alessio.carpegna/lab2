# Internal variables
set TOP_ENTITY FPmul
set ARCH pipeline
set CLOCK_SIGNAL MY_CLK
set CLK_INPUT clk
set CLK_PERIOD 0.00
set CLK_UNCERT 0.07
set IN_DELAY 0.5
set OUT_DELAY 0.5

# Output files
set REPORT_TIMING report_timing_handmade_ultra.txt
set REPORT_AREA report_area_handmade_ultra.txt

# Import vhdl source files in the current design
# 	-f = format of the source files


analyze -f vhdl -lib WORK ../multiplier/remapper/remapper2.vhd
analyze -f vhdl -lib WORK ../multiplier/remapper/remapper4.vhd
analyze -f vhdl -lib WORK ../multiplier/remapper/remapper6.vhd
analyze -f vhdl -lib WORK ../multiplier/remapper/remapper8.vhd
analyze -f vhdl -lib WORK ../multiplier/compressor/HA.vhd
analyze -f vhdl -lib WORK ../multiplier/compressor/FA.vhd
analyze -f vhdl -lib WORK ../multiplier/compressor/comp12_8.vhd
analyze -f vhdl -lib WORK ../multiplier/compressor/comp9_6.vhd
analyze -f vhdl -lib WORK ../multiplier/compressor/comp6_4.vhd
analyze -f vhdl -lib WORK ../multiplier/compressor/comp3_2.vhd
analyze -f vhdl -lib WORK ../multiplier/shift_triangle/shift_triangle.vhd
analyze -f vhdl -lib WORK ../multiplier/sideCompressors/rightCompressor15_12.vhd
analyze -f vhdl -lib WORK ../multiplier/sideCompressors/rightCompressor11_8.vhd
analyze -f vhdl -lib WORK ../multiplier/sideCompressors/rightCompressor7_5.vhd
analyze -f vhdl -lib WORK ../multiplier/sideCompressors/rightCompressor4_3.vhd
analyze -f vhdl -lib WORK ../multiplier/sideCompressors/rightCompressor2_2.vhd
analyze -f vhdl -lib WORK ../multiplier/sideCompressors/leftCompressor16_12.vhd
analyze -f vhdl -lib WORK ../multiplier/sideCompressors/leftCompressor12_8.vhd
analyze -f vhdl -lib WORK ../multiplier/sideCompressors/leftCompressor7_5.vhd
analyze -f vhdl -lib WORK ../multiplier/sideCompressors/leftCompressor4_3.vhd
analyze -f vhdl -lib WORK ../multiplier/sideCompressors/leftCompressor3_2.vhd
analyze -f vhdl -lib WORK ../multiplier/layers/L17.vhd
analyze -f vhdl -lib WORK ../multiplier/layers/L13.vhd
analyze -f vhdl -lib WORK ../multiplier/layers/L9.vhd
analyze -f vhdl -lib WORK ../multiplier/layers/L6.vhd
analyze -f vhdl -lib WORK ../multiplier/layers/L4.vhd
analyze -f vhdl -lib WORK ../multiplier/layers/L3.vhd
analyze -f vhdl -lib WORK ../multiplier/booth/mux_4to1.vhd
analyze -f vhdl -lib WORK ../multiplier/booth/encoder.vhd
analyze -f vhdl -lib WORK ../multiplier/booth/pp_create.vhd
analyze -f vhdl -lib WORK ../multiplier/booth/booth_encoder.vhd
analyze -f vhdl -lib WORK ../multiplier/main_blocks/dadda_tree.vhd
analyze -f vhdl -lib WORK ../multiplier/MBE_mpy.vhd

analyze -f vhdl -lib WORK ../fpuvhdl/common/fpnormalize_fpnormalize.vhd
analyze -f vhdl -lib WORK ../fpuvhdl/common/fpround_fpround.vhd
analyze -f vhdl -lib WORK ../fpuvhdl/common/packfp_packfp.vhd
analyze -f vhdl -lib WORK ../fpuvhdl/common/unpackfp_unpackfp.vhd
analyze -f vhdl -lib WORK ../fpuvhdl/multiplier/fpmul_stage1_struct.vhd
analyze -f vhdl -lib WORK ../fpuvhdl/multiplier/fpmul_stage2_struct.vhd
analyze -f vhdl -lib WORK ../fpuvhdl/multiplier/fpmul_stage3_struct.vhd
analyze -f vhdl -lib WORK ../fpuvhdl/multiplier/fpmul_stage4_struct.vhd
analyze -f vhdl -lib WORK ../fpuvhdl/multiplier/fpmul_pipeline.vhd


# Preserve RTL names in the generated netlist
#set power_preserve_rtl_hier_names true

# Synthesize the desired architecture (-arch) of the main entity
elaborate $TOP_ENTITY -arch $ARCH -lib WORK

# Make the various instances of a component to refer to the same
# component
uniquify

link


create_clock -name $CLOCK_SIGNAL -period $CLK_PERIOD $CLK_INPUT
set_dont_touch_network $CLOCK_SIGNAL
set_clock_uncertainty $CLK_UNCERT [get_clocks $CLOCK_SIGNAL]
set_input_delay $IN_DELAY -max -clock $CLOCK_SIGNAL \
[remove_from_collection [all_inputs] $CLK_INPUT]
set_output_delay $OUT_DELAY -max -clock $CLOCK_SIGNAL [all_outputs]

set OLOAD [load_of NangateOpenCellLibrary/BUF_X4/A]
set_load $OLOAD [all_outputs]

ungroup -all -flatten

# set_implementation DW02_mult/pparch [find cell *mult*]

compile_ultra

#compile

#optimize_registers

# Report results
report_timing > $REPORT_TIMING
report_area > $REPORT_AREA

quit
