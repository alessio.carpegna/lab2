#!/usr/bin/python3

import sys
sys.path.append("/home/isa18/Documents/lab2/multiplier/pythonSimulation/remapper/commonFunctions")
sys.path.append("/home/isa18/Documents/lab2/multiplier/pythonSimulation/remapper/pythonRemappers")

from colorTable import colorTable
from createInput import createInput
from printDict import printDict
from remapper4 import remapper4

N = 2
L = 16

sumAndCarry = createInput(N, L, colorTable)

outDict = remapper4(*sumAndCarry)

printDict(outDict)
