#!/usr/bin/python3

import sys
sys.path.append("/home/isa18/Documents/lab2/multiplier/pythonSimulation/remapper/commonFunctions")
sys.path.append("/home/isa18/Documents/lab2/multiplier/pythonSimulation/remapper/pythonRemappers")

from colorTable import colorTable
from createInput import createInput
from printDict import printDict
from remapper8 import remapper8

N = 4
L = 16

sumAndCarry = createInput(N, L, colorTable)

outDict = remapper8(*sumAndCarry)

printDict(outDict)
