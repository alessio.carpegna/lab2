#!/usr/bin/python3

def remapper2(l1_sum, l1_carry):

	# Initialize output of the remapper
	remappedDict = {
		"l1_s1":[],
		"l1_s2":[]
	}

	# Input length (the carry element is supposed to have the
	# same length of the sum one)
	l1_len = len(l1_sum)

	# l1_s1
	remappedDict["l1_s1"].extend(l1_carry)
	remappedDict["l1_s1"].append(l1_sum[l1_len-1])

	# l1_s2
	remappedDict["l1_s2"].extend(l1_sum[0:l1_len-1])


	return remappedDict
