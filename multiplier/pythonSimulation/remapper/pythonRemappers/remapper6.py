#!/usr/bin/python3

def remapper6(l1_sum, l1_carry, l2_sum, l2_carry, 
		l3_sum, l3_carry):

	# Initialize output of the remapper
	remappedDict = {
		"l1_s1":[],
		"l1_s2":[],
		"l1_c":[],
		"l2_s1":[],
		"l2_s2":[],
		"l2_c":[]
	}

	# Input length (the carry element is supposed to have the
	# same length of the sum one)
	l1_len = len(l1_sum)
	l2_len = len(l2_sum)
	l3_len = len(l3_sum)

	# l1_s1
	remappedDict["l1_s1"].extend(l1_carry)
	remappedDict["l1_s1"].append(l1_sum[l1_len-1])

	# l1_s2
	remappedDict["l1_s2"].append(l1_sum[0])
	remappedDict["l1_s2"].extend(l2_carry)
	remappedDict["l1_s2"].extend(l1_sum[l1_len-3:l1_len-1])

	# l1_c
	remappedDict["l1_c"].extend(l1_sum[1:3])
	remappedDict["l1_c"].extend(l3_carry)
	remappedDict["l1_c"].extend(l1_sum[l1_len-5:l1_len-3])
	remappedDict["l1_c"].append(l2_sum[l2_len-1])

	# l2_s1
	remappedDict["l2_s1"].append(l2_sum[0])
	remappedDict["l2_s1"].extend(l1_sum[3:l1_len-5])
	remappedDict["l2_s1"].extend(l2_sum[l2_len-3:l2_len-1])

	# l2_s2
	remappedDict["l2_s2"].extend(l2_sum[1:l2_len-3])
	remappedDict["l2_s2"].append(l3_sum[l3_len-1])

	# l2_c
	remappedDict["l2_c"].extend(l3_sum[0:l3_len-1])


	return remappedDict
