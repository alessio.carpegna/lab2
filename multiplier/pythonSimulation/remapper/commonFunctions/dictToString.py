#!/usr/bin/python3

def dictToString(inDict):

	# Initialuze output to empty string
	outString = ""

	# Loop over the dictionary
	for binList in inDict.values():

		# Convert list to string
		binStr = "".join(map(str,binList))

		# Convert binary string to int
		intValue = int(binStr, 2)

		# Append the obtained int to the output string
		outString += str(intValue) + " "

	outString = outString[:-1] + "\n"

	return outString;
