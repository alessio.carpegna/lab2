#!/usr/bin/python3

from createInputForFile import createInputForFile

def inputFileGeneration(input_filename, desired_N, N, L):

	f = open(input_filename, "w")

	for i in range(0, desired_N):
		f.write(createInputForFile(N, L))
		
	f.close()
