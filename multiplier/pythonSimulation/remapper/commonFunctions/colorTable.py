#!/usr/bin/python3

colorTable = [
	"\033[38;5;196m",	# bright red
	"\033[38;5;10m",	# green
	"\033[38;5;27m",	# blue
	"\033[38;5;211m",	# pink
	"\033[38;5;52m",	# dark red
	"\033[38;5;11m",	# yellow
	"\033[38;5;14m",	# light blue
	"\033[38;5;56m",	# purple
	"\033[0m"		# uncolor
]
