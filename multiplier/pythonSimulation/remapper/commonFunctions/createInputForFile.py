#!/usr/bin/python3

from random import randrange

def createInputForFile(N,L):

	outString = ""

	for i in range(0,int(N/2)):
		sumRand = randrange(0, 2**L)
		carryRand = randrange(0,2**L)
		outString += str(sumRand) + " " + str(carryRand) + " "
		L = L-4

	outString = outString[:-1] + "\n"

	return outString
