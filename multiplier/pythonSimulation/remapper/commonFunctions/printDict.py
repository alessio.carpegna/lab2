#!/usr/bin/python3

def printDict(inDict):

	# Initialize the number of spaces
	tabs = 0;

	# Print all the elements of the dictionary with a proper amount of
	# trailing spaces
	for element in inDict.keys():
		outLine = tabs*"\t" + "\t".join(map(str,inDict[element]))
		print(outLine)
		tabs = tabs + 1	
