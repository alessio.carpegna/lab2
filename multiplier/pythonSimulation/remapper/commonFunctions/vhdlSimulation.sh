#!/usr/bin/bash

HERE="`pwd`"
SIM_DIR="/home/isa18/Documents/lab2/multiplier/test_componenti/remappers"
TCL_SCRIPT="remapper8.do"
INIT_SCRIPT="/software/scripts/init_msim6.2g"

cd $SIM_DIR

source $INIT_SCRIPT

vsim -c -do $TCL_SCRIPT

rm -rf work vsim.wlf transcript

cd $HERE
