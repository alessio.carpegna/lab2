#!/usr/bin/python3

from colorTable import colorTable

def createInput(N, L, colorTable):

	completeList = []

	for i in range(0,N,):
		
		sumList = []
		carryList = []

		for j in range(L-1,-1,-1):

			# Assign the color
			sumElement = colorTable[i]
			carryElement = colorTable[i+N]
			
			sumElement += "s" + str(j)
			carryElement += "c" + str(j)
			
			# Remove the color
			sumElement += colorTable[-1]
			carryElement += colorTable[-1]

			# Append the two elements
			sumList.append(sumElement)
			carryList.append(carryElement)
		
		completeList.append(sumList)
		completeList.append(carryList)

		L = L-4

	return completeList
