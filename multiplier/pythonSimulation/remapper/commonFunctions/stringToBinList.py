#!/usr/bin/python3

def stringToBinList(inString, L):

	# Initializze the output list
	binList = []

	# List of int in the line read from file
	intList = inString.split()

	# Loop over the integers in line in couples
	for i in range(1, len(intList), 2):
		
		# Convert on a decreasing amount of bits
		binFormat = "{0:0" + str(L) + "b}"

		# Convert integer to binary
		binSum = binFormat.format(int(intList[i-1]))
		binCarry = binFormat.format(int(intList[i]))

		# Convert binary to list
		listSum = list(binSum)
		listCarry = list(binCarry)

		# Prepare the output list
		binList.append(listSum)
		binList.append(listCarry)

		L = L-4

	return binList
