#!/usr/bin/python3

import sys
sys.path.append("/home/isa18/Documents/lab2/multiplier/pythonSimulation/remapper/commonFunctions")
sys.path.append("/home/isa18/Documents/lab2/multiplier/pythonSimulation/remapper/pythonRemappers")

from dictToString import dictToString
from stringToBinList import stringToBinList
from remapper2 import remapper2


def pySim2(input_filename, output_filename, L):

	f_in = open(input_filename, "r")
	f_out = open(output_filename, "w")

	for line in f_in:
	
		# Convert the line in the format expected by remapper8
		binList = stringToBinList(line,L)

		# Call the python remapper
		outDict = remapper2(*binList)

		# Convert back the results in order to write them on filr
		intString = dictToString(outDict)

		# Write the results on file
		f_out.write(intString)


	f_in.close()
	f_out.close()
