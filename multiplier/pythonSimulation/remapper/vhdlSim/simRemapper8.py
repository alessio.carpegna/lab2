#!/usr/bin/python3

import sys
sys.path.append("/home/isa18/Documents/lab2/\
multiplier/pythonSimulation/remapper/pythonRemappers/")
sys.path.append("/home/isa18/Documents/lab2/\
multiplier/pythonSimulation/remapper/commonFunctions")

from inputFileGeneration import inputFileGeneration
from pySim8 import pySim8
from subprocess import run as bash

vhdlSimScript = "/home/isa18/Documents/lab2/\
multiplier/pythonSimulation/remapper/commonFunctions/vhdlSimulation.sh"

vhdlResults = "/home/isa18/Documents/lab2/multiplier/\
test_componenti/remappers/remapper8_output.txt"

input_filename = "../inputFiles/remapper8_input.txt"
output_filename = "../results/remapper8_output.txt"
desired_N = 100
N = 8
L = 19

inputFileGeneration(input_filename, desired_N, N, L)

pySim8(input_filename, output_filename, L)

bash(vhdlSimScript, shell=True, check=True)

command = "diff " + output_filename + " " + vhdlResults
bash(command, shell=True, check=True)
