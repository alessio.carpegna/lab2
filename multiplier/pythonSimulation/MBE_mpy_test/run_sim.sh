#!/usr/bin/bash

# A questo script passo come parametri tutti i file necessari alla simulazione. Essi verrano poi passati allo script python che genererà coopie di numeri casuali e ne calcolerà
# il prodotto corretto e scriverà il tutti su due file. Il python genererà inoltre uno script modelsim "simulation.do" in cui sono presenti tutti i comandi necessari alla simulazione.


# simulation.do sarà poi lanciato da questo script

# lancio lo script python passandogli tutti i percorsi dei file in VHDL necessari per simulare
# Esso genera:
#	1) uno script simulation.do che servirà poi per lanciare la simulazione
#	2) un file contenente coppie di numeri in binario unsigned casuali il cui numero di bit si fissa nello script python. Il nome e il percorso di questo file è
#	   anch'esso fissato dentro lo script.
#	3) un file contenente i risultati delle moltiplicazioni tra i numeri generati dal precedente file anch'essi in binario su un numero di bit fissato nello
#	   script (per noi è 64 bit perchè il nostro moltiplicatore uscirà sempre su 64 bit). Questo file servirà poi come riferimento per verificare la correttezza dei risultati
#	   del moltiplicatore in analisi.


./MBE_mpy_test.py $@

# Avvio modelsim

source /software/scripts/init_msim6.2g

# Creo cartella work

vlib work

# Lancio su Modelsim lo script sopra generato
vsim -c -do simulation.do

# A questo punto confronto i risultati del VHDL e del reference file che il python ha creato

reference_file="expected_results.txt"
vhdl_results_file="results_MBE.txt"
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0;m'
if [[ `diff $reference_file $vhdl_results_file` == "" ]]
then
	echo -e "${GREEN}Device under test works properly\n${GRENN}GOOD JOB!!${NC} "
else
	echo -e "${RED}DEVICE UNDER TEST DOES NOT WORK PROPERLY\n${RED}CHECK AGAIN YOU DESIGN!!${NC}"
fi
