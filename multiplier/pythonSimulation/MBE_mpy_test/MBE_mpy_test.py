#!/usr/bin/python3

# Script che serve per testare il corretto funzionamento dell'MBE multiplier progettato su un'ampia gamma di valori per gli operandi.
# Esso genera coppie di numeri casuali, le scrive su file e ne calcola il prodotto scrivendo su un altro file per ogni coppia di valori
# generati.
# A questo punto lo script compila il testbench in VHDL il quale prende il file in cui sono state scritte le coppie di ingressi casuali e scrive
# su un ulteriore file i risultati che il componente produce. A questo punto il file di risultati prodotti dal python e quelli dal componente
# hardware sono confrontati e se sono identici il moltiplicatore risulta funzionante.

import sys
import random 
import os
import subprocess


n_bit=32				#numero di bit dei numeri da generare
bits_of_the_product= 64			#numero di bit su cui i risultati delle moltiplicazioni saranno scirtti in binario. Siccome il nostro moltiplicatore esce in ogni
					# caso su 64 bit non toccare questo valore. In caso di riutilizzo di questo script in un altro progetto modificare a piacere questo numero


starting_dir = os.getcwd() 		#mi salvo la directory da dove parto
n_gen = 1000				#numero di coppie di operandi generati
tb_results_location = "./"		#dove salvare i risultati del testbench appena usato
exp_results_location="./"
exp_results = "expected_results.txt"
input_file = "input_couples.txt"	#numeri generati
modelsim_file = "simulation.do"		#file generato per lanciare la simulazione su Modelsim



# Il primo argomento da passare allo script è il percorso completo (con anche il nome del file) del testbench
# gli altri argomenti sono i file necessari alla compilazione del testbench, ovvero tutti i file che componegono il progetto

# ESEMPIO:	./questo_script.py /generic_path/testbench_name.vhd /generic_path/file1.vhd /generic_path/file2.vhd


tb_file_name = sys.argv[1].split("/")[-1]		#salvo il nome del file testbench
tb_path = "/".join(sys.argv[1].split("/")[:-1])		#salvo il percorso del testbench per metterci il file di ingressi generati

#print(tb_path)
f_in = open(input_file,'w')
f_res = open(exp_results_location+exp_results,'w')
f_mod = open(modelsim_file,'w')
for i in range(0,n_gen):
	n1 = random.randint(0,(2**n_bit) -1)
	n2 = random.randint(0,(2**n_bit) -1)
	p = n1*n2
	n1 = format(n1,'0'+str(n_bit)+'b')
	n2 = format(n2,'0'+str(n_bit)+'b')
	p = format(p,'0'+str(bits_of_the_product)+'b')
	f_in.write(str(n1)+" "+str(n2)+'\n')
	f_res.write(str(p)+'\n')

mod_command=""
for k in sys.argv[1:]:
	mod_command=mod_command + "vcom -93 -work ./work "+k+"\n"


mod_command=mod_command+"vsim -t ns -novopt work.tb_mbe\n"+"run 100 us\n"+"quit -f\n"
f_mod.write(mod_command)
