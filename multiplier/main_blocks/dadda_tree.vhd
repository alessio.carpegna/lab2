library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity dadda_tree is

	port(
		-- input signals
		pp1	: in std_logic_vector(35 downto 0);
		pp2	: in std_logic_vector(36 downto 0);
		pp3	: in std_logic_vector(36 downto 0);
		pp4	: in std_logic_vector(36 downto 0);
		pp5	: in std_logic_vector(36 downto 0);
		pp6	: in std_logic_vector(36 downto 0);
		pp7	: in std_logic_vector(36 downto 0);
		pp8	: in std_logic_vector(36 downto 0);
		pp9	: in std_logic_vector(36 downto 0);
		pp10	: in std_logic_vector(36 downto 0);
		pp11	: in std_logic_vector(36 downto 0);
		pp12	: in std_logic_vector(36 downto 0);
		pp13	: in std_logic_vector(36 downto 0);
		pp14	: in std_logic_vector(36 downto 0);
		pp15	: in std_logic_vector(36 downto 0);
		pp16	: in std_logic_vector(35 downto 0);
		pp17	: in std_logic_vector(33 downto 0);
		
		-- output signals
		product	: out std_logic_vector(63 downto 0)
	);
end entity dadda_tree;

architecture structure of dadda_tree is

	component shift_triangle
	
		port(
		
			--Ingressi 
			pp1 			: in std_logic_vector(35 downto 0);
			pp2,pp3,pp4,pp5 	: in std_logic_vector(36 downto 0);
			pp6,pp7,pp8,pp9		: in std_logic_vector(36 downto 0);
			pp10,pp11,pp12,pp13	: in std_logic_vector(36 downto 0);
			pp14,pp15		: in std_logic_vector(36 downto 0);
			pp16			: in std_logic_vector(35 downto 0);
			pp17			: in std_logic_vector(33 downto 0);
			
			--Uscite
			ppout1,ppout2		: out std_logic_vector(63 downto 0);
			ppout3			: out std_logic_vector(60 downto 0);
			ppout4			: out std_logic_vector(56 downto 0);
			ppout5			: out std_logic_vector(52 downto 0);
			ppout6			: out std_logic_vector(48 downto 0);
			ppout7			: out std_logic_vector(44 downto 0);
			ppout8			: out std_logic_vector(40 downto 0);
			ppout9 			: out std_logic_vector(36 downto 0);
			ppout10			: out std_logic_vector(32 downto 0);
			ppout11			: out std_logic_vector(28 downto 0);
			ppout12			: out std_logic_vector(24 downto 0);
			ppout13 		: out std_logic_vector(20 downto 0);
			ppout14			: out std_logic_vector(16 downto 0);
			ppout15			: out std_logic_vector(12 downto 0);
			ppout16			: out std_logic_vector(8 downto 0);
			ppout17			: out std_logic_vector(5 downto 0)
		);
	end component;


	component L17
	
		port(
			pp1		: in std_logic_vector(63 downto 0);
			pp2		: in std_logic_vector(63 downto 0);
			pp3		: in std_logic_vector(62 downto 2);
			pp4		: in std_logic_vector(60 downto 4);
			pp5		: in std_logic_vector(58 downto 6);
			pp6		: in std_logic_vector(56 downto 8);
			pp7		: in std_logic_vector(54 downto 10);
			pp8		: in std_logic_vector(52 downto 12);
			pp9		: in std_logic_vector(50 downto 14);
			pp10	: in std_logic_vector(48 downto 16);
			pp11	: in std_logic_vector(46 downto 18);
			pp12	: in std_logic_vector(44 downto 20);
			pp13	: in std_logic_vector(42 downto 22);
			pp14	: in std_logic_vector(40 downto 24);
			pp15	: in std_logic_vector(38 downto 26);
			pp16	: in std_logic_vector(36 downto 28);
			pp17	: in std_logic_vector(35 downto 30);
			
			L17_out1	: out std_logic_vector(63 downto 0);
			L17_out2	: out std_logic_vector(63 downto 0);
			L17_out3	: out std_logic_vector(62 downto 2);
			L17_out4	: out std_logic_vector(60 downto 4);
			L17_out5	: out std_logic_vector(58 downto 6);
			L17_out6	: out std_logic_vector(56 downto 8);
			L17_out7	: out std_logic_vector(54 downto 10);
			L17_out8	: out std_logic_vector(52 downto 12);
			L17_out9	: out std_logic_vector(50 downto 14);
			L17_out10	: out std_logic_vector(48 downto 16);
			L17_out11	: out std_logic_vector(46 downto 18);
			L17_out12	: out std_logic_vector(44 downto 20);
			L17_out13	: out std_logic_vector(43 downto 22)
		);
		
	end component;
	
	component L13
	
		port(
			L13_in1		: in std_logic_vector(63 downto 0);
			L13_in2		: in std_logic_vector(63 downto 0);
			L13_in3		: in std_logic_vector(62 downto 2);
			L13_in4		: in std_logic_vector(60 downto 4);
			L13_in5		: in std_logic_vector(58 downto 6);
			L13_in6		: in std_logic_vector(56 downto 8);
			L13_in7		: in std_logic_vector(54 downto 10);
			L13_in8		: in std_logic_vector(52 downto 12);
			L13_in9		: in std_logic_vector(50 downto 14);
			L13_in10	: in std_logic_vector(48 downto 16);
			L13_in11	: in std_logic_vector(46 downto 18);
			L13_in12	: in std_logic_vector(44 downto 20);
			L13_in13	: in std_logic_vector(43 downto 22);
			
			L13_out1	: out std_logic_vector(63 downto 0);
			L13_out2	: out std_logic_vector(63 downto 0);
			L13_out3	: out std_logic_vector(62 downto 2);
			L13_out4	: out std_logic_vector(60 downto 4);
			L13_out5	: out std_logic_vector(58 downto 6);
			L13_out6	: out std_logic_vector(56 downto 8);
			L13_out7	: out std_logic_vector(54 downto 10);
			L13_out8	: out std_logic_vector(52 downto 12);
			L13_out9	: out std_logic_vector(51 downto 14)
		);
		
	end component;
	
	component L9
	
		port(
			L9_in1		: in std_logic_vector(63 downto 0);
			L9_in2		: in std_logic_vector(63 downto 0);
			L9_in3		: in std_logic_vector(62 downto 2);
			L9_in4		: in std_logic_vector(60 downto 4);
			L9_in5		: in std_logic_vector(58 downto 6);
			L9_in6		: in std_logic_vector(56 downto 8);
			L9_in7		: in std_logic_vector(54 downto 10);
			L9_in8		: in std_logic_vector(52 downto 12);
			L9_in9		: in std_logic_vector(51 downto 14);
			
			L9_out1	: out std_logic_vector(63 downto 0);
			L9_out2	: out std_logic_vector(63 downto 0);
			L9_out3	: out std_logic_vector(62 downto 2);
			L9_out4	: out std_logic_vector(60 downto 4);
			L9_out5	: out std_logic_vector(58 downto 6);
			L9_out6	: out std_logic_vector(57 downto 8)
		);
		
	end component;
	
	component L6
	
		port(
			L6_in1		: in std_logic_vector(63 downto 0);
			L6_in2		: in std_logic_vector(63 downto 0);
			L6_in3		: in std_logic_vector(62 downto 2);
			L6_in4		: in std_logic_vector(60 downto 4);
			L6_in5		: in std_logic_vector(58 downto 6);
			L6_in6		: in std_logic_vector(57 downto 8);
			
			L6_out1		: out std_logic_vector(63 downto 0);
			L6_out2		: out std_logic_vector(63 downto 0);
			L6_out3		: out std_logic_vector(62 downto 2);
			L6_out4		: out std_logic_vector(61 downto 4)
		);
		
	end component;
	
	component L4
	
		port(
			L4_in1		: in std_logic_vector(63 downto 0);
			L4_in2		: in std_logic_vector(63 downto 0);
			L4_in3		: in std_logic_vector(62 downto 2);
			L4_in4		: in std_logic_vector(61 downto 4);
			
			L4_out1		: out std_logic_vector(63 downto 0);
			L4_out2		: out std_logic_vector(63 downto 0);
			L4_out3		: out std_logic_vector(63 downto 2)
		);
		
	end component;
	
	component L3
	
		port(
			L3_in1		: in std_logic_vector(63 downto 0);
			L3_in2		: in std_logic_vector(63 downto 0);
			L3_in3		: in std_logic_vector(63 downto 2);
			
			L3_out1		: out std_logic_vector(63 downto 0);
			L3_out2		: out std_logic_vector(63 downto 0)
		);
		
	end component;
	
	--Signal from shift_triangle to L17
	signal ppout1	: std_logic_vector(63 downto 0);
	signal ppout2	: std_logic_vector(63 downto 0);
	signal ppout3	: std_logic_vector(60 downto 0);
	signal ppout4	: std_logic_vector(56 downto 0);
	signal ppout5	: std_logic_vector(52 downto 0);
	signal ppout6	: std_logic_vector(48 downto 0);
	signal ppout7	: std_logic_vector(44 downto 0);
	signal ppout8	: std_logic_vector(40 downto 0);
	signal ppout9	: std_logic_vector(36 downto 0);
	signal ppout10	: std_logic_vector(32 downto 0);
	signal ppout11	: std_logic_vector(28 downto 0);
	signal ppout12	: std_logic_vector(24 downto 0);
	signal ppout13	: std_logic_vector(20 downto 0);
	signal ppout14	: std_logic_vector(16 downto 0);
	signal ppout15	: std_logic_vector(12 downto 0);
	signal ppout16	: std_logic_vector(8 downto 0);
	signal ppout17	: std_logic_vector(5 downto 0);
	
	-- Signals from L17 to L13
	signal L17_out1		: std_logic_vector(63 downto 0);
	signal L17_out2		: std_logic_vector(63 downto 0);
	signal L17_out3		: std_logic_vector(62 downto 2);
	signal L17_out4		: std_logic_vector(60 downto 4);
	signal L17_out5		: std_logic_vector(58 downto 6);
	signal L17_out6		: std_logic_vector(56 downto 8);
	signal L17_out7		: std_logic_vector(54 downto 10);
	signal L17_out8		: std_logic_vector(52 downto 12);
	signal L17_out9		: std_logic_vector(50 downto 14);
	signal L17_out10	: std_logic_vector(48 downto 16);
	signal L17_out11	: std_logic_vector(46 downto 18);
	signal L17_out12	: std_logic_vector(44 downto 20);
	signal L17_out13	: std_logic_vector(43 downto 22);
	
	-- Signals from L13 to L9
	signal L13_out1	: std_logic_vector(63 downto 0);
	signal L13_out2	: std_logic_vector(63 downto 0);
	signal L13_out3	: std_logic_vector(62 downto 2);
	signal L13_out4	: std_logic_vector(60 downto 4);
	signal L13_out5	: std_logic_vector(58 downto 6);
	signal L13_out6	: std_logic_vector(56 downto 8);
	signal L13_out7	: std_logic_vector(54 downto 10);
	signal L13_out8	: std_logic_vector(52 downto 12);
	signal L13_out9	: std_logic_vector(51 downto 14);
	
	-- Signals from L9 to L6
	signal L9_out1	: std_logic_vector(63 downto 0);
	signal L9_out2	: std_logic_vector(63 downto 0);
	signal L9_out3	: std_logic_vector(62 downto 2);
	signal L9_out4	: std_logic_vector(60 downto 4);
	signal L9_out5	: std_logic_vector(58 downto 6);
	signal L9_out6	: std_logic_vector(57 downto 8);
	
	-- Signals from L6 to L4
	signal L6_out1	: std_logic_vector(63 downto 0);
	signal L6_out2	: std_logic_vector(63 downto 0);
	signal L6_out3	: std_logic_vector(62 downto 2);
	signal L6_out4	: std_logic_vector(61 downto 4);
	
	-- Signals from L4 to L3
	signal L4_out1	: std_logic_vector(63 downto 0);
	signal L4_out2	: std_logic_vector(63 downto 0);
	signal L4_out3	: std_logic_vector(63 downto 2);
	
	signal L3_out1	: std_logic_vector(63 downto 0);
	signal L3_out2	: std_logic_vector(63 downto 0);
	
begin

	triangular_shape: shift_triangle
		port map(
			
			pp1 	=> pp1,
			pp2		=> pp2,
			pp3		=> pp3,
			pp4		=> pp4,
			pp5 	=> pp5,
			pp6		=> pp6,
			pp7		=> pp7,		
			pp8		=> pp8,
			pp9		=> pp9,
			pp10	=> pp10,
			pp11	=> pp11,
			pp12	=> pp12,
			pp13	=> pp13,
			pp14	=> pp14,
			pp15	=> pp15,
			pp16	=> pp16,
			pp17	=> pp17,
			
			ppout1	=> ppout1,
			ppout2	=> ppout2,
			ppout3	=> ppout3,
			ppout4	=> ppout4,
			ppout5	=> ppout5,
			ppout6	=> ppout6,
			ppout7	=> ppout7,
			ppout8	=> ppout8,
			ppout9 	=> ppout9,
			ppout10	=> ppout10,
			ppout11	=> ppout11,
			ppout12	=> ppout12,
			ppout13 => ppout13,
			ppout14	=> ppout14,
			ppout15	=> ppout15,
			ppout16	=> ppout16,
			ppout17	=> ppout17
	);

	layer17: L17 
		port map(
		
			pp1		=> ppout1,
			pp2		=> ppout2,
			pp3		=> ppout3,
			pp4		=> ppout4,
			pp5		=> ppout5,
			pp6		=> ppout6,
			pp7		=> ppout7,
			pp8		=> ppout8,
			pp9		=> ppout9,
			pp10	=> ppout10,
			pp11	=> ppout11,
			pp12	=> ppout12,
			pp13	=> ppout13,
			pp14	=> ppout14,
			pp15	=> ppout15,
			pp16	=> ppout16,
			pp17	=> ppout17,
			
			L17_out1	=> L17_out1,
			L17_out2	=> L17_out2,
			L17_out3	=> L17_out3,
			L17_out4	=> L17_out4,
			L17_out5	=> L17_out5,
			L17_out6	=> L17_out6,
			L17_out7	=> L17_out7,
			L17_out8	=> L17_out8,
			L17_out9	=> L17_out9,
			L17_out10	=> L17_out10,
			L17_out11	=> L17_out11,
			L17_out12	=> L17_out12,
			L17_out13	=> L17_out13
	);
	
	layer13: L13
		port map(
		
			L13_in1		=> L17_out1,
			L13_in2		=> L17_out2,
			L13_in3		=> L17_out3,
			L13_in4		=> L17_out4,
			L13_in5		=> L17_out5,
			L13_in6		=> L17_out6,
			L13_in7		=> L17_out7,
			L13_in8		=> L17_out8,
			L13_in9		=> L17_out9,
			L13_in10	=> L17_out10,
			L13_in11	=> L17_out11,
			L13_in12	=> L17_out12,
			L13_in13	=> L17_out13,
			
			L13_out1	=> L13_out1,
			L13_out2	=> L13_out2,
			L13_out3	=> L13_out3,
			L13_out4	=> L13_out4,
			L13_out5	=> L13_out5,
			L13_out6	=> L13_out6,
			L13_out7	=> L13_out7,
			L13_out8	=> L13_out8,
			L13_out9	=> L13_out9
	);
	
	layer9: L9 
		port map(
		
			L9_in1	=> L13_out1,
			L9_in2	=> L13_out2,
			L9_in3	=> L13_out3,
			L9_in4	=> L13_out4,
			L9_in5	=> L13_out5,
			L9_in6	=> L13_out6,
			L9_in7	=> L13_out7,
			L9_in8	=> L13_out8,
			L9_in9	=> L13_out9,
			
			L9_out1	=> L9_out1,
			L9_out2	=> L9_out2,
			L9_out3	=> L9_out3,
			L9_out4	=> L9_out4,
			L9_out5	=> L9_out5,
			L9_out6	=> L9_out6
	);
	
	layer6: L6 
		port map(
		
			L6_in1	=> L9_out1,
			L6_in2	=> L9_out2,
			L6_in3	=> L9_out3,
			L6_in4	=> L9_out4,
			L6_in5	=> L9_out5,
			L6_in6	=> L9_out6,
			
			L6_out1	=> L6_out1,
			L6_out2	=> L6_out2,
			L6_out3	=> L6_out3,
			L6_out4	=> L6_out4
	);
	
	layer4: L4 
		port map(
		
			L4_in1	=> L6_out1,
			L4_in2	=> L6_out2,
			L4_in3	=> L6_out3,
			L4_in4	=> L6_out4,
			
			L4_out1	=> L4_out1,
			L4_out2	=> L4_out2,
			L4_out3	=> L4_out3
	);
	
	layer3: L3 
		port map(
			L3_in1	=> L4_out1,
			L3_in2	=> L4_out2,
			L3_in3	=> L4_out3,
			
			L3_out1	=> L3_out1,
			L3_out2	=> L3_out2
	);
	
	-- SOMMA FINALE
	product <= std_logic_vector(unsigned(L3_out1) + unsigned(L3_out2));

end architecture structure;
