library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity MBE_mpy is

	port(
		-- input multiplicands
		a	: in std_logic_vector(31 downto 0);
		b	: in std_logic_vector(31 downto 0);
		
		-- product
		product	: out std_logic_vector(63 downto 0)
	);
end entity MBE_mpy;

architecture structure of MBE_mpy is

	component booth_encoder
		port(
			-- input signals
			b	: in std_logic_vector(31 downto 0);
			a	: in std_logic_vector(31 downto 0);

			-- output signals
			pp1	: out std_logic_vector(35 downto 0);
			pp2	: out std_logic_vector(36 downto 0);
			pp3	: out std_logic_vector(36 downto 0);
			pp4	: out std_logic_vector(36 downto 0);
			pp5	: out std_logic_vector(36 downto 0);
			pp6	: out std_logic_vector(36 downto 0);
			pp7	: out std_logic_vector(36 downto 0);
			pp8	: out std_logic_vector(36 downto 0);
			pp9	: out std_logic_vector(36 downto 0);
			pp10	: out std_logic_vector(36 downto 0);
			pp11	: out std_logic_vector(36 downto 0);
			pp12	: out std_logic_vector(36 downto 0);
			pp13	: out std_logic_vector(36 downto 0);
			pp14	: out std_logic_vector(36 downto 0);
			pp15	: out std_logic_vector(36 downto 0);
			pp16	: out std_logic_vector(35 downto 0);
			pp17	: out std_logic_vector(33 downto 0)
		);
	end component;

	component dadda_tree
	
		port(
				-- input signals
			pp1	: in std_logic_vector(35 downto 0);
			pp2	: in std_logic_vector(36 downto 0);
			pp3	: in std_logic_vector(36 downto 0);
			pp4	: in std_logic_vector(36 downto 0);
			pp5	: in std_logic_vector(36 downto 0);
			pp6	: in std_logic_vector(36 downto 0);
			pp7	: in std_logic_vector(36 downto 0);
			pp8	: in std_logic_vector(36 downto 0);
			pp9	: in std_logic_vector(36 downto 0);
			pp10	: in std_logic_vector(36 downto 0);
			pp11	: in std_logic_vector(36 downto 0);
			pp12	: in std_logic_vector(36 downto 0);
			pp13	: in std_logic_vector(36 downto 0);
			pp14	: in std_logic_vector(36 downto 0);
			pp15	: in std_logic_vector(36 downto 0);
			pp16	: in std_logic_vector(35 downto 0);
			pp17	: in std_logic_vector(33 downto 0);
			
			-- output signals
			product	: out std_logic_vector(63 downto 0)
		);
		
	end component;
	
	signal pp1	: std_logic_vector(35 downto 0);
	signal pp2	: std_logic_vector(36 downto 0);
	signal pp3	: std_logic_vector(36 downto 0);
	signal pp4	: std_logic_vector(36 downto 0);
	signal pp5	: std_logic_vector(36 downto 0);
	signal pp6	: std_logic_vector(36 downto 0);
	signal pp7	: std_logic_vector(36 downto 0);
	signal pp8	: std_logic_vector(36 downto 0);
	signal pp9	: std_logic_vector(36 downto 0);
	signal pp10	: std_logic_vector(36 downto 0);
	signal pp11	: std_logic_vector(36 downto 0);
	signal pp12	: std_logic_vector(36 downto 0);
	signal pp13	: std_logic_vector(36 downto 0);
	signal pp14	: std_logic_vector(36 downto 0);
	signal pp15	: std_logic_vector(36 downto 0);
	signal pp16	: std_logic_vector(35 downto 0);
	signal pp17	: std_logic_vector(33 downto 0);
	
begin

	booth: booth_encoder
		port map(
		
			-- inputs
			a		=> a,
			b		=> b,

			--outputs		
			
			pp1 	=> pp1,
			pp2		=> pp2,
			pp3		=> pp3,
			pp4		=> pp4,
			pp5 	=> pp5,
			pp6		=> pp6,
			pp7		=> pp7,		
			pp8		=> pp8,
			pp9		=> pp9,
			pp10	=> pp10,
			pp11	=> pp11,
			pp12	=> pp12,
			pp13	=> pp13,
			pp14	=> pp14,
			pp15	=> pp15,
			pp16	=> pp16,
			pp17	=> pp17	
	);

	dadda: dadda_tree 
		port map(
		
			pp1 	=> pp1,
			pp2		=> pp2,
			pp3		=> pp3,
			pp4		=> pp4,
			pp5 	=> pp5,
			pp6		=> pp6,
			pp7		=> pp7,		
			pp8		=> pp8,
			pp9		=> pp9,
			pp10	=> pp10,
			pp11	=> pp11,
			pp12	=> pp12,
			pp13	=> pp13,
			pp14	=> pp14,
			pp15	=> pp15,
			pp16	=> pp16,
			pp17	=> pp17,
			
			product	=> product
	);

end architecture structure;
