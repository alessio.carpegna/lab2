library ieee;
use ieee.std_logic_1164.all;


entity shift_triangle is
	port(

		--Ingressi 
		pp1 		: in std_logic_vector(35 downto 0);
		pp2 		: in std_logic_vector(36 downto 0);		
		pp3 		: in std_logic_vector(38 downto 2);
		pp4 		: in std_logic_vector(40 downto 4);
		pp5 		: in std_logic_vector(42 downto 6);
		pp6 		: in std_logic_vector(44 downto 8);
		pp7 		: in std_logic_vector(46 downto 10);
		pp8 		: in std_logic_vector(48 downto 12);
		pp9		: in std_logic_vector(50 downto 14);
		pp10 		: in std_logic_vector(52 downto 16);
		pp11 		: in std_logic_vector(54 downto 18);
		pp12 		: in std_logic_vector(56 downto 20);
		pp13		: in std_logic_vector(58 downto 22);
		pp14 		: in std_logic_vector(60 downto 24);
		pp15		: in std_logic_vector(62 downto 26);
		pp16		: in std_logic_vector(63 downto 28);
		pp17		: in std_logic_vector(63 downto 30);

		--Uscite
		ppout1		: out std_logic_vector(63 downto 0);
		ppout2		: out std_logic_vector(63 downto 0);
		ppout3		: out std_logic_vector(62 downto 2);
		ppout4		: out std_logic_vector(60 downto 4);
		ppout5		: out std_logic_vector(58 downto 6);
		ppout6		: out std_logic_vector(56 downto 8);
		ppout7		: out std_logic_vector(54 downto 10);
		ppout8		: out std_logic_vector(52 downto 12);
		ppout9		: out std_logic_vector(50 downto 14);
		ppout10		: out std_logic_vector(48 downto 16);
		ppout11		: out std_logic_vector(46 downto 18);
		ppout12		: out std_logic_vector(44 downto 20);
		ppout13		: out std_logic_vector(42 downto 22);
		ppout14		: out std_logic_vector(40 downto 24);
		ppout15		: out std_logic_vector(38 downto 26);
		ppout16		: out std_logic_vector(36 downto 28);
		ppout17		: out std_logic_vector(35 downto 30)
	);
end entity shift_triangle;

architecture behaviour of shift_triangle is

begin

	ppout1 <=	pp16(63)		&
			pp15(62 downto 61)	&
			pp14(60 downto 59)	&
			pp13(58 downto 57)	&
			pp12(56 downto 55)	&
			pp11(54 downto 53)	&
			pp10(52 downto 51)	&
			pp9(50 downto 49)	&
			pp8(48 downto 47)	&
			pp7(46 downto 45)	&
			pp6(44 downto 43)	&
			pp5(42 downto 41)	&
			pp4(40 downto 39)	&
			pp3(38 downto 37)	&
			pp2(36)			&
			pp1(35 downto 0);
			
	ppout2 <= 	pp17(63)		&
			pp16(62 downto 61)	&
			pp15(60 downto 59)	&
			pp14(58 downto 57)	&
			pp13(56 downto 55)	&
			pp12(54 downto 53)	&
			pp11(52 downto 51)	&
			pp10(50 downto 49)	&
			pp9(48 downto 47)	&
			pp8(46 downto 45)	&
			pp7(44 downto 43)	&
			pp6(42 downto 41)	&
			pp5(40 downto 39)	&
			pp4(38 downto 37)	&
			pp3(36)			&
			pp2(35 downto 0);

	ppout3 <=	pp17(62 downto 61)	&
			pp16(60 downto 59)	&
			pp15(58 downto 57)	&
			pp14(56 downto 55)	&
			pp13(54 downto 53)	&
			pp12(52 downto 51)	&
			pp11(50 downto 49)	&
			pp10(48 downto 47)	&
			pp9(46 downto 45)	&
			pp8(44 downto 43)	&
			pp7(42 downto 41)	&
			pp6(40 downto 39)	&
			pp5(38 downto 37)	&
			pp4(36)			&
			pp3(35 downto 2);

	ppout4 <=	pp17(60 downto 59)	&
			pp16(58 downto 57)	&
			pp15(56 downto 55)	&
			pp14(54 downto 53)	&
			pp13(52 downto 51)	&
			pp12(50 downto 49)	&
			pp11(48 downto 47)	&
			pp10(46 downto 45)	&
			pp9(44 downto 43)	&
			pp8(42 downto 41)	&
			pp7(40 downto 39)	&
			pp6(38 downto 37)	&
			pp5(36)			&
			pp4(35 downto 4);	

	ppout5 <=	pp17(58 downto 57)	&
			pp16(56 downto 55)	&
			pp15(54 downto 53)	&
			pp14(52 downto 51)	&
			pp13(50 downto 49)	&
			pp12(48 downto 47)	&
			pp11(46 downto 45)	&
			pp10(44 downto 43)	&
			pp9(42 downto 41)	&
			pp8(40 downto 39)	&
			pp7(38 downto 37)	&
			pp6(36)			&
			pp5(35 downto 6);

	ppout6 <=	pp17(56 downto 55)	&
			pp16(54 downto 53)	&
			pp15(52 downto 51)	&
			pp14(50 downto 49)	&
			pp13(48 downto 47)	&
			pp12(46 downto 45)	&
			pp11(44 downto 43)	&
			pp10(42 downto 41)	&
			pp9(40 downto 39)	&
			pp8(38 downto 37)	&
			pp7(36)			&
			pp6(35 downto 8);

	ppout7 <=	pp17(54 downto 53)	&
			pp16(52 downto 51)	&
			pp15(50 downto 49)	&
			pp14(48 downto 47)	&
			pp13(46 downto 45)	&
			pp12(44 downto 43)	&
			pp11(42 downto 41)	&
			pp10(40 downto 39)	&
			pp9(38 downto 37)	&
			pp8(36)			&
			pp7(35 downto 10);

	ppout8 <=	pp17(52 downto 51)	&
			pp16(50 downto 49)	&
			pp15(48 downto 47)	&
			pp14(46 downto 45)	&
			pp13(44 downto 43)	&
			pp12(42 downto 41)	&
			pp11(40 downto 39)	&
			pp10(38 downto 37)	&
			pp9(36)			&
			pp8(35 downto 12);

	ppout9 <=	pp17(50 downto 49)	&
			pp16(48 downto 47)	&
			pp15(46 downto 45)	&
			pp14(44 downto 43)	&
			pp13(42 downto 41)	&
			pp12(40 downto 39)	&
			pp11(38 downto 37)	&
			pp10(36)		&
			pp9(35 downto 14);

	ppout10 <=	pp17(48 downto 47)	&
			pp16(46 downto 45)	&
			pp15(44 downto 43)	&
			pp14(42 downto 41)	&
			pp13(40 downto 39)	&
			pp12(38 downto 37)	&
			pp11(36)		&
			pp10(35 downto 16);

	ppout11 <=	pp17(46 downto 45)	&
			pp16(44 downto 43)	&
			pp15(42 downto 41)	&
			pp14(40 downto 39)	&
			pp13(38 downto 37)	&
			pp12(36)		&
			pp11(35 downto 18);

	ppout12 <=	pp17(44 downto 43)	&
			pp16(42 downto 41)	&
			pp15(40 downto 39)	&
			pp14(38 downto 37)	&
			pp13(36)		&
			pp12(35 downto 20);

	ppout13 <=	pp17(42 downto 41)	&
			pp16(40 downto 39)	&
			pp15(38 downto 37)	&
			pp14(36)		&
			pp13(35 downto 22);

	ppout14 <=	pp17(40 downto 39)	&
			pp16(38 downto 37)	&
			pp15(36)		&
			pp14(35 downto 24);

	ppout15 <=	pp17(38 downto 37)	&
			pp16(36)		&
			pp15(35 downto 26);

	ppout16 <=	pp17(36)		&
			pp16(35 downto 28);

	ppout17 <=	pp17(35 downto 30);




end architecture behaviour;

