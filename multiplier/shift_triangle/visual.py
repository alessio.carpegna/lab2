#!/usr/bin/python3

count_lines = 0
space_number = 1
f = open('triangle_results.txt')
for line in f:
	if count_lines == 0:
		print(line)
	if count_lines == 1:
		print(line)
	if count_lines == 2:
		print(' '*space_number + line)
	if (count_lines > 2) and (count_lines < 16 ):
		space_number = space_number + 2
		print(' '*space_number + line)	       
	if count_lines == 16:
		space_number = space_number +1
		print(' '*space_number + line)
	count_lines = count_lines + 1	
		
