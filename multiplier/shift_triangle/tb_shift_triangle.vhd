library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;
use std.textio.all;
entity tb_shift_triangle is
end entity tb_shift_triangle;

architecture behaviour of tb_shift_triangle is
    
    component shift_triangle 
        
        port(
        
	        --Ingressi 
	        pp1 			: in std_logic_vector(35 downto 0);
	        pp2,pp3,pp4,pp5 	: in std_logic_vector(36 downto 0);
	        pp6,pp7,pp8,pp9		: in std_logic_vector(36 downto 0);
	        pp10,pp11,pp12,pp13	: in std_logic_vector(36 downto 0);
	        pp14,pp15		: in std_logic_vector(36 downto 0);
	        pp16			: in std_logic_vector(35 downto 0);
	        pp17			: in std_logic_vector(33 downto 0);
	        --Uscite
	        ppout1,ppout2		: out std_logic_vector(63 downto 0);
	        ppout3			: out std_logic_vector(60 downto 0);
	        ppout4			: out std_logic_vector(56 downto 0);
       	        ppout5			: out std_logic_vector(52 downto 0);
	        ppout6			: out std_logic_vector(48 downto 0);
	        ppout7			: out std_logic_vector(44 downto 0);
	        ppout8			: out std_logic_vector(40 downto 0);
	        ppout9 			: out std_logic_vector(36 downto 0);
       	        ppout10			: out std_logic_vector(32 downto 0);
	        ppout11			: out std_logic_vector(28 downto 0);
	        ppout12			: out std_logic_vector(24 downto 0);
	        ppout13 		: out std_logic_vector(20 downto 0);
	        ppout14			: out std_logic_vector(16 downto 0);
	        ppout15			: out std_logic_vector(12 downto 0);
	        ppout16			: out std_logic_vector(8 downto 0);
	        ppout17			: out std_logic_vector(5 downto 0)
        );
end component shift_triangle;


-- Ingressi
signal pp1_test                                  : std_logic_vector(35 downto 0);
signal pp2_test                                  : std_logic_vector(36 downto 0);
signal pp3_test,pp4_test,pp5_test,pp6_test       : std_logic_vector(36 downto 0);
signal pp7_test,pp8_test,pp9_test,pp10_test      : std_logic_vector(36 downto 0);
signal pp11_test,pp12_test,pp13_test,pp14_test   : std_logic_vector(36 downto 0);
signal pp15_test                                 : std_logic_vector(36 downto 0);
signal pp16_test                                 : std_logic_vector(35 downto 0);
signal pp17_test                                 : std_logic_vector(33 downto 0);

-- Uscite
signal test_ppout1,test_ppout2                   : std_logic_vector(63 downto 0);
signal test_ppout3                               : std_logic_vector(60 downto 0);
signal test_ppout4                               : std_logic_vector(56 downto 0);
signal test_ppout5                               : std_logic_vector(52 downto 0);
signal test_ppout6                               : std_logic_vector(48 downto 0);
signal test_ppout7                               : std_logic_vector(44 downto 0); 
signal test_ppout8                               : std_logic_vector(40 downto 0);
signal test_ppout9                               : std_logic_vector(36 downto 0);
signal test_ppout10                              : std_logic_vector(32 downto 0);
signal test_ppout11                              : std_logic_vector(28 downto 0);
signal test_ppout12                              : std_logic_vector(24 downto 0);
signal test_ppout13                              : std_logic_vector(20 downto 0);
signal test_ppout14                              : std_logic_vector(16 downto 0);
signal test_ppout15                              : std_logic_vector(12 downto 0);
signal test_ppout16                              : std_logic_vector(8 downto 0);
signal test_ppout17                              : std_logic_vector(5 downto 0);

file outFile : text open write_mode is "triangle_results.txt";

begin
      
      
     pp1_test    <= "100000000000000000000000000000000001";
     pp2_test    <= "1100000000000000000000000000000000000";
     pp3_test    <= "1100000000000000000000000000000000000";
     pp4_test    <= "1100000000000000000000000000000000000";
     pp5_test    <= "1100000000000000000000000000000000000";
     pp6_test    <= "1100000000000000000000000000000000000";
     pp7_test    <= "1100000000000000000000000000000000000";
     pp8_test    <= "1100000000000000000000000000000000000";
     pp9_test    <= "1100000000000000000000000000000000000";
     pp10_test   <= "1100000000000000000000000000000000000";
     pp11_test   <= "1100000000000000000000000000000000000";
     pp12_test   <= "1100000000000000000000000000000000000";
     pp13_test   <= "1100000000000000000000000000000000000";
     pp14_test   <= "1100000000000000000000000000000000000";
     pp15_test   <= "1100000000000000000000000000000000000";
     pp16_test   <= "100000000000000000000000000000000000";
     pp17_test   <= "0000000000000000000000000000000000";
     
     
     
     
        
DUT : shift_triangle port map(
         pp1       => pp1_test,
         pp2       => pp2_test,
         pp3       => pp3_test,
         pp4       => pp4_test,
         pp5       => pp5_test,
         pp6       => pp6_test,
         pp7       => pp7_test,
         pp8       => pp8_test,
         pp9       => pp9_test,
         pp10      => pp10_test,
         pp11      => pp11_test,
         pp12      => pp12_test,
         pp13      => pp13_test,
         pp14      => pp14_test,
         pp15      => pp15_test,
         pp16      => pp16_test,
         pp17      => pp17_test,
         ppout1    => test_ppout1,
         ppout2    => test_ppout2,
         ppout3    => test_ppout3,
         ppout4    => test_ppout4,
         ppout5    => test_ppout5,
         ppout6    => test_ppout6,
         ppout7    => test_ppout7,
         ppout8    => test_ppout8,
         ppout9    => test_ppout9,
         ppout10   => test_ppout10,
         ppout11   => test_ppout11,
         ppout12   => test_ppout12,
         ppout13   => test_ppout13,
         ppout14   => test_ppout14,
         ppout15   => test_ppout15,
         ppout16   => test_ppout16,
         ppout17   => test_ppout17);
        
        
        write_results : process
           
           variable outLine : line;
           
           begin
           wait for 5 ns;    
           write(outLine,test_ppout1);
           writeline(outFile,outLine);
           
           write(outLine,test_ppout2);
           writeline(outFile,outline);
           
           write(outLine,test_ppout3);
           writeline(outFile,outline);
           
           write(outLine,test_ppout4);
           writeline(outFile,outline);
           
           write(outLine,test_ppout5);
           writeline(outFile,outline);
           
           write(outLine,test_ppout6);
           writeline(outFile,outline);
           
           write(outLine,test_ppout7);
           writeline(outFile,outline);
           
           write(outLine,test_ppout8);
           writeline(outFile,outline);
           
           write(outLine,test_ppout9);
           writeline(outFile,outline);
           
           write(outLine,test_ppout10);
           writeline(outFile,outline);
           
           write(outLine,test_ppout11);
           writeline(outFile,outline);
           
           write(outLine,test_ppout12);
           writeline(outFile,outline);
           
           write(outLine,test_ppout13);
           writeline(outFile,outline);
           
           write(outLine,test_ppout14);
           writeline(outFile,outline);
           
           write(outLine,test_ppout15);
           writeline(outFile,outline);
           
           write(outLine,test_ppout16);
           writeline(outFile,outline);
           
           write(outLine,test_ppout17);
           writeline(outFile,outline);
           
           wait;
       end process write_results; 
end architecture behaviour;
    
    
