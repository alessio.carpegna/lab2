library ieee;
use ieee.std_logic_1164.all;

entity L17_tb is
end entity L17_tb;

architecture test of L17_tb is

	component L17 is
		port(
			-- partial product as inputs
			pp1		: in std_logic_vector(63 downto 0);
			pp2		: in std_logic_vector(63 downto 0);
			pp3		: in std_logic_vector(62 downto 2);
			pp4		: in std_logic_vector(60 downto 4);
			pp5		: in std_logic_vector(58 downto 6);
			pp6		: in std_logic_vector(56 downto 8);
			pp7		: in std_logic_vector(54 downto 10);
			pp8		: in std_logic_vector(52 downto 12);
			pp9		: in std_logic_vector(50 downto 14);
			pp10		: in std_logic_vector(48 downto 16);
			pp11		: in std_logic_vector(46 downto 18);
			pp12		: in std_logic_vector(44 downto 20);
			pp13		: in std_logic_vector(42 downto 22);
			pp14		: in std_logic_vector(40 downto 24);
			pp15		: in std_logic_vector(38 downto 26);
			pp16		: in std_logic_vector(36 downto 28);
			pp17		: in std_logic_vector(35 downto 30);
			
			L17_out1	: out std_logic_vector(63 downto 0);
			L17_out2	: out std_logic_vector(63 downto 0);
			L17_out3	: out std_logic_vector(62 downto 2);
			L17_out4	: out std_logic_vector(60 downto 4);
			L17_out5	: out std_logic_vector(58 downto 6);
			L17_out6	: out std_logic_vector(56 downto 8);
			L17_out7	: out std_logic_vector(54 downto 10);
			L17_out8	: out std_logic_vector(52 downto 12);
			L17_out9	: out std_logic_vector(50 downto 14);
			L17_out10	: out std_logic_vector(48 downto 16);
			L17_out11	: out std_logic_vector(46 downto 18);
			L17_out12	: out std_logic_vector(44 downto 20);
			L17_out13	: out std_logic_vector(43 downto 22)
		);
	end component L17;


	signal pp1		: std_logic_vector(63 downto 0);
	signal pp2		: std_logic_vector(63 downto 0);
	signal pp3		: std_logic_vector(62 downto 2);
	signal pp4		: std_logic_vector(60 downto 4);
	signal pp5		: std_logic_vector(58 downto 6);
	signal pp6		: std_logic_vector(56 downto 8);
	signal pp7		: std_logic_vector(54 downto 10);
	signal pp8		: std_logic_vector(52 downto 12);
	signal pp9		: std_logic_vector(50 downto 14);
	signal pp10		: std_logic_vector(48 downto 16);
	signal pp11		: std_logic_vector(46 downto 18);
	signal pp12		: std_logic_vector(44 downto 20);
	signal pp13		: std_logic_vector(42 downto 22);
	signal pp14		: std_logic_vector(40 downto 24);
	signal pp15		: std_logic_vector(38 downto 26);
	signal pp16		: std_logic_vector(36 downto 28);
	signal pp17		: std_logic_vector(35 downto 30);
			
	signal L17_out1		: std_logic_vector(63 downto 0);
	signal L17_out2		: std_logic_vector(63 downto 0);
	signal L17_out3		: std_logic_vector(62 downto 2);
	signal L17_out4		: std_logic_vector(60 downto 4);
	signal L17_out5		: std_logic_vector(58 downto 6);
	signal L17_out6		: std_logic_vector(56 downto 8);
	signal L17_out7		: std_logic_vector(54 downto 10);
	signal L17_out8		: std_logic_vector(52 downto 12);
	signal L17_out9		: std_logic_vector(50 downto 14);
	signal L17_out10	: std_logic_vector(48 downto 16);
	signal L17_out11	: std_logic_vector(46 downto 18);
	signal L17_out12	: std_logic_vector(44 downto 20);
	signal L17_out13	: std_logic_vector(43 downto 22);


begin


	pp1 <= "1111111111111111111111111111100000000000000000000000000000000001";
	pp2 <= "0000000000000000000000000000100000000000000000000000000000000000";
	pp3 <= "0000000000000000000000000000000000000000000000000000000000000";
	pp4 <= "000000000000000000000000000000000000000000000000000000000";
	pp5 <= "00000000000000000000000000000000000000000000000000000";
	pp6 <= "0000000000000000000000000000000000000000000000000";
	pp7 <= "000000000000000000000000000000000000000000000";
	pp8 <= "00000000000000000000000000000000000000000";
	pp9 <= "0000000000000000000000000000000000000";
	pp10 <= "000000000000000000000000000000000";
	pp11 <= "00000000000000000000000000000";
	pp12 <= "0000000000000000000000000";
	pp13 <= "000000000000000000000";
	pp14 <= "00000000000000000";
	pp15 <= "0000000000000";
	pp16 <= "000000000";
	pp17 <= "000000";



	DUT : L17
		port map(
			pp1		=> pp1,
			pp2		=> pp2,	
			pp3		=> pp3,	
			pp4		=> pp4,	
			pp5		=> pp5,	
			pp6		=> pp6,	
			pp7		=> pp7,	
			pp8		=> pp8,	
			pp9		=> pp9,	
			pp10		=> pp10,	
			pp11		=> pp11,
			pp12		=> pp12,
			pp13		=> pp13,
			pp14		=> pp14,
			pp15		=> pp15,
			pp16		=> pp16,
			pp17		=> pp17,


			L17_out1 	=> L17_out1,
			L17_out2	=> L17_out2, 	
			L17_out3	=> L17_out3, 
			L17_out4	=> L17_out4, 
			L17_out5	=> L17_out5, 
			L17_out6	=> L17_out6, 
			L17_out7	=> L17_out7, 
			L17_out8	=> L17_out8, 
			L17_out9	=> L17_out9, 
			L17_out10	=> L17_out10, 
			L17_out11	=> L17_out11, 
			L17_out12	=> L17_out12, 
			L17_out13	=> L17_out13 

		);

end architecture test;
