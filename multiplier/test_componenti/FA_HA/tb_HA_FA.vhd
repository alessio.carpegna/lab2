library ieee;
use ieee.std_logic_1164.all;

entity tb_HA_FA is
end entity;

architecture simulation of tb_HA_FA is

component FA 
port(
	-- input signals
	s1	: in std_logic;
	s2	: in std_logic;
	c_in	: in std_logic;

	-- output signals
	sum 	: out std_logic;
	carry	: out std_logic
		
		
);
end component FA;

component HA
port(
	-- input signals
	s1	: in std_logic;
	s2	: in std_logic;
	-- output signals
	sum	: out std_logic;
	carry	: out std_logic	
);
end component HA;

-- FA inputs
signal FA_s1_test,FA_s2_test,FA_c_in_test	: std_logic;
-- HA inputs
signal HA_s1_test,HA_s2_test			: std_logic;
-- FA outputs
signal FA_sum_test,FA_carry_test		: std_logic;
-- HA outputs
signal HA_sum_test,HA_carry_test		: std_logic;


begin

DUT1	: FA port map(
		s1 	=> FA_s1_test,
		s2	=> FA_s2_test,
		c_in	=> FA_c_in_test,
		sum	=> FA_sum_test,
		carry	=> FA_carry_test
		);
DUT2	: HA port map(
		s1	=> HA_s1_test,
      		s2	=> HA_s2_test,
		sum	=> HA_sum_test,
		carry	=> HA_carry_test		
		);

inputs_gen : process

begin
	FA_s1_test 	<= '0';
	FA_s2_test 	<= '0';
	FA_c_in_test 	<= '0';
	HA_s1_test	<= '0';
	HA_s2_test	<= '0';
	wait for 10 ns;	
	FA_s1_test 	<= '0';
	FA_s2_test 	<= '1';
	FA_c_in_test 	<= '0';
	HA_s1_test	<= '0';
	HA_s2_test	<= '1';
	wait for 10 ns;
	FA_s1_test 	<= '1';
	FA_s2_test 	<= '0';
	FA_c_in_test 	<= '0';
	HA_s1_test	<= '1';
	HA_s2_test	<= '0';
	wait for 10 ns;
	FA_s1_test 	<= '1';
	FA_s2_test 	<= '1';
	FA_c_in_test 	<= '0';
	HA_s1_test	<= '1';
	HA_s2_test	<= '1';
	wait for 10 ns;
	FA_s1_test	<= '0';
	FA_s2_test	<= '0';
	FA_c_in_test	<= '1';
	wait for 10 ns;
	FA_s1_test	<= '1';
	FA_s2_test	<= '0';
	FA_c_in_test	<= '1';
	wait for 10 ns;
	FA_s1_test	<= '0';
	FA_s2_test	<= '1';
	FA_c_in_test	<= '1';
	wait for 10 ns;
	FA_s1_test	<= '1';
	FA_s2_test	<= '1';
	FA_c_in_test	<= '1';
	wait;

end process inputs_gen;

end architecture simulation;
