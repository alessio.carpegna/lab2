library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;

entity dadda_tree_tb is
end entity dadda_tree_tb;

architecture test of dadda_tree_tb is

	component dadda_tree is
		port(
			-- input signals
			pp1	: in std_logic_vector(35 downto 0);
			pp2	: in std_logic_vector(36 downto 0);
			pp3	: in std_logic_vector(36 downto 0);
			pp4	: in std_logic_vector(36 downto 0);
			pp5	: in std_logic_vector(36 downto 0);
			pp6	: in std_logic_vector(36 downto 0);
			pp7	: in std_logic_vector(36 downto 0);
			pp8	: in std_logic_vector(36 downto 0);
			pp9	: in std_logic_vector(36 downto 0);
			pp10	: in std_logic_vector(36 downto 0);
			pp11	: in std_logic_vector(36 downto 0);
			pp12	: in std_logic_vector(36 downto 0);
			pp13	: in std_logic_vector(36 downto 0);
			pp14	: in std_logic_vector(36 downto 0);
			pp15	: in std_logic_vector(36 downto 0);
			pp16	: in std_logic_vector(35 downto 0);
			pp17	: in std_logic_vector(33 downto 0);
			
			-- output signals
			product	: out std_logic_vector(63 downto 0)
		);
	end component dadda_tree;

	file infile  : text open read_mode is "input_dadda_tree.txt";
	file outfile : text open write_mode is "output_dadda_tree.txt";

	-- input partial products
	type matrix_17x37 is array(16 downto 0) of std_logic_vector(36 downto 0);
	signal pp_test	: matrix_17x37;

	signal product_test	: std_logic_vector(63 downto 0);

begin

	dadda_tree_test : process

		variable inLine		: line;
		variable outLine	: line;

		variable v_in		: integer; 
		variable v_out		: integer;

	begin
		if not endfile(inFile)
		then
		
			-- input generation	
			readline(inFile, inLine);
			read_inputs : for i in 0 to 16 loop

				read(inLine, v_in);
				pp_test(i) <= std_logic_vector(to_unsigned(v_in, pp_test(i)'length));

			end loop read_inputs;

			wait for 10 ns;

			-- output storing
			v_out	:= to_integer(unsigned(product_test));
			write(outLine, v_out);
			writeline(outFile, outLine);

		else
			wait;
		end if;

	end process dadda_tree_test;

	DUT : dadda_tree
	port map(
		-- input signals
		pp1	=> pp_test(0)(35 downto 0),
		pp2	=> pp_test(1),
		pp3	=> pp_test(2),
		pp4	=> pp_test(3),
		pp5	=> pp_test(4),
		pp6	=> pp_test(5),
		pp7	=> pp_test(6),
		pp8	=> pp_test(7),
		pp9	=> pp_test(8),
		pp10	=> pp_test(9),
		pp11	=> pp_test(10),
		pp12	=> pp_test(11),
		pp13	=> pp_test(12),
		pp14	=> pp_test(13),
		pp15	=> pp_test(14),
		pp16	=> pp_test(15)(35 downto 0),
		pp17	=> pp_test(16)(33 downto 0),

		-- output signals
		product	=> product_test
	);

end architecture test;
