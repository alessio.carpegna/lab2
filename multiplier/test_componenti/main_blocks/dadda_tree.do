#vcom ../../main_blocks/dadda_tree.vhd
#vcom ../../layers/L17.vhd
#vcom ../../layers/L13.vhd
#vcom ../../layers/L9.vhd
#vcom ../../layers/L6.vhd
#vcom ../../layers/L4.vhd
#vcom ../../layers/L3.vhd
#vcom ../../shift_triangle/shift_triangle.vhd
#vcom ../../remapper/*.vhd
#vcom ../../compressor/*.vhd
#vcom ../../sideCompressors/*.vhd
#vcom dadda_tree_tb.vhd

vsim -t ns -novopt work.dadda_tree_tb
run 1 us

quit -f
