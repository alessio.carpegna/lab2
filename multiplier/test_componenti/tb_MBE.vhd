library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;


entity tb_MBE is
end entity tb_MBE;

architecture test of tb_MBE is

	component MBE_mpy
		port(
			-- input multiplicands
			a	: in std_logic_vector(31 downto 0);
			b	: in std_logic_vector(31 downto 0);
			
			-- product
			product	: out std_logic_vector(63 downto 0)
		);
	end component MBE_mpy;
 
	signal a_tb	: std_logic_vector(31 downto 0);
	signal b_tb	: std_logic_vector(31 downto 0);
	
	signal product	: std_logic_vector(63 downto 0);

	file inFile	: text open read_mode is "input_couples.txt";
	file outFile	: text open write_mode is "results_MBE.txt";
begin

	DUT : MBE_mpy port map(
			
			a => a_tb,
			b => b_tb,

			product => product
			
			);


	data_gen : process

		variable inLine		: line;
		variable outline	: line;

		variable m1		: std_logic_vector(31 downto 0);
		variable m2		: std_logic_vector(31 downto 0);
		variable prod		: std_logic_vector(63 downto 0);
		
		begin
			if not endfile(inFile)
			then
			-- input generation
				readline(inFile,inLine);
				read(inline,m1);
				a_tb	<= m1;
				read(inLine,m2);
				b_tb	<= m2;
			-- output writing
				wait for 10 ns;
				prod	:= product;
				write(outLine,prod);
				writeline(outFile,outLine);
			else 
				wait;

			end if;
		end process data_gen;
end architecture test;
