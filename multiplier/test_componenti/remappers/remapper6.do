set DUT "../../remapper/remapper6"
set TB "remapper6_tb"

vlib work

vcom $DUT.vhd
vcom $TB.vhd

vsim -t ns -novopt work.$TB

run 1 us

quit -f
