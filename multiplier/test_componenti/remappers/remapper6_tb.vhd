library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;

entity remapper6_tb is
end entity remapper6_tb;

architecture test of remapper6_tb is

	component remapper6 is
		generic(
			N	: integer := 18		
		);

		port(
			-- input signals
			l1_sum		: in std_logic_vector(N downto 0);
			l1_carry	: in std_logic_vector(N downto 0);
			l2_sum		: in std_logic_vector(N-4 downto 0);
			l2_carry	: in std_logic_vector(N-4 downto 0);
			l3_sum		: in std_logic_vector(N-8 downto 0);
			l3_carry	: in std_logic_vector(N-8 downto 0);

			--output signals
			l1_s1		: out std_logic_vector(N+1 downto 0);
			l1_s2		: out std_logic_vector(N-1 downto 0);
			l1_c		: out std_logic_vector(N-3 downto 0);
			l2_s1		: out std_logic_vector(N-5 downto 0);
			l2_s2		: out std_logic_vector(N-7 downto 0);
			l2_c		: out std_logic_vector(N-9 downto 0)
		);

	end component remapper6;

	constant N		: integer := 18;

	-- input signals
	signal l1_sum_test	: std_logic_vector(N downto 0);
	signal l1_carry_test	: std_logic_vector(N downto 0);
	signal l2_sum_test	: std_logic_vector(N-4 downto 0);
	signal l2_carry_test	: std_logic_vector(N-4 downto 0);
	signal l3_sum_test	: std_logic_vector(N-8 downto 0);
	signal l3_carry_test	: std_logic_vector(N-8 downto 0);

	--output signals
	signal l1_s1_test	: std_logic_vector(N+1 downto 0);
	signal l1_s2_test	: std_logic_vector(N-1 downto 0);
	signal l1_c_test	: std_logic_vector(N-3 downto 0);
	signal l2_s1_test	: std_logic_vector(N-5 downto 0);
	signal l2_s2_test	: std_logic_vector(N-7 downto 0);
	signal l2_c_test	: std_logic_vector(N-9 downto 0);

	file infile  : text open read_mode is "../../pythonSimulation/remapper/inputFiles/remapper6_input.txt";
	file outfile : text open write_mode is "remapper6_output.txt";  

begin



	remapper6_test : process
	
		variable inLine		: line;
		variable outLine	: line;

		variable v_in		: integer; 
		variable v_out		: integer;
	
	begin

		if not endfile(inFile)
		then
		
			-- input generation	
			readline(inFile, inLine);
			read(inLine, v_in);
			l1_sum_test <= std_logic_vector(to_unsigned(v_in, l1_sum_test'length)); 
			read(inLine, v_in);
			l1_carry_test <= std_logic_vector(to_unsigned(v_in, l1_carry_test'length)); 
			read(inLine, v_in);
			l2_sum_test <= std_logic_vector(to_unsigned(v_in, l2_sum_test'length)); 
			read(inLine, v_in);
			l2_carry_test <= std_logic_vector(to_unsigned(v_in, l2_carry_test'length)); 
			read(inLine, v_in);
			l3_sum_test <= std_logic_vector(to_unsigned(v_in, l3_sum_test'length)); 
			read(inLine, v_in);
			l3_carry_test <= std_logic_vector(to_unsigned(v_in, l3_carry_test'length)); 	
			
			wait for 10 ns;

			-- output storing
			v_out	:= to_integer(unsigned(l1_s1_test));
			write(outLine, v_out);
			write(outline,string'(" "));
			v_out	:= to_integer(unsigned(l1_s2_test));
			write(outLine, v_out);
			write(outline,string'(" "));
			v_out	:= to_integer(unsigned(l1_c_test));
			write(outLine, v_out);
			write(outline,string'(" "));
			v_out	:= to_integer(unsigned(l2_s1_test));
			write(outLine, v_out);
			write(outline,string'(" "));
			v_out	:= to_integer(unsigned(l2_s2_test));
			write(outLine, v_out);
			write(outline,string'(" "));
			v_out	:= to_integer(unsigned(l2_c_test));
			write(outLine, v_out);
			writeline(outFile, outLine);

		else
			wait;
		end if;

	end process remapper6_test;



	DUT : remapper6 port map(

		-- input signals
		l1_sum		=> l1_sum_test,
		l1_carry	=> l1_carry_test,
		l2_sum		=> l2_sum_test,
		l2_carry	=> l2_carry_test,
		l3_sum		=> l3_sum_test,
		l3_carry	=> l3_carry_test,

		-- output signals
		l1_s1		=> l1_s1_test,
		l1_s2		=> l1_s2_test,
		l1_c		=> l1_c_test,
		l2_s1		=> l2_s1_test,
		l2_s2		=> l2_s2_test,
		l2_c		=> l2_c_test
	);
end architecture test;
