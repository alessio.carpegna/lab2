library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;

entity remapper2_tb is
end entity remapper2_tb;

architecture test of remapper2_tb is

	component remapper2 is
		generic(
			N	: integer := 18		
		);

		port(
			-- input signals
			l1_sum		: in std_logic_vector(N downto 0);
			l1_carry	: in std_logic_vector(N downto 0);

			--output signals
			l1_s1		: out std_logic_vector(N+1 downto 0);
			l1_s2		: out std_logic_vector(N-1 downto 0)
		);

	end component remapper2;

	constant N		: integer := 18;

	-- input signals
	signal l1_sum_test	: std_logic_vector(N downto 0);
	signal l1_carry_test	: std_logic_vector(N downto 0);

	--output signals
	signal l1_s1_test	: std_logic_vector(N+1 downto 0);
	signal l1_s2_test	: std_logic_vector(N-1 downto 0);

	file infile  : text open read_mode is "../../pythonSimulation/remapper/inputFiles/remapper2_input.txt";
	file outfile : text open write_mode is "remapper2_output.txt";  

begin



	remapper2_test : process
	
		variable inLine		: line;
		variable outLine	: line;

		variable v_in		: integer; 
		variable v_out		: integer;
	
	begin

		if not endfile(inFile)
		then
		
			-- input generation	
			readline(inFile, inLine);
			read(inLine, v_in);
			l1_sum_test <= std_logic_vector(to_unsigned(v_in, l1_sum_test'length)); 
			read(inLine, v_in);
			l1_carry_test <= std_logic_vector(to_unsigned(v_in, l1_carry_test'length)); 
			
			wait for 10 ns;

			-- output storing
			v_out	:= to_integer(unsigned(l1_s1_test));
			write(outLine, v_out);
			write(outline,string'(" "));
			v_out	:= to_integer(unsigned(l1_s2_test));
			write(outLine, v_out);
			writeline(outFile, outLine);

		else
			wait;
		end if;

	end process remapper2_test;



	DUT : remapper2 port map(

		-- input signals
		l1_sum		=> l1_sum_test,
		l1_carry	=> l1_carry_test,

		-- output signals
		l1_s1		=> l1_s1_test,
		l1_s2		=> l1_s2_test
	);
end architecture test;
