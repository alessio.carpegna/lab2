set DUT "../../remapper/remapper2"
set TB "remapper2_tb"

vlib work

vcom $DUT.vhd
vcom $TB.vhd

vsim -t ns -novopt work.$TB

run 1 us

quit -f
