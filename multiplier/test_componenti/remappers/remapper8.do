set DUT "../../remapper/remapper8"
set TB "remapper8_tb"

vlib work

vcom $DUT.vhd
vcom $TB.vhd

vsim -t ns -novopt work.$TB

run 1 us

quit -f
