set DUT "../../remapper/remapper4"
set TB "remapper4_tb"

vlib work

vcom $DUT.vhd
vcom $TB.vhd

vsim -t ns -novopt work.$TB

run 1 us

quit -f
