library ieee;
use ieee.std_logic_1164.all;


entity rightCompressor7_5_tb is
end entity rightCompressor7_5_tb;


architecture test of rightCompressor7_5_tb is

	component rightCompressor7_5 is
		port(
			-- 7 inputs
			inRow3		: in std_logic_vector(1 downto 0);
			inRow4		: in std_logic_vector(1 downto 0);
			inRow5		: in std_logic_vector(1 downto 0);
			inRow6		: in std_logic_vector(3 downto 0);
			inRow7		: in std_logic_vector(3 downto 0);
			inRow8		: in std_logic_vector(3 downto 2);
			inRow9		: in std_logic_vector(5 downto 4);

			-- 5 outputs
			outRow2		: out std_logic;
			outRow3		: out std_logic_vector(1 downto 0);
			outRow4		: out std_logic_vector(2 downto 0);
			outRow5		: out std_logic_vector(3 downto 0);
			outRow6		: out std_logic_vector(4 downto 0)
		);

	end component rightCompressor7_5;

	-- 7 inputs
	signal inRow3		: std_logic_vector(1 downto 0);
	signal inRow4		: std_logic_vector(1 downto 0);
	signal inRow5		: std_logic_vector(1 downto 0);
	signal inRow6		: std_logic_vector(3 downto 0);
	signal inRow7		: std_logic_vector(3 downto 0);
	signal inRow8		: std_logic_vector(3 downto 2);
	signal inRow9		: std_logic_vector(5 downto 4);

	-- 5 outputs
	signal outRow2		: std_logic;
	signal outRow3		: std_logic_vector(1 downto 0);
	signal outRow4		: std_logic_vector(2 downto 0);
	signal outRow5		: std_logic_vector(3 downto 0);
	signal outRow6		: std_logic_vector(4 downto 0);

begin


	DUT : rightCompressor7_5 port map(
		-- 7 inputs
		inRow3		=> inRow3,
		inRow4		=> inRow4,
		inRow5		=> inRow5,
		inRow6		=> inRow6,
		inRow7		=> inRow7,
		inRow8		=> inRow8,
		inRow9		=> inRow9,

		-- 5 outputs
		outRow2		=> outRow2,
		outRow3		=> outRow3,
		outRow4		=> outRow4,
		outRow5		=> outRow5,
		outRow6		=> outRow6
	);


	inRow3 	<= "01";
	inRow4 	<= "10";
	inRow5 	<= "01";
	inRow6 	<= "0110";
	inRow7 	<= "1001";
	inRow8 	<= "01";
	inRow9 	<= "01";


end architecture test;
