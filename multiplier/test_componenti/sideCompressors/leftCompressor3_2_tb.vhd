library ieee;
use ieee.std_logic_1164.all;


entity leftCompressor3_2_tb is
end entity leftCompressor3_2_tb;

architecture test of leftCompressor3_2_tb is

	component leftCompressor3_2 is
		port(
			-- 3 inputs
			inRow1		: in std_logic;
			inRow2		: in std_logic;
			inRow3		: in std_logic;


			-- 2 outputs
			outRow2		: out std_logic;
			outRow3		: out std_logic_vector(7 downto 6)
		);

	end component leftCompressor3_2;

	-- 3 inputs
	signal inRow1		: std_logic;
	signal inRow2		: std_logic;
	signal inRow3		: std_logic;

	-- 2 outputs
	signal outRow2		: std_logic;
	signal outRow3		: std_logic_vector(7 downto 6);

begin

	DUT : leftCompressor3_2 port map(
	
		-- 3 inputs
		inRow1		=> inRow1,
		inRow2		=> inRow2,
		inRow3		=> inRow3,

		-- 2 outputs
		outRow2		=> outRow2,
		outRow3		=> outRow3
	);
inRow1 <= '1';
inRow2 <= '0';
inRow3 <= '0';
end architecture test;
