library ieee;
use ieee.std_logic_1164.all;


entity leftCompressor7_5_tb is
end entity leftCompressor7_5_tb;

architecture test of leftCompressor7_5_tb is

	component leftCompressor7_5 is
		port(
			-- 7 inputs
			inRow1		: in std_logic;
			inRow2		: in std_logic;
			inRow3		: in std_logic_vector(7 downto 6);
			inRow4		: in std_logic_vector(7 downto 5);
			inRow5		: in std_logic_vector(7 downto 5);
			inRow6		: in std_logic_vector(6 downto 4);
			inRow7		: in std_logic_vector(4 downto 3);

			-- 5 outputs
			outRow2		: out std_logic;
			outRow3		: out std_logic_vector(7 downto 6);
			outRow4		: out std_logic_vector(7 downto 5);
			outRow5		: out std_logic_vector(7 downto 4);
			outRow6		: out std_logic_vector(7 downto 3)
		);

	end component leftCompressor7_5;

	-- 7 inputs
	signal inRow1		: std_logic;
	signal inRow2		: std_logic;
	signal inRow3		: std_logic_vector(7 downto 6);
	signal inRow4		: std_logic_vector(7 downto 5);
	signal inRow5		: std_logic_vector(7 downto 5);
	signal inRow6		: std_logic_vector(6 downto 4);
	signal inRow7		: std_logic_vector(4 downto 3);

	-- 5 outputs
	signal outRow2		: std_logic;
	signal outRow3		: std_logic_vector(7 downto 6);
	signal outRow4		: std_logic_vector(7 downto 5);
	signal outRow5		: std_logic_vector(7 downto 4);
	signal outRow6		: std_logic_vector(7 downto 3);

begin

	DUT : leftCompressor7_5 port map(
	
		-- 7 inputs
		inRow1		=> inRow1,
		inRow2		=> inRow2,
		inRow3		=> inRow3,
		inRow4		=> inRow4,
		inRow5		=> inRow5,
		inRow6		=> inRow6,
		inRow7		=> inRow7,

		-- 5 outputs
		outRow2		=> outRow2,
		outRow3		=> outRow3,
		outRow4		=> outRow4,
		outRow5		=> outRow5,
		outRow6		=> outRow6
	);
inRow1 <= '1';
inRow2 <= '0';
inRow3 <= "10";
inRow4 <= "011";
inRow5 <= "100";
inRow6 <= "110";
inRow7 <= "11";
end architecture test;
