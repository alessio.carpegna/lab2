library ieee;
use ieee.std_logic_1164.all;


entity leftCompressor16_12_tb is
end entity leftCompressor16_12_tb;

architecture test of leftCompressor16_12_tb is

	component leftCompressor16_12 is
		port(
			-- 16 inputs
			inRow1		: in std_logic;
			inRow2		: in std_logic;
			inRow3		: in std_logic_vector(7 downto 6);
			inRow4		: in std_logic_vector(7 downto 5);
			inRow5		: in std_logic_vector(7 downto 5);
			inRow6		: in std_logic_vector(7 downto 4);
			inRow7		: in std_logic_vector(7 downto 3);
			inRow8		: in std_logic_vector(7 downto 3);
			inRow9		: in std_logic_vector(7 downto 2);
			inRow10		: in std_logic_vector(7 downto 1);
			inRow11		: in std_logic_vector(7 downto 1);
			inRow12		: in std_logic_vector(7 downto 0);
			inRow13		: in std_logic_vector(6 downto 0);
			inRow14		: in std_logic_vector(4 downto 0);
			inRow15		: in std_logic_vector(2 downto 0);
			inRow16		: in std_logic;


			-- 12 outputs
			outRow2		: out std_logic;
			outRow3		: out std_logic_vector(7 downto 6);
			outRow4		: out std_logic_vector(7 downto 5);
			outRow5		: out std_logic_vector(7 downto 4);
			outRow6		: out std_logic_vector(7 downto 3);
			outRow7		: out std_logic_vector(7 downto 2);
			outRow8		: out std_logic_vector(7 downto 1);
			outRow9		: out std_logic_vector(7 downto 0);
			outRow10	: out std_logic_vector(7 downto 0);
			outRow11	: out std_logic_vector(7 downto 0);
			outRow12	: out std_logic_vector(7 downto 0);
			outRow13	: out std_logic_vector(7 downto 0)
		);

	end component leftCompressor16_12;

	-- 16 inputs
	signal inRow1		: std_logic;
	signal inRow2		: std_logic;
	signal inRow3		: std_logic_vector(7 downto 6);
	signal inRow4		: std_logic_vector(7 downto 5);
	signal inRow5		: std_logic_vector(7 downto 5);
	signal inRow6		: std_logic_vector(7 downto 4);
	signal inRow7		: std_logic_vector(7 downto 3);
	signal inRow8		: std_logic_vector(7 downto 3);
	signal inRow9		: std_logic_vector(7 downto 2);
	signal inRow10		: std_logic_vector(7 downto 1);
	signal inRow11		: std_logic_vector(7 downto 1);
	signal inRow12		: std_logic_vector(7 downto 0);
	signal inRow13		: std_logic_vector(6 downto 0);
	signal inRow14		: std_logic_vector(4 downto 0);
	signal inRow15		: std_logic_vector(2 downto 0);
	signal inRow16		: std_logic;


	-- 12 outputs
	signal outRow2		: std_logic;
	signal outRow3		: std_logic_vector(7 downto 6);
	signal outRow4		: std_logic_vector(7 downto 5);
	signal outRow5		: std_logic_vector(7 downto 4);
	signal outRow6		: std_logic_vector(7 downto 3);
	signal outRow7		: std_logic_vector(7 downto 2);
	signal outRow8		: std_logic_vector(7 downto 1);
	signal outRow9		: std_logic_vector(7 downto 0);
	signal outRow10		: std_logic_vector(7 downto 0);
	signal outRow11		: std_logic_vector(7 downto 0);
	signal outRow12		: std_logic_vector(7 downto 0);
	signal outRow13		: std_logic_vector(7 downto 0);


begin

	DUT : leftCompressor16_12 port map(
	
		-- 16 inputs
		inRow1		=> inRow1,
		inRow2		=> inRow2,
		inRow3		=> inRow3,
		inRow4		=> inRow4,
		inRow5		=> inRow5,
		inRow6		=> inRow6,
		inRow7		=> inRow7,
		inRow8		=> inRow8,
		inRow9		=> inRow9,
		inRow10		=> inRow10,
		inRow11		=> inRow11,
		inRow12		=> inRow12,
		inRow13		=> inRow13,
		inRow14		=> inRow14,
		inRow15		=> inRow15,
		inRow16		=> inRow16,

		--12 outputs
		outRow2		=> outRow2,
		outRow3		=> outRow3,
		outRow4		=> outRow4,
		outRow5		=> outRow5,
		outRow6		=> outRow6,
		outRow7		=> outRow7,
		outRow8		=> outRow8,
		outRow9		=> outRow9,
		outRow10	=> outRow10,
		outRow11	=> outRow11,
		outRow12	=> outRow12,
		outRow13	=> outRow13
	);
-- inputs given

      inRow1    <= '1';
      inRow2    <= '0';
      inRow3    <= "10";
      inRow4    <= "011";
      inRow5    <= "100";
      inRow6    <= "0110";
      inRow7    <= "10011";
      inRow8    <= "01100";
      inRow9    <= "100110";
      inRow10   <= "0110011";
      inRow11   <= "1001100";
      inRow12   <= "01100110";
      inRow13   <= "0011001";
      inRow14   <= "00110";
      inRow15   <= "001";
      inRow16   <= '0';


end architecture test;
