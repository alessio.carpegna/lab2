library ieee;
use ieee.std_logic_1164.all;


entity rightCompressor2_2_tb is
end entity rightCompressor2_2_tb;


architecture test of rightCompressor2_2_tb is

	component rightCompressor2_2 is
		port(
			-- 2 inputs
			inRow3		: in std_logic_vector(1 downto 0);
			inRow4		: in std_logic_vector(1 downto 0);

			-- 2 outputs
			outRow2		: out std_logic;
			outRow3		: out std_logic_vector(1 downto 0)
		);

	end component rightCompressor2_2;

	-- 2 inputs
	signal inRow3		: std_logic_vector(1 downto 0);
	signal inRow4		: std_logic_vector(1 downto 0);

	-- 2 outputs
	signal outRow2		: std_logic;
	signal outRow3		: std_logic_vector(1 downto 0);

begin


	DUT : rightCompressor2_2 port map(
		-- 2 inputs
		inRow3		=> inRow3,
		inRow4		=> inRow4,

		-- 2 outputs
		outRow2		=> outRow2,
		outRow3		=> outRow3
	);


	inRow3 	<= "01";
	inRow4 	<= "10";


end architecture test;
