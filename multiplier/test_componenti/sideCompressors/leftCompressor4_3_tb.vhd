library ieee;
use ieee.std_logic_1164.all;


entity leftCompressor4_3_tb is
end entity leftCompressor4_3_tb;

architecture test of leftCompressor4_3_tb is

	component leftCompressor4_3 is
		port(
			-- 4 inputs
			inRow1		: in std_logic;
			inRow2		: in std_logic;
			inRow3		: in std_logic_vector(7 downto 6);
			inRow4		: in std_logic_vector(6 downto 5);

			-- 3 outputs
			outRow2		: out std_logic;
			outRow3		: out std_logic_vector(7 downto 6);
			outRow4		: out std_logic_vector(7 downto 5)
		);

	end component leftCompressor4_3;

	-- 4 inputs
	signal inRow1		: std_logic;
	signal inRow2		: std_logic;
	signal inRow3		: std_logic_vector(7 downto 6);
	signal inRow4		: std_logic_vector(6 downto 5);

	-- 3 outputs
	signal outRow2		: std_logic;
	signal outRow3		: std_logic_vector(7 downto 6);
	signal outRow4		: std_logic_vector(7 downto 5);

begin

	DUT : leftCompressor4_3 port map(
	
		-- 4 inputs
		inRow1		=> inRow1,
		inRow2		=> inRow2,
		inRow3		=> inRow3,
		inRow4		=> inRow4,

		-- 3 outputs
		outRow2		=> outRow2,
		outRow3		=> outRow3,
		outRow4		=> outRow4
	);
inRow1 <= '1';
inRow2 <= '0';
inRow3 <= "10";
inRow4 <= "11";
end architecture test;
