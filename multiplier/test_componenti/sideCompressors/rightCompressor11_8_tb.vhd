library ieee;
use ieee.std_logic_1164.all;


entity rightCompressor11_8_tb is
end entity rightCompressor11_8_tb;


architecture test of rightCompressor11_8_tb is

	component rightCompressor11_8 is
		port(
			-- 11 inputs
			inRow3		: in std_logic_vector(1 downto 0);
			inRow4		: in std_logic_vector(1 downto 0);
			inRow5		: in std_logic_vector(1 downto 0);
			inRow6		: in std_logic_vector(3 downto 0);
			inRow7		: in std_logic_vector(3 downto 0);
			inRow8		: in std_logic_vector(3 downto 0);
			inRow9		: in std_logic_vector(5 downto 0);
			inRow10		: in std_logic_vector(5 downto 0);
			inRow11		: in std_logic_vector(5 downto 2);
			inRow12		: in std_logic_vector(7 downto 4);
			inRow13		: in std_logic_vector(7 downto 6);


			-- 8 outputs
			outRow2		: out std_logic;
			outRow3		: out std_logic_vector(1 downto 0);
			outRow4		: out std_logic_vector(2 downto 0);
			outRow5		: out std_logic_vector(3 downto 0);
			outRow6		: out std_logic_vector(4 downto 0);
			outRow7		: out std_logic_vector(5 downto 0);
			outRow8		: out std_logic_vector(6 downto 0);
			outRow9		: out std_logic_vector(7 downto 0)
			
		);

	end component rightCompressor11_8;

	-- 11 inputs
	signal inRow3		: std_logic_vector(1 downto 0);
	signal inRow4		: std_logic_vector(1 downto 0);
	signal inRow5		: std_logic_vector(1 downto 0);
	signal inRow6		: std_logic_vector(3 downto 0);
	signal inRow7		: std_logic_vector(3 downto 0);
	signal inRow8		: std_logic_vector(3 downto 0);
	signal inRow9		: std_logic_vector(5 downto 0);
	signal inRow10		: std_logic_vector(5 downto 0);
	signal inRow11		: std_logic_vector(5 downto 2);
	signal inRow12		: std_logic_vector(7 downto 4);
	signal inRow13		: std_logic_vector(7 downto 6);

	-- 8 outputs
	signal outRow2		: std_logic;
	signal outRow3		: std_logic_vector(1 downto 0);
	signal outRow4		: std_logic_vector(2 downto 0);
	signal outRow5		: std_logic_vector(3 downto 0);
	signal outRow6		: std_logic_vector(4 downto 0);
	signal outRow7		: std_logic_vector(5 downto 0);
	signal outRow8		: std_logic_vector(6 downto 0);
	signal outRow9		: std_logic_vector(7 downto 0);

begin


	DUT : rightCompressor11_8 port map(
		-- 11 inputs
		inRow3		=> inRow3,
		inRow4		=> inRow4,
		inRow5		=> inRow5,
		inRow6		=> inRow6,
		inRow7		=> inRow7,
		inRow8		=> inRow8,
		inRow9		=> inRow9,
		inRow10		=> inRow10,
		inRow11		=> inRow11,
		inRow12		=> inRow12,
		inRow13		=> inRow13,

		-- 8 outputs
		outRow2		=> outRow2,
		outRow3		=> outRow3,
		outRow4		=> outRow4,
		outRow5		=> outRow5,
		outRow6		=> outRow6,
		outRow7		=> outRow7,
		outRow8		=> outRow8,
		outRow9		=> outRow9

	);


	inRow3 	<= "01";
	inRow4 	<= "10";
	inRow5 	<= "01";
	inRow6 	<= "0110";
	inRow7 	<= "1001";
	inRow8 	<= "0110";
	inRow9 	<= "011001";
	inRow10	<= "100110";
	inRow11	<= "0110";
	inRow12	<= "0110";
	inRow13	<= "10";


end architecture test;
