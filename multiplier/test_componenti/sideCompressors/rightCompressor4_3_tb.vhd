library ieee;
use ieee.std_logic_1164.all;


entity rightCompressor4_3_tb is
end entity rightCompressor4_3_tb;


architecture test of rightCompressor4_3_tb is

	component rightCompressor4_3 is
		port(
			-- 4 inputs
			inRow3		: in std_logic_vector(1 downto 0);
			inRow4		: in std_logic_vector(1 downto 0);
			inRow5		: in std_logic_vector(1 downto 0);
			inRow6		: in std_logic_vector(3 downto 2);

			-- 3 outputs
			outRow2		: out std_logic;
			outRow3		: out std_logic_vector(1 downto 0);
			outRow4		: out std_logic_vector(2 downto 0)
		);

	end component rightCompressor4_3;

	-- 4 inputs
	signal inRow3		: std_logic_vector(1 downto 0);
	signal inRow4		: std_logic_vector(1 downto 0);
	signal inRow5		: std_logic_vector(1 downto 0);
	signal inRow6		: std_logic_vector(3 downto 2);

	-- 3 outputs
	signal outRow2		: std_logic;
	signal outRow3		: std_logic_vector(1 downto 0);
	signal outRow4		: std_logic_vector(2 downto 0);

begin


	DUT : rightCompressor4_3 port map(
		-- 4 inputs
		inRow3		=> inRow3,
		inRow4		=> inRow4,
		inRow5		=> inRow5,
		inRow6		=> inRow6,

		-- 3 outputs
		outRow2		=> outRow2,
		outRow3		=> outRow3,
		outRow4		=> outRow4
	);


	inRow3 	<= "01";
	inRow4 	<= "10";
	inRow5 	<= "01";
	inRow6 	<= "01";


end architecture test;
