library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;


entity comp12_8_tb is
end entity comp12_8_tb;

architecture test of comp12_8_tb is

	component comp12_8
		generic(n_component_first_row: integer := 18);
   	   port(
		   -- input signals
		   L1_S1		: in std_logic_vector(n_component_first_row downto 0);
		   L1_S2		: in std_logic_vector(n_component_first_row downto 0);
		   L1_C		: in std_logic_vector(n_component_first_row -1 downto 2);
		   
		   L2_S1		: in std_logic_vector(n_component_first_row -2 downto 2);
		   L2_S2		: in std_logic_vector(n_component_first_row -2 downto 2);
		   L2_C		: in std_logic_vector(n_component_first_row-3 downto 4);
		   
		   L3_S1		: in std_logic_vector(n_component_first_row-4 downto 4);
		   L3_S2		: in std_logic_vector(n_component_first_row-4 downto 4);
		   L3_C		: in std_logic_vector(n_component_first_row-5 downto 6);
		   
		   L4_S1		: in std_logic_vector(n_component_first_row -6 downto 6);
		   L4_S2		: in std_logic_vector(n_component_first_row -6 downto 6);
		   L4_C		: in std_logic_vector(n_component_first_row -7 downto 8);
   
		   --output signals
		   L1_sum		: out std_logic_vector(n_component_first_row downto 0);
		   L1_carry	: out std_logic_vector(n_component_first_row downto 0);
		   
		   L2_sum		: out std_logic_vector(n_component_first_row -2 downto 2);
		   L2_carry	: out std_logic_vector(n_component_first_row -2 downto 2);
		   
		   L3_sum		: out std_logic_vector(n_component_first_row -4 downto 4);
		   L3_carry	: out std_logic_vector(n_component_first_row -4 downto 4);
		   
		   L4_sum		: out std_logic_vector(n_component_first_row -6 downto 6);
		   L4_carry	: out std_logic_vector(n_component_first_row -6 downto 6)
	      );

	end component comp12_8;

	constant n_component_first_row	: integer := 18;

	-- test input signals
	signal L1_S1_test	: std_logic_vector(n_component_first_row downto 0);
	signal L1_S2_test	: std_logic_vector(n_component_first_row downto 0);
	signal L1_C_test	: std_logic_vector(n_component_first_row -1 downto 2);
		
	signal L2_S1_test	: std_logic_vector(n_component_first_row -2 downto 2);
	signal L2_S2_test	: std_logic_vector(n_component_first_row -2 downto 2);
	signal L2_C_test	: std_logic_vector(n_component_first_row-3 downto 4);
				
	signal L3_S1_test	: std_logic_vector(n_component_first_row -4 downto 4);
	signal L3_S2_test	: std_logic_vector(n_component_first_row -4 downto 4);
	signal L3_C_test	: std_logic_vector(n_component_first_row -5 downto 6);
		
	signal L4_S1_test	: std_logic_vector(n_component_first_row -6 downto 6);
	signal L4_S2_test	: std_logic_vector(n_component_first_row -6 downto 6);
	signal L4_C_test	: std_logic_vector(n_component_first_row-7 downto 8);

	--test output signals
	signal L1_sum_test	: std_logic_vector(n_component_first_row downto 0);
	signal L1_carry_test	: std_logic_vector(n_component_first_row downto 0);
		
	signal L2_sum_test	: std_logic_vector(n_component_first_row -2 downto 2);
	signal L2_carry_test	: std_logic_vector(n_component_first_row -2 downto 2);
		
	signal L3_sum_test	: std_logic_vector(n_component_first_row -4 downto 4);
	signal L3_carry_test	: std_logic_vector(n_component_first_row -4 downto 4);
		
	signal L4_sum_test	: std_logic_vector(n_component_first_row -6 downto 6);
	signal L4_carry_test	: std_logic_vector(n_component_first_row -6 downto 6);

	file infile  : text open read_mode is "comp12_8_inputs.txt";
	file outfile : text open write_mode is "comp12_8_outputs.txt";   
	
begin

	comp12_8_test : process
	
		variable inLine		: line;
		variable outLine	: line;

		variable v_in		: integer; 
		variable v_out		: integer;
	
	begin

		if not endfile(inFile)
		then
		
			-- input generation	
			readline(inFile, inLine);
			read(inLine, v_in);
			L1_S1_test <= std_logic_vector(to_unsigned(v_in, L1_S1_test'length)); 
			read(inLine, v_in);
			L1_S2_test <= std_logic_vector(to_unsigned(v_in, L1_S2_test'length)); 
			read(inLine, v_in);
			L1_C_test <= std_logic_vector(to_unsigned(v_in, L1_C_test'length)); 
			readline(inFile,inLine);
			read(inLine, v_in);
			L2_S1_test <= std_logic_vector(to_unsigned(v_in, L2_S1_test'length)); 
			read(inLine, v_in);
			L2_S2_test <= std_logic_vector(to_unsigned(v_in, L2_S2_test'length)); 
			read(inLine, v_in);
			L2_C_test <= std_logic_vector(to_unsigned(v_in, L2_C_test'length)); 
			readline(inFile,inLine);
			read(inLine, v_in);
			L3_S1_test <= std_logic_vector(to_unsigned(v_in, L3_S1_test'length)); 
			read(inLine, v_in);
			L3_S2_test <= std_logic_vector(to_unsigned(v_in, L3_S2_test'length)); 
			read(inLine, v_in);
			L3_C_test <= std_logic_vector(to_unsigned(v_in, L3_C_test'length)); 
			readline(inFile,inLine);
			read(inLine, v_in);
			L4_S1_test <= std_logic_vector(to_unsigned(v_in, L4_S1_test'length)); 
			read(inLine, v_in);
			L4_S2_test <= std_logic_vector(to_unsigned(v_in, L4_S2_test'length)); 
			read(inLine, v_in);
			L4_C_test <= std_logic_vector(to_unsigned(v_in, L4_C_test'length));

			wait for 10 ns;

			-- output storing
			v_out	:= to_integer(unsigned(L1_sum_test));
			write(outLine, v_out);
			write(outline,string'(" "));
			v_out	:= to_integer(unsigned(L1_carry_test));
			write(outLine, v_out);
			write(outline,string'(" "));	
			v_out	:= to_integer(unsigned(L2_sum_test));
			write(outLine, v_out);
			write(outline,string'(" "));
			v_out	:= to_integer(unsigned(L2_carry_test));
			write(outLine, v_out);
			write(outline,string'(" "));	
			v_out	:= to_integer(unsigned(L3_sum_test));
			write(outLine, v_out);
			write(outline,string'(" "));	
			v_out	:= to_integer(unsigned(L3_carry_test));
			write(outLine, v_out);
			write(outline,string'(" "));	
			v_out	:= to_integer(unsigned(L4_sum_test));
			write(outLine, v_out);
			write(outline,string'(" "));	
			v_out	:= to_integer(unsigned(L4_carry_test));
			write(outLine, v_out);
			writeline(outFile, outLine);
         wait;
		end if;

	end process comp12_8_test;


	DUT : comp12_8 port map(
		-- input signals
		L1_S1		=> L1_S1_test,
		L1_S2		=> L1_S2_test,
		L1_C		=> L1_C_test,
		L2_S1		=> L2_S1_test,
		L2_S2		=> L2_S2_test,
		L2_C		=> L2_C_test,
		L3_S1		=> L3_S1_test,
		L3_S2		=> L3_S2_test,
		L3_C		=> L3_C_test,
		L4_S1		=> L4_S1_test,
		L4_S2		=> L4_S2_test,
		L4_C		=> L4_C_test,

		-- output signals
		L1_sum		=> L1_sum_test,
		L1_carry	=> L1_carry_test,
		L2_sum		=> L2_sum_test,
		L2_carry	=> L2_carry_test,
		L3_sum		=> L3_sum_test,
		L3_carry	=> L3_carry_test,
		L4_sum		=> L4_sum_test,
		L4_carry	=> L4_carry_test

	);

end architecture test;
