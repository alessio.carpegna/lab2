#!/usr/bin/python3

import random

n_bit = 30	 #numero di bit degli operandi
n_levels = 1 	 #numero di triplette Addendo1 Addendo2 Carry da generare
nome_file = 'comp3_2_inputs.txt'
first_write = 0
n_layers = 1

def layer_gen(nome_f,bit_number,n_levels):

	global first_write
	if first_write==0:
		f=open(nome_f,'w')
	
		for i in range(0,n_levels):
			n_gen=random.randint(0,2**(bit_number)-1)
			print(str(n_gen) + ' ')
			f.write(str(n_gen) + ' ')
			n_gen=random.randint(0,2**(bit_number)-1)
			print(str(n_gen) + ' ')
			f.write(str(n_gen)+ ' ')
			n_gen=random.randint(0,2**(bit_number-3)-1)
			print(str(n_gen)+'\n')
			f.write(str(n_gen)+'\n')
		f.close()
		first_write=1
	else:
		f=open(nome_f,'a')

		for i in range(0,n_levels):
			n_gen=random.randint(0,2**(bit_number)-1)
			print(str(n_gen) + ' ')
			f.write(str(n_gen) + ' ')
			n_gen=random.randint(0,2**(bit_number)-1)
			print(str(n_gen) + ' ')
			f.write(str(n_gen)+ ' ')
			n_gen=random.randint(0,2**(bit_number-3)-1)
			print(str(n_gen)+'\n')
			f.write(str(n_gen)+'\n')
		f.close()
#genero i layer necessari
for k in range(0,n_layers):
	print('Layer'+str(k+1))
	print('Genero con '+ str(n_bit)+' bit')
	layer_gen(nome_file,n_bit-4*k,n_levels)

