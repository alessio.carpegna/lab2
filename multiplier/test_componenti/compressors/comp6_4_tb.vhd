library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;


entity comp6_4_tb is
end entity comp6_4_tb;

architecture test of comp6_4_tb is

	component comp6_4
		
	      generic(n_component_first_row: integer := 54);
      
	      port(
		      -- input signals
		      L1_S1		: in std_logic_vector(n_component_first_row downto 0);
		      L1_S2		: in std_logic_vector(n_component_first_row downto 0);
		      L1_C		: in std_logic_vector(n_component_first_row -1 downto 2);
		      
		      L2_S1		: in std_logic_vector(n_component_first_row -2 downto 2);
		      L2_S2		: in std_logic_vector(n_component_first_row -2 downto 2);
		      L2_C		: in std_logic_vector(n_component_first_row-3 downto 4);
      
		      --output signals
		      L1_sum		: out std_logic_vector(n_component_first_row downto 0);
		      L1_carry	: out std_logic_vector(n_component_first_row downto 0);
		      
		      L2_sum		: out std_logic_vector(n_component_first_row -2 downto 2);
		      L2_carry	: out std_logic_vector(n_component_first_row -2 downto 2)
	      );
	end component comp6_4;

	constant n_component_first_row	: integer := 54;

	-- test input signals
	signal L1_S1_test	: std_logic_vector(n_component_first_row downto 0);
	signal L1_S2_test	: std_logic_vector(n_component_first_row downto 0);
	signal L1_C_test	: std_logic_vector(n_component_first_row -1 downto 2);
		
	signal L2_S1_test	: std_logic_vector(n_component_first_row -2 downto 2);
	signal L2_S2_test	: std_logic_vector(n_component_first_row -2 downto 2);
	signal L2_C_test	: std_logic_vector(n_component_first_row-3 downto 4);
	
	--test output signals
	signal L1_sum_test	: std_logic_vector(n_component_first_row downto 0);
	signal L1_carry_test	: std_logic_vector(n_component_first_row downto 0);
		
	signal L2_sum_test	: std_logic_vector(n_component_first_row -2 downto 2);
	signal L2_carry_test	: std_logic_vector(n_component_first_row -2 downto 2);
	
	file infile  : text open read_mode is "comp6_4_inputs.txt";
	file outfile : text open write_mode is "comp6_4_outputs.txt";   
	
begin

	comp6_4_test : process
	
		variable inLine		: line;
		variable outLine	: line;

		variable v_in		: integer; 
		variable v_out		: integer;
	
	begin

		if not endfile(inFile)
		then
		
			-- input generation	
			readline(inFile, inLine);
			read(inLine, v_in);
			L1_S1_test <= std_logic_vector(to_unsigned(v_in, L1_S1_test'length)); 
			read(inLine, v_in);
			L1_S2_test <= std_logic_vector(to_unsigned(v_in, L1_S2_test'length)); 
			read(inLine, v_in);
			L1_C_test <= std_logic_vector(to_unsigned(v_in, L1_C_test'length)); 
			readline(inFile,inLine);
			read(inLine, v_in);
			L2_S1_test <= std_logic_vector(to_unsigned(v_in, L2_S1_test'length)); 
			read(inLine, v_in);
			L2_S2_test <= std_logic_vector(to_unsigned(v_in, L2_S2_test'length)); 
			read(inLine, v_in);
			L2_C_test <= std_logic_vector(to_unsigned(v_in, L2_C_test'length)); 

			wait for 10 ns;

			-- output stoting
			v_out	:= to_integer(unsigned(L1_sum_test));
			write(outLine, v_out);
			write(outline,string'(" "));
			v_out	:= to_integer(unsigned(L1_carry_test));
			write(outLine, v_out);
			write(outline,string'(" "));	
			v_out	:= to_integer(unsigned(L2_sum_test));
			write(outLine, v_out);
			write(outline,string'(" "));
			v_out	:= to_integer(unsigned(L2_carry_test));
			write(outLine, v_out);
			writeline(outFile, outLine);
      else 
         wait;
		end if;

	end process comp6_4_test;


	DUT : comp6_4 port map(
		-- input signals
		L1_S1		=> L1_S1_test,
		L1_S2		=> L1_S2_test,
		L1_C		=> L1_C_test,
		L2_S1		=> L2_S1_test,
		L2_S2		=> L2_S2_test,
		L2_C		=> L2_C_test,

		-- output signals
		L1_sum		=> L1_sum_test,
		L1_carry	=> L1_carry_test,
		L2_sum		=> L2_sum_test,
		L2_carry	=> L2_carry_test
	);

end architecture test;
