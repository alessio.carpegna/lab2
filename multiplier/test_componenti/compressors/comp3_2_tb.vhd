library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;


entity comp3_2_tb is
end entity comp3_2_tb;

architecture test of comp3_2_tb is
component comp3_2
	generic(n_component_first_row: integer := 58);

	port(
		-- input signals
		L1_S1		: in std_logic_vector(n_component_first_row downto 0);
		L1_S2		: in std_logic_vector(n_component_first_row downto 0);
		L1_C		: in std_logic_vector(n_component_first_row -1 downto 2);

		--output signals
		L1_sum		: out std_logic_vector(n_component_first_row downto 0);
		L1_carry	: out std_logic_vector(n_component_first_row downto 0)
	);

	end component comp3_2;
	constant n_component_first_row	: integer := 58;

	-- test input signals
	signal L1_S1_test	: std_logic_vector(n_component_first_row downto 0);
	signal L1_S2_test	: std_logic_vector(n_component_first_row downto 0);
	signal L1_C_test	: std_logic_vector(n_component_first_row -1 downto 2);
	
	--test output signals
	signal L1_sum_test	: std_logic_vector(n_component_first_row downto 0);
	signal L1_carry_test	: std_logic_vector(n_component_first_row downto 0);
	
	file infile  : text open read_mode is "comp3_2_inputs.txt";
	file outfile : text open write_mode is "comp3_2_outputs.txt";   
	
begin

	comp3_2_test : process
	
		variable inLine		: line;
		variable outLine	: line;

		variable v_in		: integer; 
		variable v_out		: integer;
	
	begin

		if not endfile(inFile)
		then
		
			-- input generation	
			readline(inFile, inLine);
			read(inLine, v_in);
			L1_S1_test <= std_logic_vector(to_unsigned(v_in, L1_S1_test'length)); 
			read(inLine, v_in);
			L1_S2_test <= std_logic_vector(to_unsigned(v_in, L1_S2_test'length)); 
			read(inLine, v_in);
			L1_C_test <= std_logic_vector(to_unsigned(v_in, L1_C_test'length)); 

			wait for 10 ns;

			-- output stoting
			v_out	:= to_integer(unsigned(L1_sum_test));
			write(outLine, v_out);
			write(outline,string'(" "));
			v_out	:= to_integer(unsigned(L1_carry_test));
			write(outLine, v_out);
			writeline(outFile, outLine);
      else 
         wait;
		end if;

	end process comp3_2_test;


	DUT : comp3_2 port map(
		-- input signals
		L1_S1		=> L1_S1_test,
		L1_S2		=> L1_S2_test,
		L1_C		=> L1_C_test,

		-- output signals
		L1_sum		=> L1_sum_test,
		L1_carry	=> L1_carry_test
	);

end architecture test;
