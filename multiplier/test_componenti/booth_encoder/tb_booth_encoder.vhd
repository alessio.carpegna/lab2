library ieee;
use ieee.std_logic_1164.all;

entity tb_booth_encoder is
end entity tb_booth_encoder;

architecture simulation of tb_booth_encoder is

component booth_encoder
port(

	-- input signals
	b	: in std_logic_vector(31 downto 0);
	a	: in std_logic_vector(31 downto 0);

	-- output signals
	pp1	: out std_logic_vector(35 downto 0);
	pp2	: out std_logic_vector(36 downto 0);
	pp3	: out std_logic_vector(36 downto 0);
	pp4	: out std_logic_vector(36 downto 0);
	pp5	: out std_logic_vector(36 downto 0);
	pp6	: out std_logic_vector(36 downto 0);
	pp7	: out std_logic_vector(36 downto 0);
	pp8	: out std_logic_vector(36 downto 0);
	pp9	: out std_logic_vector(36 downto 0);
	pp10	: out std_logic_vector(36 downto 0);
	pp11	: out std_logic_vector(36 downto 0);
	pp12	: out std_logic_vector(36 downto 0);	
	pp13	: out std_logic_vector(36 downto 0);
	pp14	: out std_logic_vector(36 downto 0);
	pp15	: out std_logic_vector(36 downto 0);
	pp16	: out std_logic_vector(35 downto 0);
	pp17	: out std_logic_vector(33 downto 0)
);
end component booth_encoder;


-- Ingressi
signal test_a : std_logic_vector(31 downto 0);
signal test_b : std_logic_vector(31 downto 0);

signal test_pp1 			: std_logic_vector(35 downto 0);
signal test_pp2 			: std_logic_vector(36 downto 0);
signal test_pp3,test_pp4,test_pp5	: std_logic_vector(36 downto 0);
signal test_pp6,test_pp7,test_pp8	: std_logic_vector(36 downto 0);
signal test_pp9,test_pp10,test_pp11	: std_logic_vector(36 downto 0);
signal test_pp12,test_pp13,test_pp14	: std_logic_vector(36 downto 0);
signal test_pp15			: std_logic_vector(36 downto 0);
signal test_pp16 			: std_logic_vector(35 downto 0);
signal test_pp17 			: std_logic_vector(33 downto 0);

file outfile : text open write_mode is "output_dadda_tree.txt";

begin 

DUT : booth_encoder port map(
			b 	=> test_b,
			a	=> test_a,
			pp1	=> test_pp1,
			pp2	=> test_pp2,
			pp3	=> test_pp3,
			pp4	=> test_pp4,
			pp5	=> test_pp5,
			pp6	=> test_pp6,
			pp7	=> test_pp7,
			pp8	=> test_pp8,
			pp9	=> test_pp9,
			pp10	=> test_pp10,
			pp11	=> test_pp11,
			pp12	=> test_pp12,
			pp13	=> test_pp13,
			pp14	=> test_pp14,
			pp15	=> test_pp15,
			pp16	=> test_pp16,
			pp17	=> test_pp17
		);


	testing : process

		variable outLine	: line;
		variable v_pp1		: std_logic_vector(35 downto 0);
       		variable v_pp2		: std_logic_vector(36 downto 0);
		variable v_pp17		: std_logic_vector(33 downto 0);		


	begin
		test_a	<= "00000000000000000000000000000010";
		test_b	<= "00000000000000000000000000000011";

		wait 10 ns;



	end process testing;

end architecture simulation;
