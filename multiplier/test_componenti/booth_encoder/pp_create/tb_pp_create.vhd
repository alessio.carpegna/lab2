library ieee;
use ieee.std_logic_1164.all;

entity tb_pp_create is
end entity tb_pp_create;

architecture simulation of tb_pp_create is

component pp_create
port(
	-- Ingressi
	b	: in std_logic_vector(2 downto 0);
	a	: in std_logic_vector(31 downto 0);	
	-- Uscite
	pp	: out std_logic_vector(32 downto 0);
	s 	: out std_logic
);
end component pp_create; 

-- Ingressi
signal test_b	: std_logic_vector(2 downto 0);
signal test_a	: std_logic_vector(31 downto 0);


-- Uscite 
signal test_pp	: std_logic_vector(32 downto 0);
signal test_s	: std_logic;

begin

DUT : pp_create port map(
		b	=> test_b,
		a	=> test_a,
		pp	=> test_pp,
		s	=> test_s
		);

input_gen : process

begin
	test_a	<= "01101000101000111010001011010101";	-- Ingresso casuale
	test_b	<= "000";
	wait for 10 ns;
	test_b	<= "001";
	wait for 10 ns;
	test_b	<= "010";
	wait for 10 ns;
	test_b	<= "011";
	wait for 10 ns;
	test_b	<= "100";
	wait for 10 ns;
	test_b	<= "101";
	wait for 10 ns;
	test_b	<= "110";
	wait for 10 ns;
	test_b	<= "111";
	wait;

end process input_gen;

end architecture simulation;
