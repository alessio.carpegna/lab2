-- The left compressors are used to compress the bits on the sides of
-- the FA/HA blocks. In general they compress from (N-1), where N is
-- the number of rows of the current block, to (M-1), where M is the
-- number of rows of the next block. This block however compresses
-- from (N-2) to (M-1)
--
--	Example: to compress from L6 to L4 a side compressor
--		4 to 3 is needed.
--
-- leftCompressor4_3
---------------------------------------------------------------------
-- The component expects to receive the rows of the block from row 1
-- to row 4 (numbering the rows from 1 to 6). It returns the
-- compressed rows from row 2 to row 4. This convention is used for
-- the names of the input/output signals).

library ieee;
use ieee.std_logic_1164.all;

entity leftCompressor4_3 is

	port(
		-- 7 inputs
		inRow1		: in std_logic;
		inRow2		: in std_logic;
		inRow3		: in std_logic_vector(7 downto 6);
		inRow4		: in std_logic_vector(6 downto 5);
	
		-- 5 outputs
		outRow2		: out std_logic;
		outRow3		: out std_logic_vector(7 downto 6);
		outRow4		: out std_logic_vector(7 downto 5)
	);

end entity leftCompressor4_3;

architecture behaviour of leftCompressor4_3 is
begin

	outRow2 <= inRow1;
	outRow3 <= inRow2 & inRow3(6);
	outRow4 <= inRow3(7) & inRow4(6) & inRow4(5);

end architecture behaviour;
