-- The right compressors are used to compress the bits on the sides of
-- the FA/HA blocks. In general they compress from (N-2), where N is
-- the number of rows of the current block, to (M-1), where M is the
-- number of rows of the next block.
--
--	Example: to compress from L17 to L13 a side compressor
--		15 to 12 is needed.
--
-- rightCompressor15_12
---------------------------------------------------------------------
-- The component expects to receive the rows of the block from row 3
-- to row 17 (numbering the rows from 1 to 17). It returns the
-- compressed rows from row 2 to row 13. This convention is used for
-- the names of the input/output signals).

library ieee;
use ieee.std_logic_1164.all;

entity rightCompressor15_12 is

	port(
		-- 15 inputs
		inRow3		: in std_logic_vector(1 downto 0);
		inRow4		: in std_logic_vector(1 downto 0);
		inRow5		: in std_logic_vector(1 downto 0);
		inRow6		: in std_logic_vector(3 downto 0);
		inRow7		: in std_logic_vector(3 downto 0);
		inRow8		: in std_logic_vector(3 downto 0);
		inRow9		: in std_logic_vector(5 downto 0);
		inRow10		: in std_logic_vector(5 downto 0);
		inRow11		: in std_logic_vector(5 downto 0);
		inRow12		: in std_logic_vector(7 downto 0);
		inRow13		: in std_logic_vector(7 downto 0);
		inRow14		: in std_logic_vector(7 downto 0);
		inRow15		: in std_logic_vector(7 downto 2);
		inRow16		: in std_logic_vector(7 downto 4);
		inRow17		: in std_logic_vector(7 downto 6);


		-- 12 outputs
		outRow2		: out std_logic;
		outRow3		: out std_logic_vector(1 downto 0);
		outRow4		: out std_logic_vector(2 downto 0);
		outRow5		: out std_logic_vector(3 downto 0);
		outRow6		: out std_logic_vector(4 downto 0);
		outRow7		: out std_logic_vector(5 downto 0);
		outRow8		: out std_logic_vector(6 downto 0);
		outRow9		: out std_logic_vector(7 downto 0);
		outRow10	: out std_logic_vector(7 downto 0);
		outRow11	: out std_logic_vector(7 downto 0);
		outRow12	: out std_logic_vector(7 downto 0);
		outRow13	: out std_logic_vector(7 downto 0)
	);
end entity rightCompressor15_12;

architecture behaviour of rightCompressor15_12 is
begin

	outRow2 <= inRow3(0);
	outRow3 <= inRow3(1) & inRow4(0);
	outRow4 <= inRow6(2) & inRow4(1) & inRow5(0);
	outRow5 <= inRow6(3) & inRow7(2) & inRow5(1) & inRow6(0);
	outRow6 <= inRow9(4) & inRow7(3) & inRow8(2) & inRow6(1) & inRow7(0);
	outRow7 <= inRow9(5) & inRow10(4) & inRow8(3) & inRow9(2) & inRow7(1) & inRow8(0);
	outRow8 <= inRow12(6) & inRow10(5) & inRow11(4) & inRow9(3) & inRow10(2) & inRow8(1) & inRow9(0);
	outRow9 <= inRow12(7) & inRow13(6) & inRow11(5) & inRow12(4) & inRow10(3) & inRow11(2) & inRow9(1) & inRow10(0);
	outRow10 <= inRow13(7) & inRow14(6) & inRow12(5) & inRow13(4) & inRow11(3) & inRow12(2) & inRow10(1) & inRow11(0);
	outRow11 <= inRow14(7) & inRow15(6) & inRow13(5) & inRow14(4) & inRow12(3) & inRow13(2) & inRow11(1) & inRow12(0);
	outRow12 <= inRow15(7) & inRow16(6) & inRow14(5) & inRow15(4) & inRow13(3) & inRow14(2) & inRow12(1) & inRow13(0);
	outRow13 <= inRow16(7) & inRow17(6) & inRow15(5) & inRow16(4) & inRow14(3) & inRow15(2) & inRow13(1) & inRow14(0);

end architecture behaviour;
