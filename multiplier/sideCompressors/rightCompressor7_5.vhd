-- The right compressors are used to compress the bits on the sides of
-- the FA/HA blocks. In general they compress from (N-2), where N is
-- the number of rows of the current block, to (M-1), where M is the
-- number of rows of the next block.
--
--	Example: to compress from L9 to L6 a side compressor
--		7 to 5 is needed.
--
-- rightCompressor7_5
---------------------------------------------------------------------
-- The component expects to receive the rows of the block from row 3
-- to row 9 (numbering the rows from 1 to 9). It returns the
-- compressed rows from row 2 to row 6. This convention is used for
-- the names of the input/output signals).

library ieee;
use ieee.std_logic_1164.all;

entity rightCompressor7_5 is

	port(
		-- 7 inputs
		inRow3		: in std_logic_vector(1 downto 0);
		inRow4		: in std_logic_vector(1 downto 0);
		inRow5		: in std_logic_vector(1 downto 0);
		inRow6		: in std_logic_vector(3 downto 0);
		inRow7		: in std_logic_vector(3 downto 0);
		inRow8		: in std_logic_vector(3 downto 2);
		inRow9		: in std_logic_vector(5 downto 4);

		-- 5 outputs
		outRow2		: out std_logic;
		outRow3		: out std_logic_vector(1 downto 0);
		outRow4		: out std_logic_vector(2 downto 0);
		outRow5		: out std_logic_vector(3 downto 0);
		outRow6		: out std_logic_vector(4 downto 0)
	);
end entity rightCompressor7_5;

architecture behaviour of rightCompressor7_5 is
begin

	outRow2 <= inRow3(0);
	outRow3 <= inRow3(1) & inRow4(0);
	outRow4 <= inRow6(2) & inRow4(1) & inRow5(0);
	outRow5 <= inRow6(3) & inRow7(2) & inRow5(1) & inRow6(0);
	outRow6 <= inRow9(4) & inRow7(3) & inRow8(2) & inRow6(1) & inRow7(0);

end architecture behaviour;
