-- The left compressors are used to compress the bits on the sides of
-- the FA/HA blocks. In general they compress from (N-1), where N is
-- the number of rows of the current block, to (M-1), where M is the
-- number of rows of the next block. From this block over however it
-- compresses from (N-2) to (M-1)
--
--	Example: to compress from L9 to L6 a side compressor
--		7 to 5 is needed.
--
-- leftCompressor7_5
---------------------------------------------------------------------
-- The component expects to receive the rows of the block from row 1
-- to row 7 (numbering the rows from 1 to 9). It returns the
-- compressed rows from row 2 to row 6. This convention is used for
-- the names of the input/output signals).

library ieee;
use ieee.std_logic_1164.all;

entity leftCompressor7_5 is

	port(
		-- 7 inputs
		inRow1		: in std_logic;
		inRow2		: in std_logic;
		inRow3		: in std_logic_vector(7 downto 6);
		inRow4		: in std_logic_vector(7 downto 5);
		inRow5		: in std_logic_vector(7 downto 5);
		inRow6		: in std_logic_vector(6 downto 4);
		inRow7		: in std_logic_vector(4 downto 3);
	
		-- 5 outputs
		outRow2		: out std_logic;
		outRow3		: out std_logic_vector(7 downto 6);
		outRow4		: out std_logic_vector(7 downto 5);
		outRow5		: out std_logic_vector(7 downto 4);
		outRow6		: out std_logic_vector(7 downto 3)
	);

end entity leftCompressor7_5;

architecture behaviour of leftCompressor7_5 is
begin

	outRow2 <= inRow1;
	outRow3 <= inRow2 & inRow3(6);
	outRow4 <= inRow3(7) & inRow4(6) & inRow4(5);
	outRow5 <= inRow4(7) & inRow5(6) & inRow5(5) & inRow6(4);
	outRow6 <= inRow5(7) & inRow6(6) & inRow6(5) & inRow7(4) & inRow7(3);

end architecture behaviour;
