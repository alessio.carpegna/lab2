-- The left compressors are used to compress the bits on the sides of
-- the FA/HA blocks. In general they compress from (N-1), where N is
-- the number of rows of the current block, to (M-1), where M is the
-- number of rows of the next block.
--
--	Example: to compress from L17 to L13 a side compressor
--		16 to 12 is needed.
--
-- leftCompressor16_12
---------------------------------------------------------------------
-- The component expects to receive the rows of the block from row 1
-- to row 16 (numbering the rows from 1 to 17). It returns the
-- compressed rows from row 2 to row 13. This convention is used for
-- the names of the input/output signals).

library ieee;
use ieee.std_logic_1164.all;

entity leftCompressor16_12 is

	port(
		-- 16 inputs
		inRow1		: in std_logic;
		inRow2		: in std_logic;
		inRow3		: in std_logic_vector(7 downto 6);
		inRow4		: in std_logic_vector(7 downto 5);
		inRow5		: in std_logic_vector(7 downto 5);
		inRow6		: in std_logic_vector(7 downto 4);
		inRow7		: in std_logic_vector(7 downto 3);
		inRow8		: in std_logic_vector(7 downto 3);
		inRow9		: in std_logic_vector(7 downto 2);
		inRow10		: in std_logic_vector(7 downto 1);
		inRow11		: in std_logic_vector(7 downto 1);
		inRow12		: in std_logic_vector(7 downto 0);
		inRow13		: in std_logic_vector(6 downto 0);
		inRow14		: in std_logic_vector(4 downto 0);
		inRow15		: in std_logic_vector(2 downto 0);
		inRow16		: in std_logic;


		-- 12 outputs
		outRow2		: out std_logic;
		outRow3		: out std_logic_vector(7 downto 6);
		outRow4		: out std_logic_vector(7 downto 5);
		outRow5		: out std_logic_vector(7 downto 4);
		outRow6		: out std_logic_vector(7 downto 3);
		outRow7		: out std_logic_vector(7 downto 2);
		outRow8		: out std_logic_vector(7 downto 1);
		outRow9		: out std_logic_vector(7 downto 0);
		outRow10	: out std_logic_vector(7 downto 0);
		outRow11	: out std_logic_vector(7 downto 0);
		outRow12	: out std_logic_vector(7 downto 0);
		outRow13	: out std_logic_vector(7 downto 0)
	);
end entity leftCompressor16_12;

architecture behaviour of leftCompressor16_12 is
begin

	outRow2 <= inRow1;
	outRow3 <= inRow2 & inRow3(6);
	outRow4 <= inRow3(7) & inRow4(6) & inRow4(5);
	outRow5 <= inRow4(7) & inRow5(6) & inRow5(5) & inRow6(4);
	outRow6 <= inRow5(7) & inRow6(6) & inRow6(5) & inRow7(4) & inRow7(3);
	outRow7 <= inRow6(7) & inRow7(6) & inRow7(5) & inRow8(4) & inRow8(3) & inRow9(2);
	outRow8 <= inRow7(7) & inRow8(6) & inRow8(5) & inRow9(4) & inRow9(3) & inRow10(2) & inRow10(1);
	outRow9 <= inRow8(7) & inRow9(6) & inRow9(5) & inRow10(4) & inRow10(3) & inRow11(2) & inRow11(1) & inRow12(0);
	outRow10 <= inRow9(7) & inRow10(6) & inRow10(5) & inRow11(4) & inRow11(3) & inRow12(2) & inRow12(1) & inRow13(0);
	outRow11 <= inRow10(7) & inRow11(6) & inRow11(5) & inRow12(4) & inRow12(3) & inRow13(2) & inRow13(1) & inRow14(0);
	outRow12 <= inRow11(7) & inRow12(6) & inRow12(5) & inRow13(4) & inRow13(3) & inRow14(2) & inRow14(1) & inRow15(0);
	outRow13 <= inRow12(7) & inRow13(6) & inRow13(5) & inRow14(4) & inRow14(3) & inRow15(2) & inRow15(1) & inRow16;

end architecture behaviour;
