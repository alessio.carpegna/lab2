-- The left compressors are used to compress the bits on the sides of
-- the FA/HA blocks. In general they compress from (N-1), where N is
-- the number of rows of the current block, to (M-1), where M is the
-- number of rows of the next block. 
--
--	Example: to compress from L4 to L3 a side compressor
--		3 to 2 is needed.
--
-- leftCompressor4_3
---------------------------------------------------------------------
-- The component expects to receive the rows of the block from row 1
-- to row 3 (numbering the rows from 1 to 4). It returns the
-- compressed rows from row 2 to row 3. This convention is used for
-- the names of the input/output signals).

library ieee;
use ieee.std_logic_1164.all;

entity leftCompressor3_2 is

	port(
		-- 7 inputs
		inRow1		: in std_logic;
		inRow2		: in std_logic;
		inRow3		: in std_logic;
	
		-- 5 outputs
		outRow2		: out std_logic;
		outRow3		: out std_logic_vector(7 downto 6)
	);

end entity leftCompressor3_2;

architecture behaviour of leftCompressor3_2 is
begin

	outRow2 <= inRow1;
	outRow3 <= inRow2 & inRow3;

end architecture behaviour;
