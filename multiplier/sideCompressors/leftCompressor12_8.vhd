-- The left compressors are used to compress the bits on the sides of
-- the FA/HA blocks. In general they compress from (N-1), where N is
-- the number of rows of the current block, to (M-1), where M is the
-- number of rows of the next block.
--
--	Example: to compress from L13 to L7 a side compressor
--		13 to 7 is needed.
--
-- leftCompressor12_8
---------------------------------------------------------------------
-- The component expects to receive the rows of the block from row 1
-- to row 12 (numbering the rows from 1 to 13). It returns the
-- compressed rows from row 2 to row 9. This convention is used for
-- the names of the input/output signals).

library ieee;
use ieee.std_logic_1164.all;

entity leftCompressor12_8 is

	port(
		-- 12 inputs
		inRow1		: in std_logic;
		inRow2		: in std_logic;
		inRow3		: in std_logic_vector(7 downto 6);
		inRow4		: in std_logic_vector(7 downto 5);
		inRow5		: in std_logic_vector(7 downto 5);
		inRow6		: in std_logic_vector(7 downto 4);
		inRow7		: in std_logic_vector(7 downto 3);
		inRow8		: in std_logic_vector(7 downto 3);
		inRow9		: in std_logic_vector(6 downto 2);
		inRow10		: in std_logic_vector(4 downto 1);
		inRow11		: in std_logic_vector(2 downto 1);
		inRow12		: in std_logic;
	
		-- 8 outputs
		outRow2		: out std_logic;
		outRow3		: out std_logic_vector(7 downto 6);
		outRow4		: out std_logic_vector(7 downto 5);
		outRow5		: out std_logic_vector(7 downto 4);
		outRow6		: out std_logic_vector(7 downto 3);
		outRow7		: out std_logic_vector(7 downto 2);
		outRow8		: out std_logic_vector(7 downto 1);
		outRow9		: out std_logic_vector(7 downto 0)
	);

end entity leftCompressor12_8;

architecture behaviour of leftCompressor12_8 is
begin

	outRow2 <= inRow1;
	outRow3 <= inRow2 & inRow3(6);
	outRow4 <= inRow3(7) & inRow4(6) & inRow4(5);
	outRow5 <= inRow4(7) & inRow5(6) & inRow5(5) & inRow6(4);
	outRow6 <= inRow5(7) & inRow6(6) & inRow6(5) & inRow7(4) & inRow7(3);
	outRow7 <= inRow6(7) & inRow7(6) & inRow7(5) & inRow8(4) & inRow8(3) & inRow9(2);
	outRow8 <= inRow7(7) & inRow8(6) & inRow8(5) & inRow9(4) & inRow9(3) & inRow10(2) & inRow10(1);
	outRow9 <= inRow8(7) & inRow9(6) & inRow9(5) & inRow10(4) & inRow10(3) & inRow11(2) & inRow11(1) & inRow12;

end architecture behaviour;
