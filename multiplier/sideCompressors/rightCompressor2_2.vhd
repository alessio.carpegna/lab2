-- The right compressors are used to compress the bits on the sides of
-- the FA/HA blocks. In general they compress from (N-2), where N is
-- the number of rows of the current block, to (M-1), where M is the
-- number of rows of the next block.
--
--	Example: to compress from L4 to L3 a side compressor
--		2 to 2 is needed.
--
-- rightCompressor2_2
---------------------------------------------------------------------
-- The component expects to receive the rows of the block from row 3
-- to row 4 (numbering the rows from 1 to 4). It returns the
-- compressed rows from row 2 to row 3. This convention is used for
-- the names of the input/output signals).

library ieee;
use ieee.std_logic_1164.all;

entity rightCompressor2_2 is

	port(
		-- 4 inputs
		inRow3		: in std_logic_vector(1 downto 0);
		inRow4		: in std_logic_vector(1 downto 0);

		-- 3 outputs
		outRow2		: out std_logic;
		outRow3		: out std_logic_vector(1 downto 0)
	);
end entity rightCompressor2_2;

architecture behaviour of rightCompressor2_2 is
begin

	outRow2 <= inRow3(0);
	outRow3 <= inRow3(1) & inRow4(0);

end architecture behaviour;
