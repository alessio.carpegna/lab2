-- The right compressors are used to compress the bits on the sides of
-- the FA/HA blocks. In general they compress from (N-2), where N is
-- the number of rows of the current block, to (M-1), where M is the
-- number of rows of the next block.
--
--	Example: to compress from L6 to L4 a side compressor
--		4 to 3 is needed.
--
-- rightCompressor4_3
---------------------------------------------------------------------
-- The component expects to receive the rows of the block from row 3
-- to row 6 (numbering the rows from 1 to 6). It returns the
-- compressed rows from row 2 to row 4. This convention is used for
-- the names of the input/output signals).

library ieee;
use ieee.std_logic_1164.all;

entity rightCompressor4_3 is

	port(
		-- 4 inputs
		inRow3		: in std_logic_vector(1 downto 0);
		inRow4		: in std_logic_vector(1 downto 0);
		inRow5		: in std_logic_vector(1 downto 0);
		inRow6		: in std_logic_vector(3 downto 2);

		-- 3 outputs
		outRow2		: out std_logic;
		outRow3		: out std_logic_vector(1 downto 0);
		outRow4		: out std_logic_vector(2 downto 0)
	);
end entity rightCompressor4_3;

architecture behaviour of rightCompressor4_3 is
begin

	outRow2 <= inRow3(0);
	outRow3 <= inRow3(1) & inRow4(0);
	outRow4 <= inRow6(2) & inRow4(1) & inRow5(0);

end architecture behaviour;
