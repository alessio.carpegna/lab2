library ieee;
use ieee.std_logic_1164.all;

entity L4 is

	port(
		-- partial product as inputs
		L4_in1		: in std_logic_vector(63 downto 0);
		L4_in2		: in std_logic_vector(63 downto 0);
		L4_in3		: in std_logic_vector(62 downto 2);
		L4_in4		: in std_logic_vector(61 downto 4);
		
		L4_out1		: out std_logic_vector(63 downto 0);
		L4_out2		: out std_logic_vector(63 downto 0);
		L4_out3		: out std_logic_vector(63 downto 2)
	);
end entity L4;

architecture structure of L4 is

	component comp3_2 
	
		generic(n_component_first_row: integer := 58);
	
		port(
			-- input signals
			L1_S1		: in std_logic_vector(n_component_first_row downto 0);
			L1_S2		: in std_logic_vector(n_component_first_row downto 0);
			L1_C		: in std_logic_vector(n_component_first_row -1 downto 2);

			--output signals
			L1_sum		: out std_logic_vector(n_component_first_row downto 0);
			L1_carry	: out std_logic_vector(n_component_first_row downto 0)
		);
	
	end component comp3_2;
	
	component remapper2
	
		generic(N: integer := 58);
		
		port(
				-- input signals
			l1_sum		: in std_logic_vector(N downto 0);
			l1_carry	: in std_logic_vector(N downto 0);

			--output signals
			l1_s1		: out std_logic_vector(N+1 downto 0);
			l1_s2		: out std_logic_vector(N-1 downto 0)
		);
		end component remapper2;
		
	component leftCompressor3_2	
	
		port(
			-- 3 inputs
			inRow1		: in std_logic;
			inRow2		: in std_logic;
			inRow3		: in std_logic;
		
			-- 2 outputs
			outRow2		: out std_logic;
			outRow3		: out std_logic_vector(7 downto 6)
		);
		
	end component;	

	component rightCompressor2_2
	
		port(
			-- 2 inputs
			inRow3		: in std_logic_vector(1 downto 0);
			inRow4		: in std_logic_vector(1 downto 0);

			-- 2 outputs
			outRow2		: out std_logic;
			outRow3		: out std_logic_vector(1 downto 0)
		);
	end component;
		
	-- signals to connect compressor to remapper
	signal l1_sum		: std_logic_vector(58 downto 0);
	signal l1_carry		: std_logic_vector(58 downto 0);
	
begin

		Semplification1: comp3_2 generic map (n_component_first_row => 58)
			port map(
				L1_S1 	=> L4_in1(62 downto 4),
				L1_S2	=> L4_in2(62 downto 4),
				L1_C	=> L4_in3(61 downto 6),
				
				L1_sum		=> l1_sum,
				L1_carry	=> l1_carry
			);
			
		Remap: remapper2 generic map(N => 58)
			port map(
				l1_sum		=> l1_sum,
				l1_carry	=> l1_carry,
			
				l1_s1	=> L4_out1(63 downto 4),
				l1_s2	=> L4_out2(62 downto 5)
			);
			
		rightCompr: rightCompressor2_2
			port map(
				inRow3	=> L4_in3(5 downto 4),
				inRow4	=> L4_in4(5 downto 4),
				
				outRow2	=> L4_out2(4),
				outRow3	=> L4_out3(5 downto 4)
		);
		
		leftCompr: leftCompressor3_2 
			port map(
				inRow1	=> L4_in1(63),
				inRow2	=> L4_in2(63),
				inRow3	=> L4_in3(62),
				
				outRow2	=> L4_out2(63),
				outRow3	=> L4_out3(63 downto 62)
		);
			
		-- RECOSTRUCTION OF THE OUTPUTS
						
			-- Right triangle
			L4_out1(3 downto 0)	<= L4_in1(3 downto 0);
			L4_out2(3 downto 0)	<= L4_in2(3 downto 0);
			L4_out3(3 downto 2)	<= L4_in3(3 downto 2);

			-- Square below
			L4_out3(61 downto 6)	<= L4_in4(61 downto 6);
	
end architecture structure;
