library ieee;
use ieee.std_logic_1164.all;

entity L6 is

	port(
		-- partial product as inputs
		L6_in1		: in std_logic_vector(63 downto 0);
		L6_in2		: in std_logic_vector(63 downto 0);
		L6_in3		: in std_logic_vector(62 downto 2);
		L6_in4		: in std_logic_vector(60 downto 4);
		L6_in5		: in std_logic_vector(58 downto 6);
		L6_in6		: in std_logic_vector(57 downto 8);
		
		L6_out1		: out std_logic_vector(63 downto 0);
		L6_out2		: out std_logic_vector(63 downto 0);
		L6_out3		: out std_logic_vector(62 downto 2);
		L6_out4		: out std_logic_vector(61 downto 4)
	);
end entity L6;

architecture structure of L6 is

	component comp6_4 
	
		generic(n_component_first_row: integer := 54);
	
		port(
	
			-- input signals
			L1_S1		: in std_logic_vector(n_component_first_row downto 0);
			L1_S2		: in std_logic_vector(n_component_first_row downto 0);
			L1_C		: in std_logic_vector(n_component_first_row -1 downto 2);
			
			L2_S1		: in std_logic_vector(n_component_first_row -2 downto 2);
			L2_S2		: in std_logic_vector(n_component_first_row -2 downto 2);
			L2_C		: in std_logic_vector(n_component_first_row-3 downto 4);

			--output signals
			L1_sum		: out std_logic_vector(n_component_first_row downto 0);
			L1_carry	: out std_logic_vector(n_component_first_row downto 0);
			
			L2_sum		: out std_logic_vector(n_component_first_row -2 downto 2);
			L2_carry	: out std_logic_vector(n_component_first_row -2 downto 2)
		);
	
	end component comp6_4;
	
	component remapper4
	
		generic(N: integer := 54);
		
		port(
				-- input signals
			l1_sum		: in std_logic_vector(N downto 0);
			l1_carry	: in std_logic_vector(N downto 0);
			l2_sum		: in std_logic_vector(N-4 downto 0);
			l2_carry	: in std_logic_vector(N-4 downto 0);

			--output signals
			l1_s1		: out std_logic_vector(N+1 downto 0);
			l1_s2		: out std_logic_vector(N-1 downto 0);
			l1_c		: out std_logic_vector(N-3 downto 0);
			l2_s1		: out std_logic_vector(N-5 downto 0)
		);
		end component remapper4;
		
	component leftCompressor4_3	
	
		port(
			-- 4 inputs
			inRow1		: in std_logic;
			inRow2		: in std_logic;
			inRow3		: in std_logic_vector(7 downto 6);
			inRow4		: in std_logic_vector(6 downto 5);
		
			-- 3 outputs
			outRow2		: out std_logic;
			outRow3		: out std_logic_vector(7 downto 6);
			outRow4		: out std_logic_vector(7 downto 5)
		);
		
	end component;	

	component rightCompressor4_3
	
		port(
			-- 4 inputs
			inRow3		: in std_logic_vector(1 downto 0);
			inRow4		: in std_logic_vector(1 downto 0);
			inRow5		: in std_logic_vector(1 downto 0);
			inRow6		: in std_logic_vector(3 downto 2);

			-- 3 outputs
			outRow2		: out std_logic;
			outRow3		: out std_logic_vector(1 downto 0);
			outRow4		: out std_logic_vector(2 downto 0)
		);
	end component;
	
	-- signals to connect compressor to remapper
	signal l1_sum		: std_logic_vector(54 downto 0);
	signal l1_carry		: std_logic_vector(54 downto 0);
	signal l2_sum		: std_logic_vector(50 downto 0);
	signal l2_carry		: std_logic_vector(50 downto 0);	
	
begin

		Semplification1: comp6_4 generic map (n_component_first_row => 54)
			port map(
				L1_S1 	=> L6_in1(60 downto 6),
				L1_S2	=> L6_in2(60 downto 6),
				L1_C	=> L6_in3(59 downto 8),
				
				L2_S1	=> L6_in4(58 downto 8),
				L2_S2	=> L6_in5(58 downto 8),
				L2_C	=> L6_in6(57 downto 10),
				
				L1_sum		=> l1_sum,
				L1_carry	=> l1_carry,
				L2_sum		=> l2_sum,
				L2_carry	=> l2_carry
			);
			
		Remap: remapper4 generic map(N => 54)
			port map(
				l1_sum		=> l1_sum,
				l1_carry	=> l1_carry,
				l2_sum		=> l2_sum,
				l2_carry	=> l2_carry,
			
				l1_s1	=> L6_out1(61 downto 6),
				l1_s2	=> L6_out2(60 downto 7),
				l1_c	=> L6_out3(59 downto 8),
				l2_s1	=> L6_out4(58 downto 9)
			);
			
		rightCompr: rightCompressor4_3 
			port map(
				inRow3	=> L6_in3(7 downto 6),
				inRow4	=> L6_in4(7 downto 6),
				inRow5	=> L6_in5(7 downto 6),
				inRow6	=> L6_in6(9 downto 8),
				
				outRow2		=> L6_out2(6),
				outRow3		=> L6_out3(7 downto 6),
				outRow4		=> L6_out4(8 downto 6)
		);
		
		leftCompr: leftCompressor4_3 
			port map(
				inRow1	=> L6_in1(61),
				inRow2	=> L6_in2(61),
				inRow3	=> L6_in3(61 downto 60),
				inRow4	=> L6_in4(60 downto 59),
				
				outRow2		=> L6_out2(61),
				outRow3		=> L6_out3(61 downto 60),
				outRow4		=> L6_out4(61 downto 59)
		);
			
		-- RECOSTRUCTION OF THE OUTPUTS
			
			-- Left triangle
			L6_out1(63 downto 62)	<= L6_in1(63 downto 62);
			L6_out2(63 downto 62)	<= L6_in2(63 downto 62);
			L6_out3(62)				<= L6_in3(62);
			
			-- Right triangle
			L6_out1(5 downto 0)	<= L6_in1(5 downto 0);
			L6_out2(5 downto 0)	<= L6_in2(5 downto 0);
			L6_out3(5 downto 2)	<= L6_in3(5 downto 2);
			L6_out4(5 downto 4)	<= L6_in4(5 downto 4);			
	
end architecture structure;
