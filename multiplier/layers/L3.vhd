library ieee;
use ieee.std_logic_1164.all;

entity L3 is

	port(
		-- partial product as inputs
		L3_in1		: in std_logic_vector(63 downto 0);
		L3_in2		: in std_logic_vector(63 downto 0);
		L3_in3		: in std_logic_vector(63 downto 2);
		
		L3_out1		: out std_logic_vector(63 downto 0);
		L3_out2		: out std_logic_vector(63 downto 0)
	);
end entity L3;

architecture structure of L3 is

	component comp3_2 
	
		generic(n_component_first_row: integer := 62);
	
		port(
			-- input signals
			L1_S1		: in std_logic_vector(n_component_first_row downto 0);
			L1_S2		: in std_logic_vector(n_component_first_row downto 0);
			L1_C		: in std_logic_vector(n_component_first_row -1 downto 2);

			--output signals
			L1_sum		: out std_logic_vector(n_component_first_row downto 0);
			L1_carry	: out std_logic_vector(n_component_first_row downto 0)
		);
	
	end component comp3_2;
	
	component remapper2
	
		generic(N: integer := 61);
		
		port(
				-- input signals
			l1_sum		: in std_logic_vector(N downto 0);
			l1_carry	: in std_logic_vector(N downto 0);

			--output signals
			l1_s1		: out std_logic_vector(N+1 downto 0);
			l1_s2		: out std_logic_vector(N-1 downto 0)
		);
		end component remapper2;
			
	-- signals to connect compressor to remapper
	signal l1_sum	: std_logic_vector(62 downto 0);
	signal l1_carry	: std_logic_vector(62 downto 0);

	signal L3_in1_dummy	: std_logic_vector(L3_in1'length downto 2);
	signal L3_in2_dummy	: std_logic_vector(L3_in2'length downto 2);

	signal l1_s1_dummy	: std_logic_vector(62 downto 0);	
	
begin

		-- dummy bits for the unused HA
		L3_in1_dummy <= '0' & L3_in1(63 downto 2);
		L3_in2_dummy <= '0' & L3_in2(63 downto 2);

		Semplification1: comp3_2 generic map (n_component_first_row => 62)
			port map(
				L1_S1 	=> L3_in1_dummy,
				L1_S2	=> L3_in2_dummy,
				L1_C	=> L3_in3(63 downto 4),
				
				L1_sum		=> l1_sum,
				L1_carry	=> l1_carry
			);
			
		Remap: remapper2 generic map(N => 61)
			port map(
				l1_sum		=> l1_sum(61 downto 0),
				l1_carry	=> l1_carry(61 downto 0),
			
				l1_s1	=> l1_s1_dummy, 
				l1_s2	=> L3_out2(63 downto 3)
			);
			
		L3_out1(63 downto 2) <= l1_s1_dummy(61 downto 0);

		-- RECOSTRUCTION OF THE OUTPUTS
						
			-- Right triangle
			L3_out1(1 downto 0)	<= L3_in1(1 downto 0);
			L3_out2(1 downto 0)	<= L3_in2(1 downto 0);
			
			-- Remaining bit
			L3_out2(2)	<= L3_in3(2);
	
end architecture structure;
