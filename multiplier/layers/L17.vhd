library ieee;
use ieee.std_logic_1164.all;

entity L17 is

	port(
		-- partial product as inputs
		pp1		: in std_logic_vector(63 downto 0);
		pp2		: in std_logic_vector(63 downto 0);
		pp3		: in std_logic_vector(62 downto 2);
		pp4		: in std_logic_vector(60 downto 4);
		pp5		: in std_logic_vector(58 downto 6);
		pp6		: in std_logic_vector(56 downto 8);
		pp7		: in std_logic_vector(54 downto 10);
		pp8		: in std_logic_vector(52 downto 12);
		pp9		: in std_logic_vector(50 downto 14);
		pp10		: in std_logic_vector(48 downto 16);
		pp11		: in std_logic_vector(46 downto 18);
		pp12		: in std_logic_vector(44 downto 20);
		pp13		: in std_logic_vector(42 downto 22);
		pp14		: in std_logic_vector(40 downto 24);
		pp15		: in std_logic_vector(38 downto 26);
		pp16		: in std_logic_vector(36 downto 28);
		pp17		: in std_logic_vector(35 downto 30);
		
		L17_out1	: out std_logic_vector(63 downto 0);
		L17_out2	: out std_logic_vector(63 downto 0);
		L17_out3	: out std_logic_vector(62 downto 2);
		L17_out4	: out std_logic_vector(60 downto 4);
		L17_out5	: out std_logic_vector(58 downto 6);
		L17_out6	: out std_logic_vector(56 downto 8);
		L17_out7	: out std_logic_vector(54 downto 10);
		L17_out8	: out std_logic_vector(52 downto 12);
		L17_out9	: out std_logic_vector(50 downto 14);
		L17_out10	: out std_logic_vector(48 downto 16);
		L17_out11	: out std_logic_vector(46 downto 18);
		L17_out12	: out std_logic_vector(44 downto 20);
		L17_out13	: out std_logic_vector(43 downto 22)
	);
end entity L17;

architecture structure of L17 is

	component comp12_8 
	
		generic(n_component_first_row: integer := 18);
	
		port(
	
			-- input signals
			L1_S1		: in std_logic_vector(n_component_first_row downto 0);
			L1_S2		: in std_logic_vector(n_component_first_row downto 0);
			L1_C		: in std_logic_vector(n_component_first_row -1 downto 2);

			L2_S1		: in std_logic_vector(n_component_first_row -2 downto 2);
			L2_S2		: in std_logic_vector(n_component_first_row -2 downto 2);
			L2_C		: in std_logic_vector(n_component_first_row-3 downto 4);

			L3_S1		: in std_logic_vector(n_component_first_row-4 downto 4);
			L3_S2		: in std_logic_vector(n_component_first_row-4 downto 4);
			L3_C		: in std_logic_vector(n_component_first_row-5 downto 6);

			L4_S1		: in std_logic_vector(n_component_first_row -6 downto 6);
			L4_S2		: in std_logic_vector(n_component_first_row -6 downto 6);
			L4_C		: in std_logic_vector(n_component_first_row -7 downto 8);

			--output signals
			L1_sum		: out std_logic_vector(n_component_first_row downto 0);
			L1_carry	: out std_logic_vector(n_component_first_row downto 0);

			L2_sum		: out std_logic_vector(n_component_first_row -2 downto 2);
			L2_carry	: out std_logic_vector(n_component_first_row -2 downto 2);

			L3_sum		: out std_logic_vector(n_component_first_row -4 downto 4);
			L3_carry	: out std_logic_vector(n_component_first_row -4 downto 4);

			L4_sum		: out std_logic_vector(n_component_first_row -6 downto 6);
			L4_carry	: out std_logic_vector(n_component_first_row -6 downto 6)
		);
	
	end component comp12_8;
	
	component remapper8
	
		generic(N: integer := 18);
		
		port(
				-- input signals
			l1_sum		: in std_logic_vector(N downto 0);
			l1_carry	: in std_logic_vector(N downto 0);
			l2_sum		: in std_logic_vector(N-4 downto 0);
			l2_carry	: in std_logic_vector(N-4 downto 0);
			l3_sum		: in std_logic_vector(N-8 downto 0);
			l3_carry	: in std_logic_vector(N-8 downto 0);
			l4_sum		: in std_logic_vector(N-12 downto 0);
			l4_carry	: in std_logic_vector(N-12 downto 0);

			--output signals
			l1_s1		: out std_logic_vector(N+1 downto 0);
			l1_s2		: out std_logic_vector(N-1 downto 0);
			l1_c		: out std_logic_vector(N-3 downto 0);
			l2_s1		: out std_logic_vector(N-5 downto 0);
			l2_s2		: out std_logic_vector(N-7 downto 0);
			l2_c		: out std_logic_vector(N-9 downto 0);
			l3_s1		: out std_logic_vector(N-11 downto 0);
			l3_s2		: out std_logic_vector(N-13 downto 0)
		);
		end component remapper8;
		
	component leftCompressor16_12 
	
		port(
			-- 16 inputs
			inRow1		: in std_logic;
			inRow2		: in std_logic;
			inRow3		: in std_logic_vector(7 downto 6);
			inRow4		: in std_logic_vector(7 downto 5);
			inRow5		: in std_logic_vector(7 downto 5);
			inRow6		: in std_logic_vector(7 downto 4);
			inRow7		: in std_logic_vector(7 downto 3);
			inRow8		: in std_logic_vector(7 downto 3);
			inRow9		: in std_logic_vector(7 downto 2);
			inRow10		: in std_logic_vector(7 downto 1);
			inRow11		: in std_logic_vector(7 downto 1);
			inRow12		: in std_logic_vector(7 downto 0);
			inRow13		: in std_logic_vector(6 downto 0);
			inRow14		: in std_logic_vector(4 downto 0);
			inRow15		: in std_logic_vector(2 downto 0);
			inRow16		: in std_logic;


			-- 12 outputs
			outRow2		: out std_logic;
			outRow3		: out std_logic_vector(7 downto 6);
			outRow4		: out std_logic_vector(7 downto 5);
			outRow5		: out std_logic_vector(7 downto 4);
			outRow6		: out std_logic_vector(7 downto 3);
			outRow7		: out std_logic_vector(7 downto 2);
			outRow8		: out std_logic_vector(7 downto 1);
			outRow9		: out std_logic_vector(7 downto 0);
			outRow10	: out std_logic_vector(7 downto 0);
			outRow11	: out std_logic_vector(7 downto 0);
			outRow12	: out std_logic_vector(7 downto 0);
			outRow13	: out std_logic_vector(7 downto 0)
		);
	
	end component;
	
	component rightCompressor15_12 
	
		port(
			-- 15 inputs
			inRow3		: in std_logic_vector(1 downto 0);
			inRow4		: in std_logic_vector(1 downto 0);
			inRow5		: in std_logic_vector(1 downto 0);
			inRow6		: in std_logic_vector(3 downto 0);
			inRow7		: in std_logic_vector(3 downto 0);
			inRow8		: in std_logic_vector(3 downto 0);
			inRow9		: in std_logic_vector(5 downto 0);
			inRow10		: in std_logic_vector(5 downto 0);
			inRow11		: in std_logic_vector(5 downto 0);
			inRow12		: in std_logic_vector(7 downto 0);
			inRow13		: in std_logic_vector(7 downto 0);
			inRow14		: in std_logic_vector(7 downto 0);
			inRow15		: in std_logic_vector(7 downto 2);
			inRow16		: in std_logic_vector(7 downto 4);
			inRow17		: in std_logic_vector(7 downto 6);


			-- 12 outputs
			outRow2		: out std_logic;
			outRow3		: out std_logic_vector(1 downto 0);
			outRow4		: out std_logic_vector(2 downto 0);
			outRow5		: out std_logic_vector(3 downto 0);
			outRow6		: out std_logic_vector(4 downto 0);
			outRow7		: out std_logic_vector(5 downto 0);
			outRow8		: out std_logic_vector(6 downto 0);
			outRow9		: out std_logic_vector(7 downto 0);
			outRow10	: out std_logic_vector(7 downto 0);
			outRow11	: out std_logic_vector(7 downto 0);
			outRow12	: out std_logic_vector(7 downto 0);
			outRow13	: out std_logic_vector(7 downto 0)
		);
	
	end component;
		
	-- signals to connect compressor to remapper
	signal l1_sum		: std_logic_vector(18 downto 0);
	signal l1_carry		: std_logic_vector(18 downto 0);
	signal l2_sum		: std_logic_vector(14 downto 0);
	signal l2_carry		: std_logic_vector(14 downto 0);
	signal l3_sum		: std_logic_vector(10 downto 0);
	signal l3_carry		: std_logic_vector(10 downto 0);
	signal l4_sum		: std_logic_vector(6 downto 0);
	signal l4_carry		: std_logic_vector(6 downto 0);
	
	
begin

		Semplification1: comp12_8 generic map (n_component_first_row => 18)
			port map(
				L1_S1 	=> pp1(42 downto 24),
				L1_S2	=> pp2(42 downto 24),
				L1_C	=> pp3(41 downto 26),
				
				L2_S1	=> pp4(40 downto 26),
				L2_S2	=> pp5(40 downto 26),
				L2_C	=> pp6(39 downto 28),
				
				L3_S1	=> pp7(38 downto 28),
				L3_S2	=> pp8(38 downto 28),
				L3_C	=> pp9(37 downto 30),
				
				L4_S1	=> pp10(36 downto 30),
				L4_S2	=> pp11(36 downto 30),
				L4_C	=> pp12(35 downto 32),
				
				L1_sum		=> l1_sum,
				L1_carry	=> l1_carry,
				L2_sum		=> l2_sum,
				L2_carry	=> l2_carry,
				L3_sum		=> l3_sum,
				L3_carry	=> l3_carry,
				L4_sum		=> l4_sum,
				L4_carry	=> l4_carry
			);
			
		Remap: remapper8 generic map(N => 18)
			port map(
				l1_sum		=> l1_sum,
				l1_carry	=> l1_carry,
				l2_sum		=> l2_sum,
				l2_carry	=> l2_carry,
				l3_sum		=> l3_sum,
				l3_carry	=> l3_carry,
				l4_sum		=> l4_sum,
				l4_carry	=> l4_carry,
				
				l1_s1	=> L17_out1(43 downto 24),
				l1_s2	=> L17_out2(42 downto 25),
				l1_c	=> L17_out3(41 downto 26),
				l2_s1	=> L17_out4(40 downto 27),
				l2_s2	=> L17_out5(39 downto 28),
				l2_c	=> L17_out6(38 downto 29),
				l3_s1	=> L17_out7(37 downto 30),
				l3_s2	=> L17_out8(36 downto 31)
			);
			
			
		rightCompr: rightCompressor15_12 
			port map(
				inRow3	=> pp3(25 downto 24),
				inRow4	=> pp4(25 downto 24),
				inRow5	=> pp5(25 downto 24),
				inRow6	=> pp6(27 downto 24),
				inRow7	=> pp7(27 downto 24),
				inRow8	=> pp8(27 downto 24),
				inRow9	=> pp9(29 downto 24),
				inRow10	=> pp10(29 downto 24),
				inRow11	=> pp11(29 downto 24),
				inRow12	=> pp12(31 downto 24),
				inRow13	=> pp13(31 downto 24),
				inRow14	=> pp14(31 downto 24),
				inRow15	=> pp15(31 downto 26),
				inRow16	=> pp16(31 downto 28),
				inRow17	=> pp17(31 downto 30),
				
				outRow2		=> L17_out2(24),
				outRow3		=> L17_out3(25 downto 24),
				outRow4		=> L17_out4(26 downto 24),
				outRow5		=> L17_out5(27 downto 24),
				outRow6		=> L17_out6(28 downto 24),
				outRow7		=> L17_out7(29 downto 24),
				outRow8		=> L17_out8(30 downto 24),
				outRow9		=> L17_out9(31 downto 24),
				outRow10	=> L17_out10(31 downto 24),
				outRow11	=> L17_out11(31 downto 24),
				outRow12	=> L17_out12(31 downto 24),
				outRow13	=> L17_out13(31 downto 24)
		);
		
		leftCompr: leftCompressor16_12 
			port map(
				inRow1	=> pp1(43),
				inRow2	=> pp2(43),
				inRow3	=> pp3(43 downto 42),
				inRow4	=> pp4(43 downto 41),
				inRow5	=> pp5(43 downto 41),
				inRow6	=> pp6(43 downto 40),
				inRow7	=> pp7(43 downto 39),
				inRow8	=> pp8(43 downto 39),
				inRow9	=> pp9(43 downto 38),
				inRow10	=> pp10(43 downto 37),
				inRow11	=> pp11(43 downto 37),
				inRow12	=> pp12(43 downto 36),
				inRow13	=> pp13(42 downto 36),
				inRow14	=> pp14(40 downto 36),
				inRow15	=> pp15(38 downto 36),
				inRow16	=> pp16(36),
				
				outRow2		=> L17_out2(43),
				outRow3		=> L17_out3(43 downto 42),
				outRow4		=> L17_out4(43 downto 41),
				outRow5		=> L17_out5(43 downto 40),
				outRow6		=> L17_out6(43 downto 39),
				outRow7		=> L17_out7(43 downto 38),
				outRow8		=> L17_out8(43 downto 37),
				outRow9		=> L17_out9(43 downto 36),
				outRow10	=> L17_out10(43 downto 36),
				outRow11	=> L17_out11(43 downto 36),
				outRow12	=> L17_out12(43 downto 36),
				outRow13	=> L17_out13(43 downto 36)
		);
		
		-- RECOSTRUCTION OF THE OUTPUTS
		
			-- Left triangle
			L17_out1(63 downto 44)	<= pp1(63 downto 44);
			L17_out2(63 downto 44)	<= pp2(63 downto 44);
			L17_out3(62 downto 44)	<= pp3(62 downto 44);
			L17_out4(60 downto 44)	<= pp4(60 downto 44);
			L17_out5(58 downto 44)	<= pp5(58 downto 44);
			L17_out6(56 downto 44)	<= pp6(56 downto 44);
			L17_out7(54 downto 44)	<= pp7(54 downto 44);
			L17_out8(52 downto 44)	<= pp8(52 downto 44);
			L17_out9(50 downto 44)	<= pp9(50 downto 44);
			L17_out10(48 downto 44)	<= pp10(48 downto 44);
			L17_out11(46 downto 44)	<= pp11(46 downto 44);
			L17_out12(44)			<= pp12(44);
			
			-- Right triangle
			L17_out1(23 downto 0)	<= pp1(23 downto 0);
			L17_out2(23 downto 0)	<= pp2(23 downto 0);
			L17_out3(23 downto 2)	<= pp3(23 downto 2);
			L17_out4(23 downto 4)	<= pp4(23 downto 4);
			L17_out5(23 downto 6)	<= pp5(23 downto 6);
			L17_out6(23 downto 8)	<= pp6(23 downto 8);
			L17_out7(23 downto 10)	<= pp7(23 downto 10);
			L17_out8(23 downto 12)	<= pp8(23 downto 12);
			L17_out9(23 downto 14)	<= pp9(23 downto 14);
			L17_out10(23 downto 16)	<= pp10(23 downto 16);
			L17_out11(23 downto 18)	<= pp11(23 downto 18);
			L17_out12(23 downto 20)	<= pp12(23 downto 20);
			L17_out13(23 downto 22)	<= pp13(23 downto 22);
			
			-- Square below
			L17_out9(35 downto 32)	<= pp13 (35 downto 32);
			L17_out10(35 downto 32)	<= pp14 (35 downto 32);
			L17_out11(35 downto 32)	<= pp15 (35 downto 32);
			L17_out12(35 downto 32)	<= pp16 (35 downto 32);
			L17_out13(35 downto 32)	<= pp17 (35 downto 32);
	
end architecture structure;
