library ieee;
use ieee.std_logic_1164.all;

entity L13 is

	port(
		-- partial product as inputs
		L13_in1		: in std_logic_vector(63 downto 0);
		L13_in2		: in std_logic_vector(63 downto 0);
		L13_in3		: in std_logic_vector(62 downto 2);
		L13_in4		: in std_logic_vector(60 downto 4);
		L13_in5		: in std_logic_vector(58 downto 6);
		L13_in6		: in std_logic_vector(56 downto 8);
		L13_in7		: in std_logic_vector(54 downto 10);
		L13_in8		: in std_logic_vector(52 downto 12);
		L13_in9		: in std_logic_vector(50 downto 14);
		L13_in10	: in std_logic_vector(48 downto 16);
		L13_in11	: in std_logic_vector(46 downto 18);
		L13_in12	: in std_logic_vector(44 downto 20);
		L13_in13	: in std_logic_vector(43 downto 22);
		
		L13_out1	: out std_logic_vector(63 downto 0);
		L13_out2	: out std_logic_vector(63 downto 0);
		L13_out3	: out std_logic_vector(62 downto 2);
		L13_out4	: out std_logic_vector(60 downto 4);
		L13_out5	: out std_logic_vector(58 downto 6);
		L13_out6	: out std_logic_vector(56 downto 8);
		L13_out7	: out std_logic_vector(54 downto 10);
		L13_out8	: out std_logic_vector(52 downto 12);
		L13_out9	: out std_logic_vector(51 downto 14)
	);
end entity L13;

architecture structure of L13 is

	component comp12_8 
	
		generic(n_component_first_row: integer := 34);
	
		port(
	
			-- input signals
			L1_S1		: in std_logic_vector(n_component_first_row downto 0);
			L1_S2		: in std_logic_vector(n_component_first_row downto 0);
			L1_C		: in std_logic_vector(n_component_first_row -1 downto 2);

			L2_S1		: in std_logic_vector(n_component_first_row -2 downto 2);
			L2_S2		: in std_logic_vector(n_component_first_row -2 downto 2);
			L2_C		: in std_logic_vector(n_component_first_row-3 downto 4);

			L3_S1		: in std_logic_vector(n_component_first_row-4 downto 4);
			L3_S2		: in std_logic_vector(n_component_first_row-4 downto 4);
			L3_C		: in std_logic_vector(n_component_first_row-5 downto 6);

			L4_S1		: in std_logic_vector(n_component_first_row -6 downto 6);
			L4_S2		: in std_logic_vector(n_component_first_row -6 downto 6);
			L4_C		: in std_logic_vector(n_component_first_row -7 downto 8);

			--output signals
			L1_sum		: out std_logic_vector(n_component_first_row downto 0);
			L1_carry	: out std_logic_vector(n_component_first_row downto 0);

			L2_sum		: out std_logic_vector(n_component_first_row -2 downto 2);
			L2_carry	: out std_logic_vector(n_component_first_row -2 downto 2);

			L3_sum		: out std_logic_vector(n_component_first_row -4 downto 4);
			L3_carry	: out std_logic_vector(n_component_first_row -4 downto 4);

			L4_sum		: out std_logic_vector(n_component_first_row -6 downto 6);
			L4_carry	: out std_logic_vector(n_component_first_row -6 downto 6)
		);
	
	end component comp12_8;
	
	component remapper8
	
		generic(N: integer := 34);
		
		port(
				-- input signals
			l1_sum		: in std_logic_vector(N downto 0);
			l1_carry	: in std_logic_vector(N downto 0);
			l2_sum		: in std_logic_vector(N-4 downto 0);
			l2_carry	: in std_logic_vector(N-4 downto 0);
			l3_sum		: in std_logic_vector(N-8 downto 0);
			l3_carry	: in std_logic_vector(N-8 downto 0);
			l4_sum		: in std_logic_vector(N-12 downto 0);
			l4_carry	: in std_logic_vector(N-12 downto 0);

			--output signals
			l1_s1		: out std_logic_vector(N+1 downto 0);
			l1_s2		: out std_logic_vector(N-1 downto 0);
			l1_c		: out std_logic_vector(N-3 downto 0);
			l2_s1		: out std_logic_vector(N-5 downto 0);
			l2_s2		: out std_logic_vector(N-7 downto 0);
			l2_c		: out std_logic_vector(N-9 downto 0);
			l3_s1		: out std_logic_vector(N-11 downto 0);
			l3_s2		: out std_logic_vector(N-13 downto 0)
		);
		end component remapper8;
		
	component leftCompressor12_8	
	
		port(
				-- 12 inputs
			inRow1		: in std_logic;
			inRow2		: in std_logic;
			inRow3		: in std_logic_vector(7 downto 6);
			inRow4		: in std_logic_vector(7 downto 5);
			inRow5		: in std_logic_vector(7 downto 5);
			inRow6		: in std_logic_vector(7 downto 4);
			inRow7		: in std_logic_vector(7 downto 3);
			inRow8		: in std_logic_vector(7 downto 3);
			inRow9		: in std_logic_vector(6 downto 2);
			inRow10		: in std_logic_vector(4 downto 1);
			inRow11		: in std_logic_vector(2 downto 1);
			inRow12		: in std_logic;
		
			-- 8 outputs
			outRow2		: out std_logic;
			outRow3		: out std_logic_vector(7 downto 6);
			outRow4		: out std_logic_vector(7 downto 5);
			outRow5		: out std_logic_vector(7 downto 4);
			outRow6		: out std_logic_vector(7 downto 3);
			outRow7		: out std_logic_vector(7 downto 2);
			outRow8		: out std_logic_vector(7 downto 1);
			outRow9		: out std_logic_vector(7 downto 0)
		);
		
	end component;	

	component rightCompressor11_8
	
		port(
			-- 11 inputs
			inRow3		: in std_logic_vector(1 downto 0);
			inRow4		: in std_logic_vector(1 downto 0);
			inRow5		: in std_logic_vector(1 downto 0);
			inRow6		: in std_logic_vector(3 downto 0);
			inRow7		: in std_logic_vector(3 downto 0);
			inRow8		: in std_logic_vector(3 downto 0);
			inRow9		: in std_logic_vector(5 downto 0);
			inRow10		: in std_logic_vector(5 downto 0);
			inRow11		: in std_logic_vector(5 downto 2);
			inRow12		: in std_logic_vector(7 downto 4);
			inRow13		: in std_logic_vector(7 downto 6);

			-- 8 outputs
			outRow2		: out std_logic;
			outRow3		: out std_logic_vector(1 downto 0);
			outRow4		: out std_logic_vector(2 downto 0);
			outRow5		: out std_logic_vector(3 downto 0);
			outRow6		: out std_logic_vector(4 downto 0);
			outRow7		: out std_logic_vector(5 downto 0);
			outRow8		: out std_logic_vector(6 downto 0);
			outRow9		: out std_logic_vector(7 downto 0)
		);
		
	end component;
	
	-- signals to connect compressor to remapper
	signal l1_sum		: std_logic_vector(34 downto 0);
	signal l1_carry		: std_logic_vector(34 downto 0);
	signal l2_sum		: std_logic_vector(30 downto 0);
	signal l2_carry		: std_logic_vector(30 downto 0);
	signal l3_sum		: std_logic_vector(26 downto 0);
	signal l3_carry		: std_logic_vector(26 downto 0);
	signal l4_sum		: std_logic_vector(22 downto 0);
	signal l4_carry		: std_logic_vector(22 downto 0);
	
begin

		Semplification1: comp12_8 generic map (n_component_first_row => 34)
			port map(
				L1_S1 	=> L13_in1(50 downto 16),
				L1_S2	=> L13_in2(50 downto 16),
				L1_C	=> L13_in3(49 downto 18),
				
				L2_S1	=> L13_in4(48 downto 18),
				L2_S2	=> L13_in5(48 downto 18),
				L2_C	=> L13_in6(47 downto 20),
				
				L3_S1	=> L13_in7(46 downto 20),
				L3_S2	=> L13_in8(46 downto 20),
				L3_C	=> L13_in9(45 downto 22),
				
				L4_S1	=> L13_in10(44 downto 22),
				L4_S2	=> L13_in11(44 downto 22),
				L4_C	=> L13_in12(43 downto 24),
				
				L1_sum		=> l1_sum,
				L1_carry	=> l1_carry,
				L2_sum		=> l2_sum,
				L2_carry	=> l2_carry,
				L3_sum		=> l3_sum,
				L3_carry	=> l3_carry,
				L4_sum		=> l4_sum,
				L4_carry	=> l4_carry
			);
			
		Remap: remapper8 generic map(N => 34)
			port map(
				l1_sum		=> l1_sum,
				l1_carry	=> l1_carry,
				l2_sum		=> l2_sum,
				l2_carry	=> l2_carry,
				l3_sum		=> l3_sum,
				l3_carry	=> l3_carry,
				l4_sum		=> l4_sum,
				l4_carry	=> l4_carry,
			
				l1_s1	=> L13_out1(51 downto 16),
				l1_s2	=> L13_out2(50 downto 17),
				l1_c	=> L13_out3(49 downto 18),
				l2_s1	=> L13_out4(48 downto 19),
				l2_s2	=> L13_out5(47 downto 20),
				l2_c	=> L13_out6(46 downto 21),
				l3_s1	=> L13_out7(45 downto 22),
				l3_s2	=>L13_out8(44 downto 23) 
			);
			
		rightCompr: rightCompressor11_8 
			port map(
				inRow3	=> L13_in3(17 downto 16),
				inRow4	=> L13_in4(17 downto 16),
				inRow5	=> L13_in5(17 downto 16),
				inRow6	=> L13_in6(19 downto 16),
				inRow7	=> L13_in7(19 downto 16),
				inRow8	=> L13_in8(19 downto 16),
				inRow9	=> L13_in9(21 downto 16),
				inRow10	=> L13_in10(21 downto 16),
				inRow11	=> L13_in11(21 downto 18),
				inRow12	=> L13_in12(23 downto 20),
				inRow13	=> L13_in13(23 downto 22),
				
				outRow2		=> L13_out2(16),
				outRow3		=> L13_out3(17 downto 16),
				outRow4		=> L13_out4(18 downto 16),
				outRow5		=> L13_out5(19 downto 16),
				outRow6		=> L13_out6(20 downto 16),
				outRow7		=> L13_out7(21 downto 16),
				outRow8		=> L13_out8(22 downto 16),
				outRow9		=> L13_out9(23 downto 16)
		);
		
		leftCompr: leftCompressor12_8 
			port map(
				inRow1	=> L13_in1(51),
				inRow2	=> L13_in2(51),
				inRow3	=> L13_in3(51 downto 50),
				inRow4	=> L13_in4(51 downto 49),
				inRow5	=> L13_in5(51 downto 49),
				inRow6	=> L13_in6(51 downto 48),
				inRow7	=> L13_in7(51 downto 47),
				inRow8	=> L13_in8(51 downto 47),
				inRow9	=> L13_in9(50 downto 46),
				inRow10	=> L13_in10(48 downto 45),
				inRow11	=> L13_in11(46 downto 45),
				inRow12	=> L13_in12(44),
				
				outRow2		=> L13_out2(51),
				outRow3		=> L13_out3(51 downto 50),
				outRow4		=> L13_out4(51 downto 49),
				outRow5		=> L13_out5(51 downto 48),
				outRow6		=> L13_out6(51 downto 47),
				outRow7		=> L13_out7(51 downto 46),
				outRow8		=> L13_out8(51 downto 45),
				outRow9		=> L13_out9(51 downto 44)
		);
			
		-- RECOSTRUCTION OF THE OUTPUTS
		
			-- Left triangle
			L13_out1(63 downto 52)	<= L13_in1(63 downto 52);
			L13_out2(63 downto 52)	<= L13_in2(63 downto 52);
			L13_out3(62 downto 52)	<= L13_in3(62 downto 52);
			L13_out4(60 downto 52)	<= L13_in4(60 downto 52);
			L13_out5(58 downto 52)	<= L13_in5(58 downto 52);
			L13_out6(56 downto 52)	<= L13_in6(56 downto 52);
			L13_out7(54 downto 52)	<= L13_in7(54 downto 52);
			L13_out8(52)			<= L13_in8(52);
			
			-- Right triangle
			L13_out1(15 downto 0)	<= L13_in1(15 downto 0);
			L13_out2(15 downto 0)	<= L13_in2(15 downto 0);
			L13_out3(15 downto 2)	<= L13_in3(15 downto 2);
			L13_out4(15 downto 4)	<= L13_in4(15 downto 4);
			L13_out5(15 downto 6)	<= L13_in5(15 downto 6);
			L13_out6(15 downto 8)	<= L13_in6(15 downto 8);
			L13_out7(15 downto 10)	<= L13_in7(15 downto 10);
			L13_out8(15 downto 12)	<= L13_in8(15 downto 12);
			L13_out9(15 downto 14)	<= L13_in9(15 downto 14);
			
			-- Square below
			L13_out9(43 downto 24)	<= L13_in13(43 downto 24);
	
end architecture structure;
