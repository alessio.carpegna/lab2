library ieee;
use ieee.std_logic_1164.all;

entity L9 is

	port(
		-- partial product as inputs
		L9_in1		: in std_logic_vector(63 downto 0);
		L9_in2		: in std_logic_vector(63 downto 0);
		L9_in3		: in std_logic_vector(62 downto 2);
		L9_in4		: in std_logic_vector(60 downto 4);
		L9_in5		: in std_logic_vector(58 downto 6);
		L9_in6		: in std_logic_vector(56 downto 8);
		L9_in7		: in std_logic_vector(54 downto 10);
		L9_in8		: in std_logic_vector(52 downto 12);
		L9_in9		: in std_logic_vector(51 downto 14);
		
		L9_out1	: out std_logic_vector(63 downto 0);
		L9_out2	: out std_logic_vector(63 downto 0);
		L9_out3	: out std_logic_vector(62 downto 2);
		L9_out4	: out std_logic_vector(60 downto 4);
		L9_out5	: out std_logic_vector(58 downto 6);
		L9_out6	: out std_logic_vector(57 downto 8)
	);
end entity L9;

architecture structure of L9 is

	component comp9_6 
	
		generic(n_component_first_row: integer := 46);
	
		port(
			-- input signals		
			L1_S1		: in std_logic_vector(n_component_first_row downto 0);
			L1_S2		: in std_logic_vector(n_component_first_row downto 0);
			L1_C		: in std_logic_vector(n_component_first_row -1 downto 2);
			
			L2_S1		: in std_logic_vector(n_component_first_row -2 downto 2);
			L2_S2		: in std_logic_vector(n_component_first_row -2 downto 2);
			L2_C		: in std_logic_vector(n_component_first_row-3 downto 4);
			
			L3_S1		: in std_logic_vector(n_component_first_row -4 downto 4);
			L3_S2		: in std_logic_vector(n_component_first_row -4 downto 4);
			L3_C		: in std_logic_vector(n_component_first_row-5 downto 6);

			--output signals
			L1_sum		: out std_logic_vector(n_component_first_row downto 0);
			L1_carry	: out std_logic_vector(n_component_first_row downto 0);
			
			L2_sum		: out std_logic_vector(n_component_first_row -2 downto 2);
			L2_carry	: out std_logic_vector(n_component_first_row -2 downto 2);
			
			L3_sum		: out std_logic_vector(n_component_first_row -4 downto 4);
			L3_carry	: out std_logic_vector(n_component_first_row -4 downto 4)
		);
	
	end component comp9_6;
	
	component remapper6
	
		generic(N: integer := 46);
		
		port(
				-- input signals
			l1_sum		: in std_logic_vector(N downto 0);
			l1_carry	: in std_logic_vector(N downto 0);
			l2_sum		: in std_logic_vector(N-4 downto 0);
			l2_carry	: in std_logic_vector(N-4 downto 0);
			l3_sum		: in std_logic_vector(N-8 downto 0);
			l3_carry	: in std_logic_vector(N-8 downto 0);

			--output signals
			l1_s1		: out std_logic_vector(N+1 downto 0);
			l1_s2		: out std_logic_vector(N-1 downto 0);
			l1_c		: out std_logic_vector(N-3 downto 0);
			l2_s1		: out std_logic_vector(N-5 downto 0);
			l2_s2		: out std_logic_vector(N-7 downto 0);
			l2_c		: out std_logic_vector(N-9 downto 0)
		);
		end component remapper6;
		
	component leftCompressor7_5	
	
		port(
			-- 7 inputs
			inRow1		: in std_logic;
			inRow2		: in std_logic;
			inRow3		: in std_logic_vector(7 downto 6);
			inRow4		: in std_logic_vector(7 downto 5);
			inRow5		: in std_logic_vector(7 downto 5);
			inRow6		: in std_logic_vector(6 downto 4);
			inRow7		: in std_logic_vector(4 downto 3);
		
			-- 5 outputs
			outRow2		: out std_logic;
			outRow3		: out std_logic_vector(7 downto 6);
			outRow4		: out std_logic_vector(7 downto 5);
			outRow5		: out std_logic_vector(7 downto 4);
			outRow6		: out std_logic_vector(7 downto 3)
		);
		
	end component;	

	component rightCompressor7_5
	
		port(
			-- 7 inputs
			inRow3		: in std_logic_vector(1 downto 0);
			inRow4		: in std_logic_vector(1 downto 0);
			inRow5		: in std_logic_vector(1 downto 0);
			inRow6		: in std_logic_vector(3 downto 0);
			inRow7		: in std_logic_vector(3 downto 0);
			inRow8		: in std_logic_vector(3 downto 2);
			inRow9		: in std_logic_vector(5 downto 4);

			-- 5 outputs
			outRow2		: out std_logic;
			outRow3		: out std_logic_vector(1 downto 0);
			outRow4		: out std_logic_vector(2 downto 0);
			outRow5		: out std_logic_vector(3 downto 0);
			outRow6		: out std_logic_vector(4 downto 0)
		);
	end component;
		
	-- signals to connect compressor to remapper
	signal l1_sum		: std_logic_vector(46 downto 0);
	signal l1_carry		: std_logic_vector(46 downto 0);
	signal l2_sum		: std_logic_vector(42 downto 0);
	signal l2_carry		: std_logic_vector(42 downto 0);
	signal l3_sum		: std_logic_vector(38 downto 0);
	signal l3_carry		: std_logic_vector(38 downto 0);	
	
begin

		Semplification1: comp9_6 generic map (n_component_first_row => 46)
			port map(
				L1_S1 	=> L9_in1(56 downto 10),
				L1_S2	=> L9_in2(56 downto 10),
				L1_C	=> L9_in3(55 downto 12),
				
				L2_S1	=> L9_in4(54 downto 12),
				L2_S2	=> L9_in5(54 downto 12),
				L2_C	=> L9_in6(53 downto 14),
				
				L3_S1	=> L9_in7(52 downto 14),
				L3_S2	=> L9_in8(52 downto 14),
				L3_C	=> L9_in9(51 downto 16),
				
				L1_sum		=> l1_sum,
				L1_carry	=> l1_carry,
				L2_sum		=> l2_sum,
				L2_carry	=> l2_carry,
				L3_sum		=> l3_sum,
				L3_carry	=> l3_carry
			);
			
		Remap: remapper6 generic map(N => 46)
			port map(
				l1_sum		=> l1_sum,
				l1_carry	=> l1_carry,
				l2_sum		=> l2_sum,
				l2_carry	=> l2_carry,
				l3_sum		=> l3_sum,
				l3_carry	=> l3_carry,
			
				l1_s1	=> L9_out1(57 downto 10),
				l1_s2	=> L9_out2(56 downto 11),
				l1_c	=> L9_out3(55 downto 12),
				l2_s1	=> L9_out4(54 downto 13),
				l2_s2	=> L9_out5(53 downto 14),
				l2_c	=> L9_out6(52 downto 15) 
			);
			
		rightCompr: rightCompressor7_5 
			port map(
				inRow3	=> L9_in3(11 downto 10),
				inRow4	=> L9_in4(11 downto 10),
				inRow5	=> L9_in5(11 downto 10),
				inRow6	=> L9_in6(13 downto 10),
				inRow7	=> L9_in7(13 downto 10),
				inRow8	=> L9_in8(13 downto 12),
				inRow9	=> L9_in9(15 downto 14),
				
				outRow2		=> L9_out2(10),
				outRow3		=> L9_out3(11 downto 10),
				outRow4		=> L9_out4(12 downto 10),
				outRow5		=> L9_out5(13 downto 10),
				outRow6		=> L9_out6(14 downto 10)
		);
		
		leftCompr: leftCompressor7_5 
			port map(
				inRow1	=> L9_in1(57),
				inRow2	=> L9_in2(57),
				inRow3	=> L9_in3(57 downto 56),
				inRow4	=> L9_in4(57 downto 55),
				inRow5	=> L9_in5(57 downto 55),
				inRow6	=> L9_in6(56 downto 54),
				inRow7	=> L9_in7(54 downto 53),
				
				outRow2		=> L9_out2(57),
				outRow3		=> L9_out3(57 downto 56),
				outRow4		=> L9_out4(57 downto 55),
				outRow5		=> L9_out5(57 downto 54),
				outRow6		=> L9_out6(57 downto 53)
		);
		
		-- RECOSTRUCTION OF THE OUTPUTS
			
			-- Left triangle
			L9_out1(63 downto 58)		<= L9_in1(63 downto 58);
			L9_out2(63 downto 58)		<= L9_in2(63 downto 58);
			L9_out3(62 downto 58)		<= L9_in3(62 downto 58);
			L9_out4(60 downto 58)		<= L9_in4(60 downto 58);
			L9_out5(58)					<= L9_in5(58);
			
			-- Right triangle
			L9_out1(9 downto 0)		<= L9_in1(9 downto 0);
			L9_out2(9 downto 0)		<= L9_in2(9 downto 0);
			L9_out3(9 downto 2)		<= L9_in3(9 downto 2);
			L9_out4(9 downto 4)		<= L9_in4(9 downto 4);
			L9_out5(9 downto 6)		<= L9_in5(9 downto 6);
			L9_out6(9 downto 8)		<= L9_in6(9 downto 8);
	
end architecture structure;
