library ieee;
use ieee.std_logic_1164.all;

entity remapper6 is

	generic(
		N	: integer := 18	
	);

	port(
		-- input signals
		l1_sum		: in std_logic_vector(N downto 0);
		l1_carry	: in std_logic_vector(N downto 0);
		l2_sum		: in std_logic_vector(N-4 downto 0);
		l2_carry	: in std_logic_vector(N-4 downto 0);
		l3_sum		: in std_logic_vector(N-8 downto 0);
		l3_carry	: in std_logic_vector(N-8 downto 0);
		--output signals
		l1_s1		: out std_logic_vector(N+1 downto 0);
		l1_s2		: out std_logic_vector(N-1 downto 0);
		l1_c		: out std_logic_vector(N-3 downto 0);
		l2_s1		: out std_logic_vector(N-5 downto 0);
		l2_s2		: out std_logic_vector(N-7 downto 0);
		l2_c		: out std_logic_vector(N-9 downto 0)
	);
end entity remapper6;

architecture behaviour of remapper6 is
begin


-- l1_s1:		c1 c1 c1 c1 c1 c1 c1 c1 c1 c1 c1 c1 c1 c1 c1 s1
-- l1_s2:		   s1 c2 c2 c2 c2 c2 c2 c2 c2 c2 c2 c2 s1 s1
-- l1_c:		      s1 s1 c3 c3 c3 c3 c3 c3 c3 s1 s1 s2
-- l2_s1:		         s2 s1 s1 s1 s1 s1 s1 s1 s2 s2
-- l2_s2:			    s2 s2 s2 s2 s2 s2 s2 s3
-- l2_c:			       s3 s3 s3 s3 s3 s3


	-- l1_s1
	l1_s1 <= l1_carry &
		l1_sum(0);

	-- l1_s2
	l1_s2 <= l1_sum(l1_sum'length-1) &
		l2_carry &
		l1_sum(2 downto 1);

	-- l1_c
	l1_c <= l1_sum(l1_sum'length-2 downto l1_sum'length-3) &
		l3_carry &
		l1_sum(4 downto 3) &
		l2_sum(0);

	-- l2_s1
	l2_s1 <= l2_sum(l2_sum'length-1) &
		l1_sum(l1_sum'length-4 downto 5) &
		l2_sum(2 downto 1);

	-- l2_s2
	l2_s2 <= l2_sum(l2_sum'length-2 downto 3) &
		l3_sum(0);

	-- l2_c
	l2_c <= l3_sum(l3_sum'length-1 downto 1);





end architecture behaviour;
