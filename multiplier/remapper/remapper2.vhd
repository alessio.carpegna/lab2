library ieee;
use ieee.std_logic_1164.all;

entity remapper2 is

	generic(
		N	: integer := 18	
	);

	port(
		-- input signals
		l1_sum		: in std_logic_vector(N downto 0);
		l1_carry	: in std_logic_vector(N downto 0);

		--output signals
		l1_s1		: out std_logic_vector(N+1 downto 0);
		l1_s2		: out std_logic_vector(N-1 downto 0)
	);
end entity remapper2;

architecture behaviour of remapper2 is
begin


-- l1_s1:		c1 c1 c1 c1 c1 c1 c1 c1 c1 c1 c1 c1 c1 c1 c1 s1
-- l1_s2:		   s1 s1 s1 s1 s1 s1 s1 s1 s1 s1 s1 s1 s1 s1

	-- l1_s1
	l1_s1 <= l1_carry &
		l1_sum(0);

	-- l2_s1
	l1_s2 <= l1_sum(l1_sum'length-1 downto 1);


end architecture behaviour;
