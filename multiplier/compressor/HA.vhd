library ieee;
use ieee.std_logic_1164.all;

entity HA is

	port(
		-- input signals
		s1		: in std_logic;
		s2		: in std_logic;
		
		-- output signals
		sum		: out std_logic;
		carry	: out std_logic
	);
end entity HA;

architecture behavior of HA is
	
begin

	sum 	<= s1 XOR s2;
	carry	<= s1 AND s2;
	
end architecture behavior;
