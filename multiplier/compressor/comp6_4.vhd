library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity comp6_4 is

	generic(n_component_first_row: integer := 54);

	port(
		-- input signals
		L1_S1		: in std_logic_vector(n_component_first_row downto 0);
		L1_S2		: in std_logic_vector(n_component_first_row downto 0);
		L1_C		: in std_logic_vector(n_component_first_row -1 downto 2);
		
		L2_S1		: in std_logic_vector(n_component_first_row -2 downto 2);
		L2_S2		: in std_logic_vector(n_component_first_row -2 downto 2);
		L2_C		: in std_logic_vector(n_component_first_row-3 downto 4);

		--output signals
		L1_sum		: out std_logic_vector(n_component_first_row downto 0);
		L1_carry	: out std_logic_vector(n_component_first_row downto 0);
		
		L2_sum		: out std_logic_vector(n_component_first_row -2 downto 2);
		L2_carry	: out std_logic_vector(n_component_first_row -2 downto 2)
	);
end entity comp6_4;

architecture generation of comp6_4 is

	component FA port(
		s1		: in std_logic;
		s2		: in std_logic;
		c_in	: in std_logic;
		
		sum		: out std_logic;
		carry	: out std_logic
	);
	end component FA;
	
	component HA port(
		s1		: in std_logic;
		s2		: in std_logic;
		
		sum		: out std_logic;
		carry	: out std_logic
	);
	end component HA;
	
begin

	LAYER1: for i in 0 to (n_component_first_row) generate 
	
		Half_Adder: if (i=0 or i=1 or i=n_component_first_row) generate
			half: HA port map(
				s1		=> L1_S1(i),
				s2		=> L1_S2(i),
				
				sum		=> L1_sum(i),
				carry	=> L1_carry(i)
			);
		end generate Half_Adder;
		
		Full_Adder: if (i>=2 and i<=n_component_first_row-1) generate
			full: FA port map(
				s1		=> L1_S1(i),
				s2		=> L1_S2(i),
				c_in	=> L1_C(i),
				
				sum		=> L1_sum(i),
				carry	=> L1_carry(i)
			);
		end generate Full_Adder;

	end generate LAYER1;
	
	LAYER2: for i in 2 to (n_component_first_row-2) generate 
	
		Half_Adder: if (i=2 or i=3 or i=n_component_first_row-2) generate
			half: HA port map(
				s1		=> L2_S1(i),
				s2		=> L2_S2(i),
				
				sum		=> L2_sum(i),
				carry	=> L2_carry(i)
			);
		end generate Half_Adder;
		
		Full_Adder: if (i>=4 and i<=n_component_first_row-3) generate
			full: FA port map(
				s1		=> L2_S1(i),
				s2		=> L2_S2(i),
				c_in	=> L2_C(i),
				
				sum		=> L2_sum(i),
				carry	=> L2_carry(i)
			);
		end generate Full_Adder;

	end generate LAYER2;
	
end architecture generation;
