library ieee;
use ieee.std_logic_1164.all;

entity FA is

	port(
		-- input signals
		s1		: in std_logic;
		s2		: in std_logic;
		c_in	: in std_logic;
		
		-- output signals
		sum		: out std_logic;
		carry	: out std_logic
	);
end entity FA;

architecture behavior of FA is
	
begin

	sum 	<= s1 XOR s2 XOR c_in;
	carry	<= (s1 AND s2) OR (c_in AND s1) OR (c_in AND s2);
	
end architecture behavior;
