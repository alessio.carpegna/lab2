library ieee;
use ieee.std_logic_1164.all;

entity mux_4to1 is
	port (
		--input sugnals
		in_00		: in std_logic_vector(32 downto 0);
		in_01		: in std_logic_vector(32 downto 0);
		in_11		: in std_logic_vector(32 downto 0);
		in_10		: in std_logic_vector(32 downto 0);
		mux_sel		: in std_logic_vector(1 downto 0); 
		
		--output signals
		mux_out		: out std_logic_vector(32 downto 0)
	);
end mux_4to1;

architecture behavioural of mux_4to1 is 

begin

	process(mux_sel, in_00, in_01, in_11, in_10)
		begin
		if mux_sel = "00" then 
			mux_out <= in_00;
		elsif mux_sel = "01" then
			mux_out <= in_01;
		elsif mux_sel = "11" then
			mux_out <= in_11;
		else
			mux_out <= in_10;
		end if; 
	end process;

end architecture; 
