library ieee;
use ieee.std_logic_1164.all;

entity pp_create is

	port(
		-- input triplet
		b		: in std_logic_vector(2 downto 0);
		
		-- multiplicand
		a		: in std_logic_vector(31 downto 0);
		
		-- output partial product
		pp		: out std_logic_vector(32 downto 0);
		s		: out std_logic
	);
end entity pp_create;

architecture structure of pp_create is

	component encoder 
		port(
			b	: in std_logic_vector(2 downto 0);
			
			sel	: out std_logic_vector(1 downto 0)
		);
	end component encoder;
	
	component mux_4to1
		port (
			--input sugnals
			in_00		: in std_logic_vector(32 downto 0);
			in_01		: in std_logic_vector(32 downto 0);
			in_11		: in std_logic_vector(32 downto 0);
			in_10		: in std_logic_vector(32 downto 0);
			mux_sel		: in std_logic_vector(1 downto 0); 
			
			--output signals
			mux_out		: out std_logic_vector(32 downto 0)
		);
	end component mux_4to1;
	
	-- signals preparared for the mux
	signal zero		: std_logic_vector(32 downto 0);
	signal a_ext	: std_logic_vector(32 downto 0);
	signal a_double	: std_logic_vector(32 downto 0);

	signal sel		: std_logic_vector(1 downto 0);
	signal mux_out	: std_logic_vector(32 downto 0);
	
begin

	zero	<= (others => '0');		-- 0
	a_ext	<= '0' & a;			-- a
	
	a_double(32 downto 1) 	<= a;	-- 2a
	a_double(0)		<= '0';

		Decod: encoder 
			port map(
				b	=> b,
				sel	=> sel
			);
			
		pp_sel: mux_4to1
			port map(
				in_00	=> zero,
				in_01	=> zero,
				in_10	=> a_ext,
				in_11	=> a_double,
				mux_sel	=> sel,
				
				mux_out	=> mux_out
			);
			
		neg_creation: process(mux_out, b(2))
			begin
				if b(2) = '0' then
					pp	<= mux_out;
				else
					pp	<= not(mux_out);
				end if;
			end process;
	
	s <= b(2);
	
end architecture structure;
