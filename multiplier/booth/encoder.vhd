library ieee;
use ieee.std_logic_1164.all;


entity encoder is
	port(
		b	: in std_logic_vector(2 downto 0);
		sel	: out std_logic_vector(1 downto 0)		
	);
end entity encoder;

architecture behaviour of encoder is
begin

	-- the encoding is designed to drive a mux with the following
	-- selection scheme:
	--	00 -> 0
	--	01 -> 0
	--	10 -> A
	--	11 -> 2A
	encoding : process(b)
	begin
		case b is
			when "000" =>
				sel <= "00"; -- 0
			when "001" =>
				sel <= "10"; -- A
			when "010" =>
				sel <= "10"; -- A
			when "011" =>
				sel <= "11"; -- 2A
			when "100" =>
				sel <= "11"; -- 2A
			when "101" =>
				sel <= "10"; -- A
			when "110" =>
				sel <= "10"; -- A
			when others =>
				sel <= "00"; -- 0
		end case;

	end process encoding;

end architecture behaviour;
