library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;

ENTITY tb_fpmul_pipeline IS
END tb_fpmul_pipeline;

architecture tb of tb_fpmul_pipeline is

component FPmul is
  PORT( 
      FP_A : IN     std_logic_vector (31 DOWNTO 0);
      FP_B : IN     std_logic_vector (31 DOWNTO 0);
      clk  : IN     std_logic;
      FP_Z : OUT    std_logic_vector (31 DOWNTO 0)
   );
end component FPmul;

signal test_FP_A, test_FP_B, test_FP_Z 	: std_logic_vector (31 DOWNTO 0);
signal test_clk							: std_logic;

	file infile  : text open read_mode is "../fp_samples.hex";
	file outfile : text open write_mode is "fp_results.hex";

BEGIN

DUT: FPmul port map(
	clk		=> test_clk,
	FP_A	=> test_FP_A,
	FP_B	=> test_FP_B,
	FP_Z	=> test_FP_Z
);

clk_generation : process
begin
	test_clk <= '0';
	wait for 10 ns;
	test_clk <= '1';
	wait for 10 ns;
end process clk_generation;

-- READ SAMPLES FROM HEX FILE
data_vin_gen : process (test_clk)

	variable inLine 			: line;
	variable op 	: std_logic_vector(31 downto 0);

	begin
		if test_clk'event and test_clk='1' 
		then
		   if(not endfile(infile))
		   then
				-- read first operand
				readline(infile, inline);
				hread(inline, op);
				test_FP_A <= op;
				test_FP_B <= op;
			end if;
		end if;
	end process data_vin_gen;
	
-- WRITE RESULTS ON FILE IN HEX
vout_data_sample : process(test_clk)

		variable op_Z		: std_logic_vector(31 downto 0);
		variable outLine	: line;

	begin

		if test_clk'event and test_clk = '1'
		then
			op_Z := test_FP_Z;
			hwrite(outLine, op_Z);
			writeline(outFile, outLine);
		end if;
	end process vout_data_sample;

end architecture tb;
